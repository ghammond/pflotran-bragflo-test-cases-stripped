#!/bin/python
#!!py_start
import sys
import os
import argparse
import numpy as np
import h5py

""" Current usage: python ../BRAGFLO/Source/binread.py --plane xz --binary bf_6a.bin --ascii bf_6a.ascii --hdf5 bf_6a.h5 """
""" Current usage: python ../BRAGFLO/Source/binread.py --plane xz --postarray pcgw --binary bf_6a.bin --hdf5 bf_6a.h5 """
""" Current usage: python ../BRAGFLO/Source/binread.py --plane xz --unitgrid true --binary bf_6a.bin --hdf5 bf_6a.h5 """
""" Current usage: python ../BRAGFLO/Source/binread.py --plane xz --binary bf_6a.bin --hdf5 bf_6a.h5 --snapshot_timeunit y --round_time true """

"""
Warning, currently the ParaView reader only supports cartesian product grids (i.e. dx varies along x, dy varies along y, dz varies along z)
 Thus, when generating the (cell corner) coordinates for Paraview visualization, this script assumes the BRAGFLO grid is a cartesian product grid (i.e. no flaring).
 The first row of dx values and the first column of dy values are used to generate the coordinates.
 
 Also, we optionally swap (via the --plane option) the Y and Z directions in the hdf5 file to be consistent with the convention that Z is the vertical direction.
 This effects the element/snapshot variables, the Coordinate variables, the DXGRID, DYGRID, DZGRID, and GRIDVOL arrays, and NX, NY, NZ values.
"""

"""
The BRAGFLO binary file contains eight sections/sets of data.  Sections 1-4 are output once, while sections 5-8 are time-based and output multiple times.
( 1) Header
       This section contains run control information such as the
       program name, version, run date and time, input files used, etc.
       This is stored in the hdf5 group "header".
       
( 2) Problem size
       This section lists the sizes of various arrays.  For example, 
       the number of grid blocks in each dimension (NX,NY,NZ), 
       the number of user-requested "element"-by-element (i.e. snapshot) output variables (NVAREL),
       the number of user-requested "history"-at-a-specific-element (i.e. observation-point) output variables (MHIV)
       the number of simulation statistics variables (excluding TIMES and KBIN) (MHIVM)
       the number of built-in "global" mass balance statistic variables (NGVAR).
       
( 3) Glossary of output variables
       This section contains four tables:
       ( 1) History variable locations -- lists the grid block locations (i,j,k indices) for each user-requested history variable (NGBHIV(I))
       ( 2) Element variable labels -- lists the id, (short) name, description, unit, and unit conversion factor of each user-requested element variable (see UM tbl7 pg86)
       ( 3) History variable labels -- lists the id, (short) name, description, unit, and unit conversion factor of each user-requested history variable (see UM tbl7 pg86)
                                       This table also contains the labels of the simulation statistics variables (a subset of line 3 for the .sum file, see UM pg167-168)
       ( 4) Global variable labels -- lists the id, (short) name, description, unit, and unit conversion factor of each built-in global mass balance variable (see UM pg 168)
       
( 4) Grid
       This section contains the dimension of each grid block in each direction (DXGRID, DYGRID, DZGRID).
       It also contains the volume of each grid block (GRIDVOL).
       
( 5) Simulation statistics
       This section contains time, timestep, cpu time, and iteration count information.  It is output at every timestep.
       This is a modified subset of what is presented in the .sum file.  See BRAGFLO UM pg167-168.
       
( 6) History variables
       This section contains the value of each history variable at each user-specified grid block.  It is output at every timestep.
       See BRAGFLO UM pg85.
       
( 7) Mass balance variables
       This section contains the cumulative and maxiumum "global" mass balance errors for brine and gas, as well as the (i,j,k) locations 
       where the maximum error as occured.  It is only output at user-requested times, i.e. when the internal KBIN flag is one.
       This is a modified subset of what is presented in the .sum file.  See BRAGFLO UM pg168.
       
( 8) Element variables
       This section contains snapshots of each user-requested "element" variable over the entire grid, at user-specified times.
       It is only output at user-requested times, i.e. when the internal KBIN flag is one.

Note, monitor blocks are only written to the .sum file, not to the binary file.

The binread program is presented in the BRAGFLO user manual 9.1, pg 158-163.
"""
#!!py_end

#F !*****!***********************************************************************!
#F !*****!***************************************************************************************************************************!
#F PROGRAM BINREAD
#F  !
#F  ! Translates BRAGFLO binary file into ASCII file.
#F  ! J. Schreiber, SAIC, 03/08/96
#F  !  Additional comments added by M. Nemer 3/4/07
#F  !
#F  ! Sarathi: Updated parameter sizes to be consistent with BRAGFLO.
#F  !          Renamed DATE to RDATE. Changed FILNAM length from 80 to 128 (consistent ith BRAGFLO)
#F  !          Added more comments. Removed implicit typing. Updated to FORTRAN90 conventions.
#F  !
#F  ! to compile:
#F  ! gfortran -O2 -m64 -finit-local-zero -fdefault-double-8 -fdefault-real-8 -fdefault-integer-8 -fimplicit-none -o binread binread.f90
#F  !
#F  IMPLICIT NONE
#F  
#F  ! In the binary file, BRAGFLO outputs 
#F  ! (1) element variables - any variable that has a time-varying value AT EACH grid block
#F  ! (2) history variables - any variable that has a time vary value, but specified at ONE grid block
#F  !                         history variables are prinited at every time step.
#F  ! (3) global mass balance information
#F  
#F  ! Parameters that set maximum array dimensions (from bragflo's params.inc)
#F  INTEGER, PARAMETER :: MX=140, MY=80, MZ=1  !< max number of grid cells in each dimension (NX,NY,NZ)
#F  INTEGER, PARAMETER :: NVPR=137             !< NumVarPRint - max number of element variables defined in User Manual Table 7
#F  INTEGER, PARAMETER :: MXHIV=15000          !< MaXHIstVars - max number of history variables allowed
#F  INTEGER, PARAMETER :: MVHIV=MX*MY*MZ       !< MaxValuesHistVars - max number of grid blocks for history variables
#F  INTEGER, PARAMETER :: MGVAR=10             !< max number of global mass balance variables
#F  
#F  CHARACTER(LEN=128) :: BRAG_BIN_FNAME       !< binary input file name
#F  CHARACTER(LEN=128) :: ASC_OUTPUT_FNAME     !< ascii output file name
#F  INTEGER            :: FSTATUS              !< file status (for reading, e.g. detecting end-of-file
#F  
#F  CHARACTER(LEN=  8) :: PNAME(2)             !< Program name: (1) PREBRAG, (2) BRAGFLO
#F  CHARACTER(LEN=  8) :: VRSION(2)            !< PREBRAG/BRAGFLO version number
#F  CHARACTER(LEN=  8) :: REVDATE(2)           !< PREBRAG/BRAGFLO revision date
#F  CHARACTER(LEN=  9) :: RDATE(2)             !< PREBRAG/BRAGFLO run date
#F  CHARACTER(LEN=  8) :: RTIME(2)             !< PREBRAG/BRAGFLO run time
#F  
#F  CHARACTER(LEN= 32) :: CPUNAME              !< Name of machine on which BRAGFLO was run.
#F  CHARACTER(LEN=  1) :: FILTYP               !< Type of file ('B'=binary).
#F  INTEGER            :: NUMFIL               !< Number of files used in the BRAGFLO run.
#F  CHARACTER(LEN=128) :: FILNAM(6)            !< Names of NUMFIL files used by BRAGFLO.
#F  CHARACTER(LEN=132) :: TITLE                !< Title of run.
#F  
#F  INTEGER            :: NX,NY,NZ             !< Number of grid cells in each direction
#F  CHARACTER(LEN=  8) :: UNITS                !< Units system used for ouput ('SI' or 'ENGLISH').
#F  
#F  INTEGER :: NVAREL                          !< Number of element variables.
#F  INTEGER :: MHIVM                           !< Number of simulation statistics variables (excluding TIMES and KBIN)
#F  INTEGER :: MHIV                            !< Number of history variables
#F  INTEGER :: NHIV                            !< No. of element vbls that are printed as history vbls.
#F  INTEGER :: NGVAR                           !< Number of global variables.
#F  INTEGER :: MHIVT                           !< MHIVT = MHIVM + MHIV ! simulation stat + history variables
#F  
#F  INTEGER :: LHI(NVPR)                       !< Element vbl no. from User Manual Table 7 for this history vbl.
#F  INTEGER :: NGBHIV(NVPR)                    !< No. of grid blocks to be printed as history vbl for variable LHI.
#F  INTEGER :: IIHIV(MVHIV,NVPR)               !< I-index of grid block for this history variable.
#F  INTEGER :: JJHIV(MVHIV,NVPR)               !< J-index of grid block for this history variable.
#F  INTEGER :: KKHIV(MVHIV,NVPR)               !< K-index of grid block for this history variable.
#F  
#F  INTEGER :: LE(NVPR)                        !< Number of the element variable.
#F  CHARACTER(LEN=  8) :: NAMVAREL(NVPR)       !< Variable name to be used in POSTBRAG.
#F  CHARACTER(LEN= 50) :: LABELEL(NVPR)        !< Description of this variable.
#F  CHARACTER(LEN= 20) :: LABUNTEL(NVPR)       !< Units label for this variable (e.g., Pa).
#F  REAL :: UNTCNVEL(NVPR)                     !< Factor to convert from UNITSO to SI units.
#F  
#F  INTEGER :: LH(MXHIV)                       !< Number of the history variable.
#F  CHARACTER(LEN=  8) :: NAMVARHI(MXHIV)      !< Variable name to be used in POSTBRAG.
#F  CHARACTER(LEN= 50) :: LABELHI(MXHIV)       !< Description of this variable.
#F  CHARACTER(LEN= 20) :: LABUNTHI(MXHIV)      !< Units label for this variable (e.g., Pa).
#F  REAL :: UNTCNVHI(MXHIV)                    !< Factor to convert from UNITSO to SI units.
#F  
#F  INTEGER :: LG(MGVAR)                       !< Number of the global variable.
#F  CHARACTER(LEN=  8) :: NAMVARGL(MGVAR)      !< Variable name to be used in POSTBRAG.
#F  CHARACTER(LEN= 50) :: LABELGL(MGVAR)       !< Description of this variable.
#F  CHARACTER(LEN= 20) :: LABUNTGL(MGVAR)      !< Units label for this variable (e.g., kg).
#F  REAL :: UNTCNVGL(MGVAR)                    !< Factor to convert from UNITSO to SI units.
#F  
#F  REAL :: DXGRID(MX,MY,MZ)                   !< Mesh dimensions in X direction
#F  REAL :: DYGRID(MX,MY,MZ)                   !< Mesh dimensions in Y direction
#F  REAL :: DZGRID(MX,MY,MZ)                   !< Mesh dimensions in Z direction
#F  REAL :: GRIDVOL(MX,MY,MZ)                  !< Grid cell volumes
#F  
#F  REAL :: HVAR(MXHIV)                        !< Array to hold history variable values
#F  REAL :: YY(MX,MY,MZ)                       !< Array to hold element variable values
#F  
#F  INTEGER :: I,J,K,L                         !< Integers for indexing
#F  INTEGER :: NPO                             !< Counter for history variables
#F  
#F  ! Simulation performance measures
#F  REAL :: TIMES,TIMED,TIMEYR                 !< Elapsed simulation time in [s], [days], [yr]
#F  REAL :: DELT,DELTD,DELTYR                  !< Time step size in [s], [days], [yr].
#F  REAL :: AVGITER                            !< Average Newton-Raphson iterations per time step.
#F  REAL :: CPUS                               !< CPU time used for this time step [s].
#F  REAL :: CPUHR                              !< Total CPU time used so far in the run [hr].
#F  REAL :: RNSTEP                             !< Time step number (REAL).
#F  REAL :: RITERTOT                           !< Total Newton-Raphson iterations so far (REAL).
#F  INTEGER :: KBIN                            !< Flag: =0: only history vbl output will follow at this timestep.
#F                                             !<       =1: history vbls and global and element vbls output will follow at this timestep.
#F  
#F  ! Global variables (mass balance info at current step)
#F  REAL :: BBALC                              !< Brine mass balance error over entire mesh [kg].
#F  REAL :: GBALC                              !< Gas mass balance error over entire mesh [kg].
#F  REAL :: BBALMX                             !< Max relative brine mass bal error in any grid block.
#F  REAL :: GBALMX                             !< Max relative gas mass bal error in any grid block.
#F  REAL :: IBBALMX                            !< I-index of grid block with BBALMX \
#F  REAL :: JBBALMX                            !< J-index of grid block with BBALMX  |  These are
#F  REAL :: KBBALMX                            !< K-index of grid block with BBALMX  |_ saved as
#F  REAL :: IGBALMX                            !< I-index of grid block with GBALMX  |  REAL vbls
#F  REAL :: JGBALMX                            !< J-index of grid block with GBALMX  |
#F  REAL :: KGBALMX                            !< K-index of grid block with GBALMX /
#F  
#F  ! Check that two arguments are passed - the binary input and ascii output filenames
#F  if ( COMMAND_ARGUMENT_COUNT() /= 2 ) THEN
#F    write(*,*) 'You need to specify both the binary input and ascii output files on the command line.'
#F    stop -1
#F  end if
#F  
#F  ! Read in input binary and output ascii file names
#F  ! WRITE (*,*) 'Enter name of BRAGFLO binary file.'
#F  ! READ  (*,'(A)') BRAG_BIN_FNAME
#F  call GET_COMMAND_ARGUMENT(1, BRAG_BIN_FNAME)
#F  
#F  ! WRITE (*,*) 'Enter name of ASCII output file.'
#F  ! READ  (*,'(A)') ASC_OUTPUT_FNAME
#F  call GET_COMMAND_ARGUMENT(2, ASC_OUTPUT_FNAME)
#F  
#F  OPEN (8,FILE=BRAG_BIN_FNAME,STATUS='OLD',FORM='UNFORMATTED',IOSTAT=FSTATUS)
#F  OPEN (9,FILE=ASC_OUTPUT_FNAME,STATUS='REPLACE')
#F  

#!!py_start
## Explanation of FORTRAN records
#   The BRAGFLO .bin file is a FORTRAN-written, UNFORMATTED, SEQUENTIAL binary file.
#   In FORTRAN I/O, each WRITE statement writes "record" to the file.
#   For SEQUENTIAL access files, typically each record is padded with a header and footer, 
#   e.g., record_header, WRITE data, record_footer.
#   The record_header and record_footer lengths and datatypes vary depending on the compiler
#   and system, but at least for gcc and OracleSolarisStudio, the 
#   record_header and record_footer are each 4 bytes (e.g. int*4 or 4 1-byte (unprinting) characters).
#   These must be read and thrown away when reading and stepping through the binary file.

# Set FORTRAN Data types and lengths
rec_marker_bytes = 4; # assume 4-byte record markers
fortran_integer = np.dtype('int64'); # assume 8-byte integer (this could be different)
fortran_double = np.dtype('float64'); # assume 8-byte double

###############################################################################
## Process command line options
###############################################################################

output_options = {'ascii': False, 'hdf5': False, 'plane': 'xy', 'unitgrid': False, 'create_pcgw': False, 'snapshot_timeunit': 'y', 'round_time': False};

# Get the input and output file names from the command line arguments
# fname_bragflo_binary = sys.argv[1];
# fname_ascii_output = sys.argv[2];
# fname_h5_snapshot_output = sys.argv[3];

arg_parser = argparse.ArgumentParser();
arg_parser.add_argument('-binary', '--binary', help='bragflo binary file name', required=True);
arg_parser.add_argument('-ascii', '--ascii', help='ascii text output file name', required=False);
arg_parser.add_argument('-hdf5', '--hdf5', help='hdf5 output file name', required=False);
arg_parser.add_argument('-plane', '--plane', help='view plane for hdf5 file, xy or xz', required=True );
arg_parser.add_argument('-unitgrid', '--unitgrid', help='Calculate the Coordinate array with unit steps instead of dx dy dz values', required=False );
arg_parser.add_argument('-postarray', '--postarray', help='post-processed arrays to create for hdf5 file', required=False, action='append' );

arg_parser.add_argument('-snapshot_timeunit', '--snapshot_timeunit', help='Time unit printed to the snapshot group names, y or s', required=False );
arg_parser.add_argument('-round_time', '--round_time', help='Round the time printed to the snapshot group name. Default is false.', required=False );
arg_parser_args = vars(arg_parser.parse_args());

if (arg_parser_args['binary']):
  fname_bragflo_binary = arg_parser_args['binary'];
if (arg_parser_args['ascii']):
  output_options['ascii'] = True;
  fname_ascii_output = arg_parser_args['ascii'];
if (arg_parser_args['hdf5']):
  output_options['hdf5'] = True;
  fname_h5_snapshot_output = arg_parser_args['hdf5'];
  
if (arg_parser_args['plane'].lower()=='xy'):
  output_options['plane'] = arg_parser_args['plane'];
elif (arg_parser_args['plane'].lower()=='xz'):
  output_options['plane'] = arg_parser_args['plane'];
else:
  print('The view plane output for hdf5 must be either xy or xz');
  
if (arg_parser_args['unitgrid']):
  if (arg_parser_args['unitgrid'] in map(str.lower, ['true', 'yes', 't', 'y', '1']) ):
    output_options['unitgrid'] = True;
  
if (arg_parser_args['postarray']):
  if ('pcgw' in map(str.lower, arg_parser_args['postarray']) ):
    output_options['create_pcgw'] = True;

if (arg_parser_args['snapshot_timeunit']):
  if (arg_parser_args['snapshot_timeunit'] in map(str.lower, ['y', 'yr']) ):
    output_options['snapshot_timeunit'] = 'y';
  elif (arg_parser_args['snapshot_timeunit'] in map(str.lower, ['s', 'sec']) ):
    output_options['snapshot_timeunit'] = 's';
  if (arg_parser_args['snapshot_timeunit'] in map(str.lower, ['d', 'days']) ):
    output_options['snapshot_timeunit'] = 'd';

if (arg_parser_args['round_time']):
  if (arg_parser_args['round_time'] in map(str.lower, ['true', 'yes', 't', 'y', '1']) ):
    output_options['round_time'] = True;

###############################################################################
## Open files
###############################################################################

# Open the binary file, in read-binary mode, set position to beginning
fh_bin = open(fname_bragflo_binary, mode='rb');
fh_bin.seek(0,0);

# Create the ascii file
if (output_options['ascii']):
  fh_ascii = open(fname_ascii_output, mode='w');

# Create the h5 snapshot file
if (output_options['hdf5']):
  fh_h5_snapshot = h5py.File(fname_h5_snapshot_output, mode='w');
  
  # Create top-level h5 groups
  fh_h5_snapshot.create_group(b'/header');
  fh_h5_snapshot.create_group(b'/variable_glossary');
  fh_h5_snapshot.create_group(b'/grid');
  fh_h5_snapshot.create_group(b'/output_tables');
  #fh_h5_snapshot.create_group(b'/element_variables');
  fh_h5_snapshot.create_group(b'/output_times');
  fh_h5_snapshot.create_group(b'/Coordinates');
  

#!!py_end

#F  
#F  !
#F  ! Read header section with QA info, vbl glossary, grid dimensions:
#F  !   Note:  If first record is 'PREBRAG', then the file starts with
#F  !          PREBRAG QA info; otherwise, there is no PREBRAG info, and
#F  !          the file starts with BRAGFLO QA info.
#F  !   
#F  !   PNAME(1)   = Preprocessor name (PREBRAG).
#F  !   VRSION(1)  = PREBRAG version number.
#F  !   REVDATE(1) = PREBRAG revision date.
#F  !   RDATE(1)   = PREBRAG run date.
#F  !   RTIME(1)   = PREBRAG run time.
#F  !   
#F  !   RDATE(2)   = BRAGFLO run date.
#F  !   RTIME(2)   = BRAGFLO run time.
#F  !   PNAME(2)   = Program name (BRAGFLO).
#F  !   VRSION(2)  = BRAGFLO version number.
#F  !   REVDATE(2) = BRAGFLO revision date.
#F  !   
#F  !   CPUNAME    = Name of machine on which BRAGFLO was run.
#F  !   FILTYPE    = Type of file ('B'=binary).
#F  !   NUMFIL     = Number of files used in the BRAGFLO run.
#F  !   FILNAM     = Names of NUMFIL files used by BRAGFLO.
#F  !   
#F  !   TITLE      = Title of run.
#F  !   NX,NY,NZ   = Dimensions of problem run.
#F  !   UNITSO     = Units system used for ouput ('SI' or 'ENGLISH').
#F  !   NVAREL     = Number of element variables.
#F  !   MHIVM      = Number of time- & performance-related history vbls.
#F  !   MHIV       = Total no. of element vbl-based history vbls printed
#F  !                at each time step.
#F  !   NHIV       = No. of element vbls that are printed as history vbls.
#F  !   NGVAR      = Number of global variables.
#F  !
#F  
#F  !
#F  ! Read Program information: PNAME,RDATE,RTIME,VRSION,REVDATE
#F  !
#F  READ (8) PNAME(1)
#F  ! Detect optional PREBRAG header info
#F  IF (PNAME(1)(1:7) .EQ. 'PREBRAG') THEN
#F     READ (8,IOSTAT=FSTATUS) VRSION(1)
#F     READ (8,IOSTAT=FSTATUS) REVDATE(1)
#F     READ (8,IOSTAT=FSTATUS) RDATE(1)
#F     READ (8,IOSTAT=FSTATUS) RTIME(1)
#F  END IF
#F  
#F  IF (PNAME(1)(1:7) .EQ. 'PREBRAG') THEN
#F     WRITE (9,*) '** PREBRAG Program Name **'
#F     WRITE (9,*) PNAME(1)
#F     WRITE (9,*) '** PREBRAG Version Number **'
#F     WRITE (9,*) VRSION(1)
#F     WRITE (9,*) '** PREBRAG Revision Date **'
#F     WRITE (9,*) REVDATE(1)
#F     WRITE (9,*) '** PREBRAG Run Date **'
#F     WRITE (9,*) RDATE(1)
#F     WRITE (9,*) '** PREBRAG Run Time **'
#F     WRITE (9,*) RTIME(1)
#F  END IF
#F  
#F  IF (PNAME(1)(1:7) .EQ. 'PREBRAG') THEN
#F     READ (8,IOSTAT=FSTATUS) RDATE(2)
#F  ELSE
#F     ! otherwise the first line read was the bragflo run date
#F     RDATE(2) = PNAME(1)
#F  END IF
#F  
#F  READ (8,IOSTAT=FSTATUS) RTIME(2)
#F  READ (8,IOSTAT=FSTATUS) PNAME(2)
#F  READ (8,IOSTAT=FSTATUS) VRSION(2)
#F  READ (8,IOSTAT=FSTATUS) REVDATE(2)
#F  
#F  WRITE (9,*) '** BRAGFLO Run Date **'
#F  WRITE (9,*) RDATE(2)
#F  WRITE (9,*) '** BRAGFLO Run Time **'
#F  WRITE (9,*) RTIME(2)
#F  WRITE (9,*) '** BRAGFLO Program Name **'
#F  WRITE (9,*) PNAME(2)
#F  WRITE (9,*) '** BRAGFLO Version Number **'
#F  WRITE (9,*) VRSION(2)
#F  WRITE (9,*) '** BRAGFLO Revision Date **'
#F  WRITE (9,*) REVDATE(2)
#F  

#!!pystart

###############################################################################
## Process ( 1) Header
###############################################################################

# read from bin
dummy_int = fh_bin.read(rec_marker_bytes); # skip the fortran record header
pname = np.fromfile(fh_bin, dtype=np.dtype('S8'), count=1)[0];
dummy_int = fh_bin.read(rec_marker_bytes); # skip the fortran record footer

# If first record is 'PREBRAG', then the file starts with PREBRAG QA info; 
# otherwise, there is no PREBRAG info, and the file starts with BRAGFLO QA info.
if(b'PREBRAG' in pname):
  dummy_int = fh_bin.read(rec_marker_bytes);
  vrsion = np.fromfile(fh_bin, dtype=np.dtype('S8'), count=1)[0];
  dummy_int = fh_bin.read(rec_marker_bytes);
  
  dummy_int = fh_bin.read(rec_marker_bytes);
  revdate = np.fromfile(fh_bin, dtype=np.dtype('S8'), count=1)[0];
  dummy_int = fh_bin.read(rec_marker_bytes);
  
  dummy_int = fh_bin.read(rec_marker_bytes);
  rdate = np.fromfile(fh_bin, dtype=np.dtype('S8'), count=1)[0];  # S8 for prebrag/solaris
  dummy_int = fh_bin.read(rec_marker_bytes);
  
  dummy_int = fh_bin.read(rec_marker_bytes);
  rtime = np.fromfile(fh_bin, dtype=np.dtype('S8'), count=1)[0];
  dummy_int = fh_bin.read(rec_marker_bytes);
  
# ascii output
if (output_options['ascii']):
  if(b'PREBRAG' in pname):
    fh_ascii.write(b'** PREBRAG Program Name **' + '\n');
    fh_ascii.write(pname + '\n');
    fh_ascii.write(b'** PREBRAG Version Number **' + '\n');
    fh_ascii.write(vrsion + '\n');
    fh_ascii.write(b'** PREBRAG Revision Date **' + '\n');
    fh_ascii.write(revdate + '\n');
    fh_ascii.write(b'** PREBRAG Run Date **' + '\n');
    fh_ascii.write(rdate + '\n');
    fh_ascii.write(b'** PREBRAG Run Time **' + '\n');
    fh_ascii.write(rtime + '\n');
  
# h5 output
if (output_options['hdf5']):
  if(b'PREBRAG' in pname):
    h5grp_header = fh_h5_snapshot['/header'];
    h5grp_header.create_dataset('prebragflo_program_name', data=pname );
    h5grp_header.create_dataset('prebragflo_version_number', data=vrsion );
    h5grp_header.create_dataset('prebragflo_revision_date', data=revdate );
    h5grp_header.create_dataset('prebragflo_run_date', data=rdate );
    h5grp_header.create_dataset('prebragflo_run_time', data=rtime );

# read from bin
# read rdate, the run date
if(b'PREBRAG' in pname):
  dummy_int = fh_bin.read(rec_marker_bytes);
  rdate = np.fromfile(fh_bin, dtype=np.dtype('S8'), count=1)[0];   # S8 for prebrag/solaris
  dummy_int = fh_bin.read(rec_marker_bytes);
else:
  # otherwise the first line read was the bragflo run date
  rdate = pname;
  
dummy_int = fh_bin.read(rec_marker_bytes);
rtime = np.fromfile(fh_bin, dtype=np.dtype('S8'), count=1)[0];
dummy_int = fh_bin.read(rec_marker_bytes);

dummy_int = fh_bin.read(rec_marker_bytes);
pname = np.fromfile(fh_bin, dtype=np.dtype('S8'), count=1)[0];
dummy_int = fh_bin.read(rec_marker_bytes);

dummy_int = fh_bin.read(rec_marker_bytes);
vrsion = np.fromfile(fh_bin, dtype=np.dtype('S8'), count=1)[0];
dummy_int = fh_bin.read(rec_marker_bytes);

dummy_int = fh_bin.read(rec_marker_bytes);
revdate = np.fromfile(fh_bin, dtype=np.dtype('S8'), count=1)[0];
dummy_int = fh_bin.read(rec_marker_bytes);

# ascii output
if (output_options['ascii']):
  fh_ascii.write(b'** BRAGFLO Run Date **' + '\n');
  fh_ascii.write(rdate + '\n');
  fh_ascii.write(b'** BRAGFLO Run Time **' + '\n');
  fh_ascii.write(rtime + '\n');
  fh_ascii.write(b'** BRAGFLO Program Name **' + '\n');
  fh_ascii.write(pname + '\n');
  fh_ascii.write(b'** BRAGFLO Version Number **' + '\n');
  fh_ascii.write(vrsion + '\n');
  fh_ascii.write(b'** BRAGFLO Revision Date **' + '\n');
  fh_ascii.write(revdate + '\n');

# h5 output
if (output_options['hdf5']):
  h5grp_header = fh_h5_snapshot['/header'];
  h5grp_header.create_dataset('bragflo_run_date', data=rdate );
  h5grp_header.create_dataset('bragflo_run_time', data=rtime );
  h5grp_header.create_dataset('bragflo_program_name', data=pname );
  h5grp_header.create_dataset('bragflo_version_number', data=vrsion );
  h5grp_header.create_dataset('bragflo_revision_date', data=revdate );

# delete variables
del(pname, vrsion, revdate, rdate, rtime);
#!!pyend

#F  !
#F  ! Read Program information: CPUNAME,FILTYP,NUMFIL,FILNAM(I),TITLE
#F  !
#F  READ (8,IOSTAT=FSTATUS) CPUNAME
#F  READ (8,IOSTAT=FSTATUS) FILTYP
#F  READ (8,IOSTAT=FSTATUS) NUMFIL
#F  DO I=1,NUMFIL
#F     READ (8,IOSTAT=FSTATUS) FILNAM(I)
#F  END DO
#F  READ (8,IOSTAT=FSTATUS) TITLE
#F  
#F  WRITE (9,*) '** Computer Name **'
#F  WRITE (9,*) CPUNAME
#F  WRITE (9,*) '** Output File Type **'
#F  WRITE (9,*) FILTYP
#F  WRITE (9,*) '** Number of Files Used **'
#F  WRITE (9,*) NUMFIL
#F  WRITE (9,*) '** Names of Files Used **'
#F  DO I=1,NUMFIL
#F     WRITE (9,*) FILNAM(I)
#F  END DO
#F  WRITE (9,*) '** Title of Run **'
#F  WRITE (9,'(A)') TITLE
#F  

#!!pystart
# read from bin
dummy_int = fh_bin.read(rec_marker_bytes);
cpuname = np.fromfile(fh_bin, dtype=np.dtype('S32'), count=1)[0];
dummy_int = fh_bin.read(rec_marker_bytes);

dummy_int = fh_bin.read(rec_marker_bytes);
filtyp = np.fromfile(fh_bin, dtype=np.dtype('S1'), count=1)[0];
dummy_int = fh_bin.read(rec_marker_bytes);

dummy_int = fh_bin.read(rec_marker_bytes);
numfil = np.fromfile(fh_bin, dtype=fortran_integer, count=1)[0];
dummy_int = fh_bin.read(rec_marker_bytes);

filnam = np.zeros((numfil,), dtype=np.dtype('S128'));
for i in range(numfil):
  dummy_int = fh_bin.read(rec_marker_bytes);
  filnam[i] = np.fromfile(fh_bin, dtype=np.dtype('S128'), count=1)[0];
  dummy_int = fh_bin.read(rec_marker_bytes);

dummy_int = fh_bin.read(rec_marker_bytes);
title = np.fromfile(fh_bin, dtype=np.dtype('S132'), count=1)[0];
dummy_int = fh_bin.read(rec_marker_bytes);

# ascii output
if (output_options['ascii']):
  fh_ascii.write(b'** Computer Name **' + '\n');
  fh_ascii.write(cpuname + '\n');
  fh_ascii.write(b'** Output File Type **' + '\n');
  fh_ascii.write(filtyp + '\n');
  fh_ascii.write(b'** Number of Files Used **' + '\n');
  fh_ascii.write('%20i' % numfil + '\n');
  fh_ascii.write(b'** Names of Files Used **' + '\n');
  for i in range(numfil):
    fh_ascii.write(filnam[i] + '\n');

  fh_ascii.write(b'** Title of Run **' + '\n');
  fh_ascii.write(title + '\n');


# h5 output
if (output_options['hdf5']):
  h5grp_header = fh_h5_snapshot['/header'];
  h5grp_header.create_dataset('computer_name', data=cpuname );
  h5grp_header.create_dataset('output_file_type', data=filtyp );
  h5grp_header.create_dataset('number_files_used', data=numfil, dtype=fortran_integer );
  h5grp_header.create_dataset('names_files_used', data=filnam );
  h5grp_header.create_dataset('title', data=title );

# delete variables
del(cpuname, filtyp, numfil, filnam);
#!!pyend

#F  !
#F  ! Read Program information: NX,NY,NZ, UNITS, NVAREL,MHIVM,MHIV,NHIV,NGVAR
#F  !
#F  READ (8,IOSTAT=FSTATUS) NX,NY,NZ
#F  READ (8,IOSTAT=FSTATUS) UNITS
#F  READ (8,IOSTAT=FSTATUS) NVAREL,MHIVM,MHIV,NHIV,NGVAR
#F  MHIVT = MHIVM + MHIV
#F  
#F  WRITE (9,*) '** NX, NY, NZ **'
#F  WRITE (9,*) NX,NY,NZ
#F  WRITE (9,*) '** Output Units **'
#F  WRITE (9,*) UNITS
#F  WRITE (9,*) '** NVAREL, MHIVM, MHIV, NHIV, MGVAR **'
#F  WRITE (9,*) NVAREL,MHIVM,MHIV,NHIV,NGVAR
#F  WRITE (9,*)
#F  

#!!pystart
###############################################################################
## Process ( 2) Problem size
###############################################################################
# read from bin
dummy_int = fh_bin.read(rec_marker_bytes);
[nx, ny, nz] = np.fromfile(fh_bin, dtype=fortran_integer, count=3);
dummy_int = fh_bin.read(rec_marker_bytes);

dummy_int = fh_bin.read(rec_marker_bytes);
units = np.fromfile(fh_bin, dtype=np.dtype('S8'), count=1)[0];
dummy_int = fh_bin.read(rec_marker_bytes);

dummy_int = fh_bin.read(rec_marker_bytes);
[nvarel,mhivm,mhiv,nhiv,ngvar] = np.fromfile(fh_bin, dtype=fortran_integer, count=5);
dummy_int = fh_bin.read(rec_marker_bytes);

# total number of history variables
mhivt = mhivm + mhiv;

# ascii output
if (output_options['ascii']):
  fh_ascii.write(b'** NX, NY, NZ **' + '\n');
  fh_ascii.write('%20i %20i %20i' % (nx,ny,nz) +'\n');
  fh_ascii.write(b'** Output Units **' + '\n');
  fh_ascii.write(units + '\n');
  fh_ascii.write(b'** NVAREL, MHIVM, MHIV, NHIV, MGVAR **' + '\n');
  fh_ascii.write('%20i %20i %20i %20i %20i' % (nvarel,mhivm,mhiv,nhiv,ngvar) + '\n');
  fh_ascii.write( '\n');

# h5 output
if (output_options['hdf5']):
  h5grp_grid = fh_h5_snapshot['/grid'];
  if (output_options['plane']=='xy'):
    h5grp_grid.create_dataset('nx', data=nx, dtype=fortran_integer );
    h5grp_grid.create_dataset('ny', data=ny, dtype=fortran_integer );
    h5grp_grid.create_dataset('nz', data=nz, dtype=fortran_integer );
  
  if (output_options['plane']=='xz'):
    # swap y and z
    h5grp_grid.create_dataset('nx', data=nx, dtype=fortran_integer );
    h5grp_grid.create_dataset('nz', data=ny, dtype=fortran_integer );
    h5grp_grid.create_dataset('ny', data=nz, dtype=fortran_integer );
  
  h5grp_header = fh_h5_snapshot['/header'];
  h5grp_header.create_dataset('output_units', data=units );
  
  h5grp_glossary = fh_h5_snapshot['/variable_glossary'];
  h5grp_glossary.create_dataset('nvarel', data=nvarel, dtype=fortran_integer );
  h5grp_glossary.create_dataset('mhivm', data=mhivm, dtype=fortran_integer );
  h5grp_glossary.create_dataset('mhiv', data=mhiv, dtype=fortran_integer );
  h5grp_glossary.create_dataset('nhiv', data=nhiv, dtype=fortran_integer );
  h5grp_glossary.create_dataset('ngvar', data=ngvar, dtype=fortran_integer );

# delete variables (need to keep some)
del(title, units);
#!!pyend

#F  !
#F  ! Read glossary of variables:
#F  !   LHI      = Element vbl no. from User Manual Table 7 for this
#F  !              history vbl.
#F  !   NGBHIV   = No. of grid blocks to be printed as history vbl for
#F  !              variable LHI.
#F  !   IIHIV    = I-index of grid block for this history variable.
#F  !   JJHIV    = J-index of grid block for this history variable.
#F  !   KKHIV    = K-index of grid block for this history variable.
#F  !   LE       = Number of the element variable.
#F  !   NAMVAREL = Variable name to be used in POSTBRAG.
#F  !   LABELEL  = Description of this variable.
#F  !   LABUNTEL = Units label for this variable (e.g., Pa).
#F  !   UNTCNVEL = Factor to convert from UNITSO to SI units.
#F  !   LH       = Number of the history variable.
#F  !   NAMVARHI = Variable name to be used in POSTBRAG.
#F  !   LABELHI  = Description of this variable.
#F  !   LABUNTHI = Units label for this variable (e.g., Pa).
#F  !   UNTCNVHI = Factor to convert from UNITSO to SI units.
#F  !   LG       = Number of the global variable.
#F  !   NAMVARGL = Variable name to be used in POSTBRAG.
#F  !   LABELGL  = Description of this variable.
#F  !   LABUNTGL = Units label for this variable (e.g., kg).
#F  !   UNTCNVGL = Factor to convert from UNITSO to SI units.
#F  !
#F  
#F  DO I=1,NHIV
#F     READ (8,IOSTAT=FSTATUS) LHI(I),NGBHIV(I),(IIHIV(J,I),JJHIV(J,I),KKHIV(J,I), J=1,NGBHIV(I))
#F  END DO
#F  DO I=1,NVAREL
#F     READ (8,IOSTAT=FSTATUS) LE(I),NAMVAREL(I),LABELEL(I),LABUNTEL(I),UNTCNVEL(I)
#F  END DO
#F  DO I=1,MHIVT
#F     READ (8,IOSTAT=FSTATUS) LH(I),NAMVARHI(I),LABELHI(I),LABUNTHI(I),UNTCNVHI(I)
#F  END DO
#F  DO I=1,NGVAR
#F     READ (8,IOSTAT=FSTATUS) LG(I),NAMVARGL(I),LABELGL(I),LABUNTGL(I),UNTCNVGL(I)
#F  END DO
#F  
#F  
#F  WRITE (9,*) '** History Vbl Locations **'
#F  DO I=1,NHIV
#F     WRITE (9,'(2I5,10(3X,3I3))') LHI(I),NGBHIV(I), (IIHIV(J,I),JJHIV(J,I),KKHIV(J,I),J=1,NGBHIV(I))
#F  END DO
#F  WRITE (9,*)
#F  WRITE (9,*) '** Element Vbl Labels **'
#F  DO I=1,NVAREL
#F     WRITE (9,'(I4,2X,A,2X,A,2X,A,1PE16.6)') LE(I),NAMVAREL(I), LABELEL(I),LABUNTEL(I),UNTCNVEL(I)
#F  END DO
#F  WRITE (9,*)
#F  WRITE (9,*) '** History Vbl Labels **'
#F  DO I=1,MHIVT
#F     WRITE (9,'(I4,2X,A,2X,A,2X,A,1PE16.6)') LH(I),NAMVARHI(I), LABELHI(I),LABUNTHI(I),UNTCNVHI(I)
#F  END DO
#F  WRITE (9,*)
#F  WRITE (9,*) '** Global Vbl Labels  **'
#F  DO I=1,NGVAR
#F     WRITE (9,'(I4,2X,A,2X,A,2X,A,1PE16.6)') LG(I),NAMVARGL(I), LABELGL(I),LABUNTGL(I),UNTCNVGL(I)
#F  END DO
#F  

#!!pystart
###############################################################################
## Process ( 3) Glossary of output variables
###############################################################################
# read from bin
# First create numpy datatype for the table, then allocate the table, then read the binary data

# History Variable Locations
# create a numpy compound data type (similar to a struct) - this serves as a row in the table
dtype_history_variables_location_table = np.dtype([('lhi', fortran_integer), ('ngbhiv', fortran_integer), ('iihiv', fortran_integer), ('jjhiv', fortran_integer), ('kkhiv', fortran_integer) ]);
history_variables_location_table = np.empty((mhiv,), dtype=dtype_history_variables_location_table); # allocate table

# The BRAGFLO binary interlaces the history-variable-id and the list of grid blocks associated with that variable.
# The premise is that different grid blocks can have different history variables requested for output.
# When we store it in memory and then write it to hdf5, we de-interlace.
# That is, we store the history-variable-id and grid block as a separate row, for each history-variable-id and associate grid block combination
idx_flat_table = 0;
for i in range(nhiv):
  dummy_int = fh_bin.read(rec_marker_bytes);
  [lhi_temp, ngbhiv_temp] = np.fromfile(fh_bin, dtype=fortran_integer, count=2);
  for j in range(ngbhiv_temp):
    [iihiv_temp, jjhiv_temp, kkhiv_temp] = np.fromfile(fh_bin, dtype=fortran_integer, count=3);
    history_variables_location_table[idx_flat_table] = (lhi_temp, ngbhiv_temp, iihiv_temp, jjhiv_temp, kkhiv_temp);
    idx_flat_table = idx_flat_table+1;
  dummy_int = fh_bin.read(rec_marker_bytes);

#del(idx_table, lhi_temp, ngbhiv_temp, iihiv_temp, jjhiv_temp, kkhiv_temp); # do not delete

# Element Variable Labels
dtype_element_variables_labels_table = np.dtype([('le', fortran_integer), ('namvarel', 'S8'), ('labelel', 'S50'), ('labuntel', 'S20'), ('untcnvel', fortran_double)]);
element_variables_labels_table = np.zeros((nvarel,), dtype=dtype_element_variables_labels_table); # allocate table
for i in range(nvarel):
  dummy_int = fh_bin.read(rec_marker_bytes);
  element_variables_labels_table[i] = np.fromfile(fh_bin, dtype=dtype_element_variables_labels_table, count=1);
  dummy_int = fh_bin.read(rec_marker_bytes);

# History Variable Labels
dtype_history_variables_labels_table = np.dtype([('lh', fortran_integer), ('namvarhi', 'S8'), ('labelhi', 'S50'), ('labunthi', 'S20'), ('untcnvhi', fortran_double)]);
history_variables_labels_table = np.zeros((mhivt,), dtype=dtype_history_variables_labels_table); # allocate table
for i in range(mhivt):
  dummy_int = fh_bin.read(rec_marker_bytes);
  history_variables_labels_table[i] = np.fromfile(fh_bin, dtype=dtype_history_variables_labels_table, count=1);
  dummy_int = fh_bin.read(rec_marker_bytes);

# Global (Mass Balance) Variable Labels
dtype_mass_balance_labels_table = np.dtype([('lg', fortran_integer), ('namvargl', 'S8'), ('labelgl', 'S50'), ('labuntgl', 'S20'), ('untcnvgl', fortran_double)]);
mass_balance_labels_table = np.zeros((ngvar,), dtype=dtype_mass_balance_labels_table); # allocate table
for i in range(ngvar):
  dummy_int = fh_bin.read(rec_marker_bytes);
  mass_balance_labels_table[i] = np.fromfile(fh_bin, dtype=dtype_mass_balance_labels_table, count=1);
  dummy_int = fh_bin.read(rec_marker_bytes);

# ascii output
if (output_options['ascii']):
  fh_ascii.write(b'** History Vbl Locations **' + '\n');
  idx_flat_table = 0;
  for i in range(nhiv):
    fh_ascii.write('%5i %5i' % (history_variables_location_table['lhi'][idx_flat_table], history_variables_location_table['ngbhiv'][idx_flat_table]) );
    for j in range(history_variables_location_table['ngbhiv'][idx_flat_table]):
      fh_ascii.write('   %3i %3i %3i' % tuple(history_variables_location_table[['iihiv', 'jjhiv', 'kkhiv']][idx_flat_table]) );
      idx_flat_table = idx_flat_table+1;
    fh_ascii.write('\n');
  fh_ascii.write('\n');

  fh_ascii.write(b'** Element Vbl Labels **' + '\n');
  for i in range(nvarel):
    fh_ascii.write('%4i  %s  %s  %s  %16.6E' % tuple(element_variables_labels_table[i]) + '\n');
  fh_ascii.write( '\n');

  fh_ascii.write(b'** History Vbl Labels **' + '\n');
  for i in range(mhivt):
    fh_ascii.write('%4i  %s  %s  %s  %16.6E' % tuple(history_variables_labels_table[i]) + '\n');
  fh_ascii.write( '\n');

  fh_ascii.write(b'** Global Vbl Labels  **' + '\n');
  for i in range(ngvar):
    fh_ascii.write('%4i  %s  %s  %s  %16.6E' % tuple(mass_balance_labels_table[i]) + '\n');
  #fh_ascii.write( '\n');


# h5 output
if (output_options['hdf5']):
  h5grp_glossary = fh_h5_snapshot['/variable_glossary'];
  h5grp_glossary.create_dataset('history_variables_location_table', data=history_variables_location_table );
  h5grp_glossary.create_dataset('element_variables_labels_table', data=element_variables_labels_table );
  
  # Split the "history" table into simulation statistics variables and history variables
  # note, BRAGFLO does not include TIMES and KBIN, which are in the simulation stats table, in the glossary
  h5grp_glossary.create_dataset('simulation_statistics_labels_table', data=history_variables_labels_table[0:mhivm] );
  h5grp_glossary.create_dataset('history_variables_labels_table', data=history_variables_labels_table[mhivm:] );
  h5grp_glossary.create_dataset('mass_balance_labels_table', data=mass_balance_labels_table );

# del(dtype_element_variables_table, dtype_history_variables_table, dtype_mass_balance_table);
# del(element_variables_table, history_variables_table, mass_balance_table);

#F  
#F  !
#F  ! Mesh dimensions and grid block volumes.
#F  !
#F  READ (8,IOSTAT=FSTATUS) (((DXGRID(I,J,K),I=1,NX),J=1,NY),K=1,NZ)
#F  READ (8,IOSTAT=FSTATUS) (((DYGRID(I,J,K),I=1,NX),J=1,NY),K=1,NZ)
#F  READ (8,IOSTAT=FSTATUS) (((DZGRID(I,J,K),I=1,NX),J=1,NY),K=1,NZ)
#F  READ (8,IOSTAT=FSTATUS) (((GRIDVOL(I,J,K),I=1,NX),J=1,NY),K=1,NZ)
#F  
#F  WRITE (9,*)
#F  WRITE (9,*) '** Mesh Sizes:  DXGRID **'
#F  WRITE (9,*) (((DXGRID(I,J,K),I=1,NX),J=1,NY),K=1,NZ)
#F  WRITE (9,*)
#F  WRITE (9,*) '** Mesh Sizes:  DYGRID **'
#F  WRITE (9,*) (((DYGRID(I,J,K),I=1,NX),J=1,NY),K=1,NZ)
#F  WRITE (9,*)
#F  WRITE (9,*) '** Mesh Sizes:  DZGRID **'
#F  WRITE (9,*) (((DZGRID(I,J,K),I=1,NX),J=1,NY),K=1,NZ)
#F  WRITE (9,*)
#F  WRITE (9,*) '** Mesh Sizes:  GRIDVOL **'
#F  WRITE (9,*) (((GRIDVOL(I,J,K),I=1,NX),J=1,NY),K=1,NZ)
#F  !
#F  ! *** End of header section (READ) ***
#F  !
#F  

#!!pystart
###############################################################################
## Process ( 4) Grid
###############################################################################
# read from bin
dxgrid = np.zeros((nx,ny,nz), dtype=fortran_double);  # allocate array
dummy_int = fh_bin.read(rec_marker_bytes);
dxgrid = np.fromfile(fh_bin, dtype=fortran_double, count=nx*ny*nz);
dummy_int = fh_bin.read(rec_marker_bytes);

dygrid = np.zeros((nx,ny,nz), dtype=fortran_double);  # allocate array
dummy_int = fh_bin.read(rec_marker_bytes);
dygrid = np.fromfile(fh_bin, dtype=fortran_double, count=nx*ny*nz);
dummy_int = fh_bin.read(rec_marker_bytes);

dzgrid = np.zeros((nx,ny,nz), dtype=fortran_double);  # allocate array
dummy_int = fh_bin.read(rec_marker_bytes);
dzgrid = np.fromfile(fh_bin, dtype=fortran_double, count=nx*ny*nz);
dummy_int = fh_bin.read(rec_marker_bytes);

gridvol = np.zeros((nx,ny,nz), dtype=fortran_double);  # allocate array
dummy_int = fh_bin.read(rec_marker_bytes);
gridvol = np.fromfile(fh_bin, dtype=fortran_double, count=nx*ny*nz);
dummy_int = fh_bin.read(rec_marker_bytes);

# ascii output
if (output_options['ascii']):
  fh_ascii.write('\n');

  fh_ascii.write(b'** Mesh Sizes:  DXGRID **' + '\n');
  dxgrid.tofile(fh_ascii, sep=' ', format='%24.16E');
  fh_ascii.write('\n\n');

  fh_ascii.write(b'** Mesh Sizes:  DYGRID **' + '\n');
  dygrid.tofile(fh_ascii, sep=' ', format='%24.16E');
  fh_ascii.write('\n\n');

  fh_ascii.write(b'** Mesh Sizes:  DZGRID **' + '\n');
  dzgrid.tofile(fh_ascii, sep=' ', format='%24.16E');
  fh_ascii.write('\n\n');

  fh_ascii.write(b'** Mesh Sizes:  GRIDVOL **' + '\n');
  gridvol.tofile(fh_ascii, sep=' ', format='%24.16E');
  fh_ascii.write('\n');

# h5 output
if (output_options['hdf5']):
  # Note, the data is stored as a flat array in fortran order
  # numpy and hdf5 use c order.  Must permute the dimensions when storing.
  if (output_options['plane']=='xy'):
    h5grp_grid = fh_h5_snapshot['/grid'];
    h5grp_grid.create_dataset('dxgrid', data=dxgrid.reshape((nx,ny,nz), order='F') );
    h5grp_grid.create_dataset('dygrid', data=dygrid.reshape((nx,ny,nz), order='F') );
    h5grp_grid.create_dataset('dzgrid', data=dzgrid.reshape((nx,ny,nz), order='F') );
    h5grp_grid.create_dataset('gridvol', data=gridvol.reshape((nx,ny,nz), order='F') );

  if (output_options['plane']=='xz'):
    # swap y and z
    h5grp_grid = fh_h5_snapshot['/grid'];
    h5grp_grid.create_dataset('dxgrid', shape=(nx,nz,ny), data=dxgrid.reshape((nx,ny,nz), order='F').transpose((0,2,1)) );
    h5grp_grid.create_dataset('dzgrid', shape=(nx,nz,ny), data=dygrid.reshape((nx,ny,nz), order='F').transpose((0,2,1)) );
    h5grp_grid.create_dataset('dygrid', shape=(nx,nz,ny), data=dzgrid.reshape((nx,ny,nz), order='F').transpose((0,2,1)) );
    h5grp_grid.create_dataset('gridvol', shape=(nx,nz,ny), data=gridvol.reshape((nx,ny,nz), order='F').transpose((0,2,1)) );
    
  
  # Create "Coordinates" group and arrays for Paraview visualization.
  # Note, Paraview requires a cartesian product grid.
  # Thus  X, Y, Z are 1D arrays that list the x, y, and z cell node (i.e. corner node) 
  # locations in the x, y, and z directions.
  h5grp_coordinates = fh_h5_snapshot['/Coordinates'];
  
  if (output_options['plane']=='xy' and output_options['unitgrid'] == False):
  
    X_coords = np.zeros(nx+1);
    X_coords[1: ] = np.cumsum( dxgrid.reshape((nx,ny,nz), order='F')[:,0,0] );
    h5grp_coordinates.create_dataset(b'X [m]', data=X_coords, dtype=fortran_double);
    
    Y_coords = np.zeros(ny+1);
    Y_coords[1: ] = np.cumsum( dygrid.reshape((nx,ny,nz), order='F')[0,:,0] );
    h5grp_coordinates.create_dataset(b'Y [m]', data=Y_coords, dtype=fortran_double);
    
    Z_coords = np.zeros(nz+1);
    Z_coords[1: ] = np.cumsum( dzgrid.reshape((nx,ny,nz), order='F')[0,0,:] );
    h5grp_coordinates.create_dataset(b'Z [m]', data=Z_coords, dtype=fortran_double);
    
  if (output_options['plane']=='xz' and output_options['unitgrid'] == False):
    # swap y and z
    X_coords = np.zeros(nx+1);
    X_coords[1: ] = np.cumsum( dxgrid.reshape((nx,ny,nz), order='F')[:,0,0] );
    h5grp_coordinates.create_dataset(b'X [m]', data=X_coords, dtype=fortran_double);
    
    Y_coords = np.zeros(ny+1);
    Y_coords[1: ] = np.cumsum( dygrid.reshape((nx,ny,nz), order='F')[0,:,0] );
    h5grp_coordinates.create_dataset(b'Z [m]', data=Y_coords, dtype=fortran_double);
    
    Z_coords = np.zeros(nz+1);
    Z_coords[1: ] = np.cumsum( dzgrid.reshape((nx,ny,nz), order='F')[0,0,:] );
    h5grp_coordinates.create_dataset(b'Y [m]', data=Z_coords, dtype=fortran_double);
  
  
  if (output_options['plane']=='xy' and output_options['unitgrid'] == True):
  
    X_coords = np.arange(0.0, nx+1.0, 1.0, dtype=fortran_double);
    h5grp_coordinates.create_dataset(b'X [m]', data=X_coords, dtype=fortran_double);
    
    Y_coords = np.arange(0.0, ny+1.0, 1.0, dtype=fortran_double);
    h5grp_coordinates.create_dataset(b'Y [m]', data=Y_coords, dtype=fortran_double);
    
    Z_coords = np.arange(0.0, nz+1.0, 1.0, dtype=fortran_double);
    h5grp_coordinates.create_dataset(b'Z [m]', data=Z_coords, dtype=fortran_double);
  
  if (output_options['plane']=='xz' and output_options['unitgrid'] == True):
  
    X_coords = np.arange(0.0, nx+1.0, 1.0, dtype=fortran_double);
    h5grp_coordinates.create_dataset(b'X [m]', data=X_coords, dtype=fortran_double);
    
    Y_coords = np.arange(0.0, ny+1.0, 1.0, dtype=fortran_double);
    h5grp_coordinates.create_dataset(b'Z [m]', data=Y_coords, dtype=fortran_double);
    
    Z_coords = np.arange(0.0, nz+1.0, 1.0, dtype=fortran_double);
    h5grp_coordinates.create_dataset(b'Y [m]', data=Z_coords, dtype=fortran_double);
  
  del(X_coords, Y_coords, Z_coords);
  
# delete variables
del(dxgrid, dygrid, dzgrid, gridvol);
#!!pyend

#F  !
#F  ! *** Cycle through time steps ***
#F  !
#F  
#F  !
#F  ! First read time, time step, CPU time, performance measures:
#F  !   TIMES    = Elapsed simulation time [s].
#F  !   TIMED    = Elapsed simulation time [days].
#F  !   TIMEYR   = Elapsed simulation time [yr].
#F  !   DELT     = Time step [s].
#F  !   DELTD    = Time step [days].
#F  !   DELTYR   = Time step [yr].
#F  !   AVGITER  = Average Newton-Raphson iterations per time step.
#F  !   CPUS     = CPU time used for this time step [s].
#F  !   CPUHR    = Total CPU time used so far in the run [hr].
#F  !   RNSTEP   = Time step number (REAL).
#F  !   RITERTOT = Total Newton-Raphson iterations so far (REAL).
#F  !   KBIN     = Flag indicating:
#F  !               =0: only history vbl output will follow.
#F  !               =1: history vbls and global and element vbls output
#F  !                   will follow at this time step.
#F  !
#F  NPO = 0
#F  DO ! loop over file records
#F  READ (8,IOSTAT=FSTATUS) TIMES,TIMED,TIMEYR,DELT,DELTD,DELTYR,AVGITER,CPUS,CPUHR,RNSTEP,RITERTOT,KBIN
#F  IF (FSTATUS /= 0) EXIT ! break loop
#F  
#F  !
#F  !  Read history variables, if there are any.
#F  !
#F  IF (MHIV .GT. 0) READ (8,IOSTAT=FSTATUS) (HVAR(I),I=1,MHIV)
#F  IF (FSTATUS /= 0) EXIT ! break loop
#F  
#F  WRITE (9,*)
#F  WRITE (9,*)
#F  WRITE (9,*) '** TIME (s,d,y), DELT (s,d,y), IterAvg, CPU (s,hr),' &
#F    //' Step No., IterTot, KBIN'
#F  WRITE (9,*) TIMES,TIMED,TIMEYR,DELT,DELTD,DELTYR,AVGITER,CPUS,CPUHR,RNSTEP,RITERTOT,KBIN
#F  WRITE (9,*)
#F  WRITE (9,*) '** History Vbls at Time Step',RNSTEP
#F  IF (MHIV .LE. 0) THEN
#F     WRITE (9,*) '   (no element-variable-based history variables)'
#F  ELSE
#F     WRITE (9,*) (HVAR(I),I=1,MHIV)
#F  END IF
#F  

#!!pystart
###############################################################################
## Create datatypes and allocate arrays for time-varying variables
###############################################################################
# First create the datatypes for the simulation and mass balance statistics tables

# Allocate the simulation stats table
#dtype_simulation_statistics_table = np.dtype([ ('times',fortran_double), ('timed',fortran_double), ('timeyr',fortran_double), ('delt',fortran_double), ('deltd',fortran_double), ('deltyr',fortran_double), 
#                                          ('avgiter',fortran_double), ('cpus',fortran_double), ('cpuhr',fortran_double), ('rnstep',fortran_double), ('ritertot',fortran_double), ('kbin', fortran_integer) ]);
dtype_simulation_statistics_table = np.dtype([ ('TIMESEC',fortran_double), ('TIMEDAY',fortran_double), ('TIMEYR',fortran_double), ('DELT',fortran_double), ('DELTDAY',fortran_double), ('DELTYR',fortran_double), 
                                          ('ITERAVG',fortran_double), ('CPUTIME',fortran_double), ('CPUTOT',fortran_double), ('STEPNUM',fortran_double), ('ITERTOT',fortran_double), ('KBIN', fortran_integer) ]);


simulation_statistics_table = np.zeros((1,), dtype=dtype_simulation_statistics_table); # allocate table


#dtype_mass_balance_table = np.dtype([ ('bbalc',fortran_double), ('gbalc',fortran_double), ('bbalmx',fortran_double), ('gbalmx',fortran_double), ('ibbalmx',fortran_double), ('jbbalmx',fortran_double), 
#                                          ('kbbalmx',fortran_double), ('igbalmx',fortran_double), ('jgbalmx',fortran_double), ('kgbalmx',fortran_double) ]);
dtype_mass_balance_table = np.dtype({ 'names':mass_balance_labels_table['namvargl'].tolist(), 'formats':[fortran_double for ii in range(ngvar)] });
mass_balance_table = np.zeros((1,), dtype=dtype_mass_balance_table); # allocate table

if (mhiv > 0):
  dtype_history_variables = np.dtype({ 'names':history_variables_labels_table['namvarhi'][mhivm:].tolist(), 'formats':[fortran_double for ii in range(mhiv)] });
  history_variables_table = np.zeros((1,), dtype=dtype_history_variables); #allocate table

# arrays for reading history and element data
yy = np.zeros((nx*ny*nz,), dtype=fortran_double);

# Create and allocate chunked space for the simulation statistics tables
chunksize_print_timestep = 1000;
chunksize_print_elements = 100;

# h5 output
if (output_options['hdf5']):
  
  h5grp_output_tables = fh_h5_snapshot['/output_tables'];
  h5data_simulation_statistics_table = h5grp_output_tables.create_dataset('simulation_statistics_table', dtype=dtype_simulation_statistics_table, shape=(chunksize_print_timestep,), maxshape=(None,), chunks=(1,));
  
  if (mhiv > 0):
    h5grp_output_tables = fh_h5_snapshot['/output_tables'];
    # h5data_history_variables = h5grp_output_tables.create_dataset('history_table', dtype=fortran_double, shape=(chunksize_print_timestep,mhiv), maxshape=(None,mhiv), chunks=(1,mhiv));
    h5data_history_variables = h5grp_output_tables.create_dataset('history_variables_table', dtype=dtype_history_variables, shape=(chunksize_print_timestep,), maxshape=(None,), chunks=(1,));
  
  h5grp_output_tables = fh_h5_snapshot['/output_tables'];
  h5data_mass_balance_table = h5grp_output_tables.create_dataset('mass_balance_table', dtype=dtype_mass_balance_table, shape=(chunksize_print_elements,), maxshape=(None,), chunks=(1,));
  
  # # create datasets for element arrays
  # h5grp_element_variables = fh_h5_snapshot['/element_variables'];
  # for l in range(nvarel):
  #   namevarel = str.strip( element_variables_labels_table['namvarel'][l] ); # element variable name
  #   h5data_element_variable = h5grp_element_variables.create_dataset(namevarel, dtype=fortran_double, shape=(chunksize_print_elements,nx*ny*nz), maxshape=(None,nx*ny*nz), chunks=(1,nx*ny*nz));
  
  # create arrays to hold time and timestep values for the element snapshots
  # create an array to hold references to the snapshot groups, ordered in time
  h5grp_output_times = fh_h5_snapshot['/output_times'];
  h5grp_output_times.create_dataset('time_s', dtype=fortran_double, shape=(chunksize_print_elements,), maxshape=(None,), chunks=(1,));
  h5grp_output_times.create_dataset('time_y', dtype=fortran_double, shape=(chunksize_print_elements,), maxshape=(None,), chunks=(1,));
  h5grp_output_times.create_dataset('timestep', dtype=fortran_double, shape=(chunksize_print_elements,), maxshape=(None,), chunks=(1,));
  h5grp_output_times.create_dataset('snapshot_refs', dtype=h5py.special_dtype(ref=h5py.Reference), shape=(chunksize_print_elements,), maxshape=(None,), chunks=(1,));

###############################################################################
## Cycle through time steps
###############################################################################
idx_print_timestep = 0;  # counter for timesteps
idx_print_elements = 0;  # counter for the number of times the element variables are printed
npo = 0;
while True:
  
###############################################################################
## ( 5) Simulation statistics
###############################################################################
  # read from bin
  dummy_int = fh_bin.read(rec_marker_bytes);
  if (dummy_int == ''): break;  # exit loop if EOF
  simulation_statistics_table[0] = np.fromfile(fh_bin, dtype=dtype_simulation_statistics_table, count=1);
  dummy_int = fh_bin.read(rec_marker_bytes);
  
  # ascii output
  if (output_options['ascii']):
    fh_ascii.write('\n');
    fh_ascii.write('\n');
    fh_ascii.write(b'** TIME (s,d,y), DELT (s,d,y), IterAvg, CPU (s,hr), Step No., IterTot, KBIN' + '\n');
    fh_ascii.write('%24.16E %24.16E %24.16E %24.16E %24.16E %24.16E %24.16E %24.16E %24.16E %24.16E %24.16E %20i' % tuple(simulation_statistics_table[0]) + '\n');
    fh_ascii.write('\n');
    
  # h5 output
  if (output_options['hdf5']):
    h5data_simulation_statistics_table[idx_print_timestep] = simulation_statistics_table[0];
    
  
###############################################################################
## ( 6) History Variables
###############################################################################
  
  # read from bin
  # Read history variables, if there are any
  if (mhiv > 0):
    dummy_int = fh_bin.read(rec_marker_bytes);
    history_variables_table[0] = np.fromfile(fh_bin, dtype=dtype_history_variables, count=1);
    dummy_int = fh_bin.read(rec_marker_bytes);
  
  # ascii output
  if (output_options['ascii']):
    fh_ascii.write(b'** History Vbls at Time Step %24.16E' % simulation_statistics_table['STEPNUM'][0] + '\n');
    if (mhiv <= 0):
      fh_ascii.write(b'   (no element-variable-based history variables)' + '\n');
    else:
      #history_variables_table[0].tofile(fh_ascii, sep=' ', format='%24.16E');
      fh_ascii.write('%24.16E '*mhiv % tuple(history_variables_table[0]) );
      fh_ascii.write('\n');
    
  # h5 output
  if (output_options['hdf5']):
    if (mhiv > 0):
      h5data_history_variables[idx_print_timestep] = history_variables_table[0];
  
#!!pyend

#F  !
#F  ! (History vbls only, if KBIN=0)
#F  !
#F  IF (KBIN .EQ. 1) THEN
#F  !
#F  ! Global vbls (mass balance info at current step) are read next:
#F  !   BBALC   = Brine mass balance error over entire mesh [kg].
#F  !   GBALC   = Gas mass balance error over entire mesh [kg].
#F  !   BBALMX  = Max relative brine mass bal error in any grid block.
#F  !   GBALMX  = Max relative gas mass bal error in any grid block.
#F  !   IBBALMX = I-index of grid block with BBALMX \
#F  !   JBBALMX = J-index of grid block with BBALMX  |  These are
#F  !   KBBALMX = K-index of grid block with BBALMX  |_ saved as
#F  !   IGBALMX = I-index of grid block with GBALMX  |  REAL vbls
#F  !   JGBALMX = J-index of grid block with GBALMX  |
#F  !   KGBALMX = K-index of grid block with GBALMX /
#F  !
#F     READ (8,IOSTAT=FSTATUS) BBALC,GBALC,BBALMX,GBALMX,IBBALMX,JBBALMX,KBBALMX,IGBALMX,JGBALMX,KGBALMX
#F     IF (FSTATUS /= 0) EXIT ! break loop
#F     
#F     NPO = NPO + 1
#F     WRITE (9,*)
#F     WRITE (9,*) '** Mass Balances (Global Vbls) Printout No.',NPO
#F     WRITE (9,*) BBALC,GBALC,BBALMX,GBALMX
#F     WRITE (9,*) IBBALMX,JBBALMX,KBBALMX,IGBALMX,JGBALMX,KGBALMX
#F     
#F     !
#F     ! The Element vbl distributions are read next.
#F     !
#F     DO L=1,NVAREL
#F        READ  (8,IOSTAT=FSTATUS) (((YY(I,J,K),I=1,NX),J=1,NY),K=1,NZ)
#F        IF (FSTATUS /= 0) EXIT ! break loop
#F        WRITE (9,*)
#F        WRITE (9,'(A,I3,3X,A,F10.1)') ' ** Element Vbl: (No., Name,' &
#F          //'Time Step No.) =',L,NAMVAREL(L),RNSTEP
#F        WRITE (9,*) (((YY(I,J,K),I=1,NX),J=1,NY),K=1,NZ)
#F     END DO
#F  END IF

#!!pystart
  # The mass balance and the snapshot variables are only printed at user-specified times
  # The internal kbin flag indicates whether-or-not the current timestep corresponds to one of those times.
  if (simulation_statistics_table['KBIN'][0] == 1):
###############################################################################
## ( 7) Global Mass Balance Variables
###############################################################################
    
    # read from bin
    dummy_int = fh_bin.read(rec_marker_bytes);
    mass_balance_table = np.fromfile(fh_bin, dtype=dtype_mass_balance_table, count=1);
    dummy_int = fh_bin.read(rec_marker_bytes);
    
    npo = npo+1;
    
    # ascii output
    if (output_options['ascii']):
      fh_ascii.write('\n');
      fh_ascii.write(b'** Mass Balances (Global Vbls) Printout No. %20i' % npo + '\n');
      fh_ascii.write('%24.16E %24.16E %24.16E %24.16E' % tuple(mass_balance_table[list(dtype_mass_balance_table.names[0:4])][0]) + '\n');
      fh_ascii.write('%24.16E %24.16E %24.16E %24.16E %24.16E %24.16E' % tuple(mass_balance_table[list(dtype_mass_balance_table.names[4:])][0]) + '\n');
      # fh_ascii.write('\n');
    
    # h5 output
    if (output_options['hdf5']):
      h5data_mass_balance_table[idx_print_elements] = mass_balance_table[0];
    
###############################################################################
## ( 8) Element Variables (Snapshots)
###############################################################################
    
    # h5 output
    if (output_options['hdf5']):
      # Create a group for the current time snapshot 
      
      if (output_options['snapshot_timeunit'] == 'y'):
        printtime = simulation_statistics_table['TIMEYR'][0];
        if (output_options['round_time']): printtime = np.round(printtime);
        snapshot_group_name = b'Time:  ' + b'%.14E' % printtime + b' y';
      elif (output_options['snapshot_timeunit'] == 's'):
        printtime = simulation_statistics_table['TIMESEC'][0];
        if (output_options['round_time']): printtime = np.round(printtime);
        snapshot_group_name = b'Time:  ' + b'%.14E' % printtime + b' s';
      elif (output_options['snapshot_timeunit'] == 'd'):
        printtime = simulation_statistics_table['TIMEDAY'][0];
        if (output_options['round_time']): printtime = np.round(printtime);
        snapshot_group_name = b'Time:  ' + b'%.14E' % printtime + b' d';
      
      #print(snapshot_group_name);
      h5grp_current_snapshot = fh_h5_snapshot.create_group(snapshot_group_name);
      # Annotate the group with the time/timestep data
      h5grp_current_snapshot.attrs.create('time_s', data=simulation_statistics_table['TIMESEC'][0], dtype=fortran_double);
      h5grp_current_snapshot.attrs.create('time_y', data=simulation_statistics_table['TIMEYR'][0], dtype=fortran_double);
      h5grp_current_snapshot.attrs.create('timestep', data=simulation_statistics_table['STEPNUM'][0], dtype=fortran_double);
      
      # Append time information to the time arrays
      h5grp_output_times['time_s'][idx_print_elements] = simulation_statistics_table['TIMESEC'][0];
      h5grp_output_times['time_y'][idx_print_elements] = simulation_statistics_table['TIMEYR'][0];
      h5grp_output_times['timestep'][idx_print_elements] = simulation_statistics_table['STEPNUM'][0];
      h5grp_output_times['snapshot_refs'][idx_print_elements] = h5grp_current_snapshot.ref;
    
    
    # Loop over each element variable
    for l in range(nvarel):
      # read from bin
      dummy_int = fh_bin.read(rec_marker_bytes);
      yy = np.fromfile(fh_bin, dtype=fortran_double, count=nx*ny*nz);
      dummy_int = fh_bin.read(rec_marker_bytes);
      
      # ascii output
      if (output_options['ascii']):
        fh_ascii.write('\n');
        fh_ascii.write(b' ** Element Vbl: (No., Name, Time Step No.) =%3i   %s %10.1F' % (l+1, element_variables_labels_table['namvarel'][l], simulation_statistics_table['STEPNUM'][0]) + '\n');
        yy.tofile(fh_ascii, sep=' ', format='%24.16E');
        fh_ascii.write('\n');
      
      # h5 output
      if (output_options['hdf5']):
        namevarel = str.strip( element_variables_labels_table['namvarel'][l] ); # element variable name
        # h5grp_element_variables[namevarel][idx_print_elements, :] = yy; # output in chunked array
        # note, we both rearrange to account for Fortran order, and we swap the y and z dimensions for Paraview
        if (output_options['plane']=='xy'):
          h5data_element_variable = h5grp_current_snapshot.create_dataset(namevarel, dtype=fortran_double, shape=(nx,ny,nz), data=yy.reshape((nx,ny,nz), order='F') );
        if (output_options['plane']=='xz'):
          h5data_element_variable = h5grp_current_snapshot.create_dataset(namevarel, dtype=fortran_double, shape=(nx,nz,ny), data=yy.reshape((nx,ny,nz), order='F').transpose((0,2,1)) );
      
    # end nvarel loop
    # increment kbin counter
    idx_print_elements = idx_print_elements+1;
    
  # end kbin if
  # increment timestep counter
  idx_print_timestep = idx_print_timestep+1;
  
  # resize chunked hdf5 datasets if larger than preallocation
  # h5 output
  if (output_options['hdf5']):
    
    if (idx_print_timestep % chunksize_print_timestep == 0):
      h5data_simulation_statistics_table.resize( (h5data_simulation_statistics_table.shape[0]+chunksize_print_timestep,) );
      if (mhiv > 0): h5data_history_variables.resize( (h5data_history_variables.shape[0]+chunksize_print_timestep,) );
      
    if (idx_print_elements % chunksize_print_elements == 0):
      h5data_mass_balance_table.resize( (h5data_mass_balance_table.shape[0]+chunksize_print_elements,) );
      h5grp_output_times['time_s'].resize( (h5grp_output_times['time_s'].shape[0]+chunksize_print_elements,) );
      h5grp_output_times['time_y'].resize( (h5grp_output_times['time_y'].shape[0]+chunksize_print_elements,) );
      h5grp_output_times['timestep'].resize( (h5grp_output_times['timestep'].shape[0]+chunksize_print_elements,) );
      h5grp_output_times['snapshot_refs'].resize( (h5grp_output_times['snapshot_refs'].shape[0]+chunksize_print_elements,) );
      
      # for l in range(nvarel):
      #   namevarel = str.strip( element_variables_labels_table['namvarel'][l] ); # element variable name
      #   h5grp_element_variables[namevarel].resize( (h5grp_element_variables[namevarel].shape[0]+chunksize_print_elements, nx*ny*nz) );
      
# end while

#!!pyend

#F  !
#F  ! Continue to next time step.
#F  !
#F  CONTINUE
#F  END DO ! end file loop
#F  
#F  CLOSE (8) ! close the binary file
#F  CLOSE (9) ! close the ascii file
#F  STOP '** Normal Completion **'

#!!pystart

# trim the hdf5 datasets when finished looping
# h5 output
if (output_options['hdf5']):
  h5data_simulation_statistics_table.resize((idx_print_timestep,));
  if (mhiv > 0): h5data_history_variables.resize((idx_print_timestep,));
  
  h5data_mass_balance_table.resize((idx_print_elements,));
  
  h5grp_output_times['time_s'].resize((idx_print_elements,));
  h5grp_output_times['time_y'].resize((idx_print_elements,));
  h5grp_output_times['timestep'].resize((idx_print_elements,));
  h5grp_output_times['snapshot_refs'].resize((idx_print_elements,));
  
  # for l in range(nvarel):
  #   namevarel = str.strip( element_variables_labels_table['namvarel'][l] ); # element variable name
  #   h5grp_element_variables[namevarel].resize((idx_print_elements,nx*ny*nz));

#!!pyend

#!!pystart

###############################################################################
## Close files
###############################################################################
fh_bin.close();

# ascii output
if (output_options['ascii']):
  fh_ascii.close();

# h5 output
if (output_options['hdf5']):
  fh_h5_snapshot.close();

#!!pyend

###############################################################################
## Create Post-Processed Arrays
###############################################################################
if (output_options['hdf5']):
  if (output_options['create_pcgw']):
    
    # Re-open the h5 file, this time read/write
    fh_h5_snapshot = h5py.File(fname_h5_snapshot_output, mode='r+');
    
    # Check that PRESBRIN and PRESGAS arrays exist in the h5 file
    snapshot_refs = fh_h5_snapshot['/output_times/snapshot_refs'];
    
    if('PRESBRIN' in fh_h5_snapshot[snapshot_refs[0]] and 'PRESGAS' in fh_h5_snapshot[snapshot_refs[0]]):
      # Loop over times and create PCGW variable in each snapshot folder
      for h5grp_ref in snapshot_refs:
        #fh_h5_snapshot[h5grp_ref].create_dataset('PCGW', dtype=fortran_double, data=fh_h5_snapshot[h5grp_ref]['PRESGAS']-fh_h5_snapshot[h5grp_ref]['PRESBRIN'] );
        fh_h5_snapshot[h5grp_ref]['PCGW'] = fh_h5_snapshot[h5grp_ref]['PRESGAS'][...]-fh_h5_snapshot[h5grp_ref]['PRESBRIN'][...] ;
    else:
      print('PRESBRIN and PRESGAS are not available to calculate PCGW');
  
      fh_h5_snapshot.close();


#F END PROGRAM
#F !*****!***************************************************************************************************************************!
