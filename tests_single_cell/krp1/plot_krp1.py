#!/bin/python
import matplotlib.pyplot as plt
import sys
import os
import argparse
import numpy as np
import h5py


sl = ['06','08','10','25','35','50','75','85',
      '9400','9410','9420','9430','9440',
      '9450','9451','9452','9453',
      '950','99']
sg = ['94','92','90','75','65','50','25','15',
      '0600','0590','0580','0570','0560',
      '0550','0549','0548','0547',
      '050','01']
sl_plot = [0.06,0.08,0.10,0.25,0.35,0.50,0.75,0.85,
           0.9400,0.9410,0.9420,0.9430,0.9440,
           0.9450,0.9451,0.9452,0.9453,
           0.950,0.99]
pf_pc = np.zeros(len(sl))
pf_krl = np.zeros(len(sl))
pf_krg = np.zeros(len(sl))

bf_pc = np.zeros(len(sl))
bf_krl = np.zeros(len(sl))
bf_krg = np.zeros(len(sl))



for jj in range(len(sl)):
  deckname = 'krp1_' + sl[jj];
  bf_deck = 'bf_'+ deckname
  pf_deck = 'pf_' + deckname

  command_line = 'python generate_input_decks.py ' + deckname + ' ' \
    + sl[jj] + ' ' + sg[jj]
  os.system(command_line)
  command_line = '$BRAGFLO_DIR/BRAGFLO/Source/bragflo -input ' + bf_deck + \
    '.inp -binary ' + bf_deck + '.bin -output ' + bf_deck + \
    '.out -summary ' + bf_deck + '.sum -rout ' + bf_deck + '.rout #> ' + \
    bf_deck + '.stdout'
  os.system(command_line)
  command_line = 'python $BINREAD_DIR/binread.py --plane xy --postarray pcgw --binary ' \
    + bf_deck + '.bin --hdf5 ' + bf_deck + '.h5 --unitgrid true --snapshot_timeunit y --round_time false --ascii ' + bf_deck + '.txt'
  os.system(command_line)
  command_line = '$PFLOTRAN_SRC/pflotran -input_prefix ' + pf_deck + ' > ' \
    + bf_deck + '.stdout'
  os.system(command_line)
  

k = 0
for jj in range(len(sl)):
  deckname = 'krp1_' + sl[jj];
  bf_h5_filename = 'bf_' + deckname + '.h5'
  pf_h5_filename = 'pf_' + deckname + '.h5'
  
  # Read bragflo h5 file
  try:
    fh_bf_h5 = h5py.File(bf_h5_filename, mode='r');
    print bf_h5_filename + ' found.'
  except IOError:
    print bf_h5_filename + ' is missing!'
    
  # Read pflotran h5 file
  try:
    fh_pf_h5 = h5py.File(pf_h5_filename, mode='r');
    print pf_h5_filename + ' found.'
  except IOError:
    print pf_h5_filename + ' is missing!'

  bf_pc[k] = fh_bf_h5['Time:  1.00000000000000E+00 y']['PCGW'][0]
  bf_krl[k] = fh_bf_h5['Time:  1.00000000000000E+00 y']['RELPERMB'][0]
  bf_krg[k] = fh_bf_h5['Time:  1.00000000000000E+00 y']['RELPERMG'][0]
  pf_pc[k] = fh_pf_h5['Time:  1.00000E+00 y']['Capillary_Pressure [Pa]'][0]
  pf_krl[k] = fh_pf_h5['Time:  1.00000E+00 y']['Liquid_Mobility [1_Pa-s]'][0]*(2.1000000e-3)
  pf_krg[k] = fh_pf_h5['Time:  1.00000E+00 y']['Gas_Mobility [1_Pa-s]'][0]*(8.9338900e-6)
  
  k = k + 1
  
plt.semilogy(sl_plot,bf_pc,label='BRAGFLO')
plt.semilogy(sl_plot,pf_pc,'r--',label='PFLOTRAN')
plt.legend(loc='best')
plt.title('KRP1 Capillary Pressure')
plt.xlabel('Liquid Saturation')
plt.ylabel('Capillary Pressure [Pa]')
#plt.show()
fig_name = 'capillary_pressure.png'
plt.savefig(fig_name, dpi=300)
plt.close()

plt.plot(sl_plot,bf_krl,label='BRAGFLO')
plt.plot(sl_plot,pf_krl,'r--',label='PFLOTRAN')
plt.legend(loc='best')
plt.title('KRP1 Relative Permeability')
plt.xlabel('Liquid Saturation')
plt.ylabel('Liquid Rel. Permeability')
#plt.show()
fig_name = 'krl.png'
plt.savefig(fig_name, dpi=300)
plt.close()

plt.plot(sl_plot,bf_krg,label='BRAGFLO')
plt.plot(sl_plot,pf_krg,'r--',label='PFLOTRAN')
plt.legend(loc='best')
plt.title('KRP1 Relative Permeability')
plt.xlabel('Liquid Saturation')
plt.ylabel('Gas Rel. Permeability')
#plt.show()
fig_name = 'krg.png'
plt.savefig(fig_name, dpi=300)
plt.close()

abs_diff = pf_pc - bf_pc

#plt.plot(sl_plot,abs_diff)
#plt.xlabel('Liquid Saturation')
#plt.ylabel('Abs. Difference Pc [Pa]')
#plt.show()
#fig_name = 'capillary_pres_diff.png'
#plt.savefig(fig_name, dpi=300)

command_line = 'rm *.out *.rec *.bin *.tec *.sum *.rout *.txt *.stdout *~'
os.system(command_line)
command_line = 'rm *.in *.inp *.h5'
os.system(command_line)
