#!/bin/python
import matplotlib
matplotlib.use('Agg') # need on jt if no display
import matplotlib.pyplot as plt
import sys
import os
import argparse
import numpy as np
import h5py

# for smoothing of the error when rates change suddenly
def moving_average(a,N) : 
  padded = np.pad(a,(N//2,N-1-N//2),mode='edge')
  smoothed = np.convolve(padded,np.ones((N,))/N,mode='valid')
  return smoothed

# Fixed parameters
VISGAS=8.93389E-06;
VISBRINE=2.10000E-03;
PERM1=2.399938E-13;
PERM2=1.047981E-19;
SECYR = 3.17097919837646E-08;
YRSEC = 3.153600E+07;

#####################################################
tpl_pf_tecplot_header = (
'TIME', 'PRESBRIN', 'PRESGAS', 'POROS', 
'RELPERMB', 'RELPERMG', 'DENBRINE', 'DENGAS', 
'PERMBRX', 'PERMGASX', 'SATBRINE', 'SATGAS'
);
#####################################################
tpl_pf_pnl_header = (
'TIME', 'PNL', 'CORRATI', 'CORRATH', 
'BIORATI', 'BIORATH', 'FEOH2_SR', 'FE_SR', 
'MGO_HR', 'MGOH2_CR', 'MGO_CR', 'HYMAG_CR', 
'H2RATE', 'BRINRATE', 'FERATE', 'CELLRATE', 
'FEOH2R', 'FESR', 'MGOR', 'MGOH2R', 
'HYMAGR', 'MGCO3R', 'FECONC', 'CELLCONC', 
'FEOH2C', 'FESC', 'MGOC', 'MGOH2C', 
'HYMAGC', 'MGCO3C', 'PORSOLID'
);
#####################################################
tpl_bf_hist_header = (
'PRESBRIN', 'PRESGAS', 'POROS', 'RELPERMB',
'RELPERMG', 'DENBRINE', 'DENGAS', 'SATBRINE', 
'SATGAS', 'CORRATI', 'CORRATH', 'BIORATI', 
'BIORATH', 'FEOH2_SR', 'FE_SR', 'MGO_HR', 
'MGOH2_CR', 'MGO_CR', 'HYMAG_CR', 'H2RATE', 
'BRINRATE', 'FERATE', 'CELLRATE', 'FEOH2R', 
'FESR', 'MGOR', 'MGOH2R', 'HYMAGR', 
'MGCO3R', 'FECONC', 'CELLCONC', 'FEOH2C', 
'FESC', 'MGOC', 'MGOH2C', 'HYMAGC', 
'MGCO3C', 'PORSOLID'
);
#####################################################
dic_bf_hist_header = {
'PRESBRIN' : 'H0010001', 'PRESGAS' : 'H0020001', 
'POROS' : 'H0060001', 'RELPERMB' : 'H0070001', 
'RELPERMG' : 'H0080001', 'DENBRINE' : 'H0090001', 
'DENGAS' : 'H0100001', 'PERMBRX' : 'H0110001', 
'PERMGASX' : 'H0140001', 'SATBRINE' : 'H0170001', 
'SATGAS' : 'H0180001', 'CORRATI' : 'H0390001', 
'CORRATH' : 'H0400001', 'BIORATI' : 'H0410001', 
'BIORATH' : 'H0420001', 'FEOH2_SR' : 'H0430001', 
'FE_SR' : 'H0440001', 'MGO_HR' : 'H0450001', 
'MGOH2_CR' : 'H0460001', 'MGO_CR' : 'H0470001', 
'HYMAG_CR' : 'H0480001', 'H2RATE' : 'H0490001', 
'BRINRATE' : 'H0500001', 'FERATE' : 'H0510001', 
'CELLRATE' : 'H0520001', 'FEOH2R' : 'H0530001', 
'FESR' : 'H0540001', 'MGOR' : 'H0550001', 
'MGOH2R' : 'H0560001', 'HYMAGR' : 'H0570001', 
'MGCO3R' : 'H0580001', 'FECONC' : 'H0590001', 
'CELLCONC' : 'H0600001', 'FEOH2C' : 'H0610001', 
'FESC' : 'H0620001', 'MGOC' : 'H0630001', 
'MGOH2C' : 'H0640001', 'HYMAGC' : 'H0650001', 
'MGCO3C' : 'H0660001', 'PORSOLID' : 'H0680001'
};
#####################################################

# Convert tuple of column headings to an indexed dict
dic_pf_tecplot_header = { k: i for i, k in enumerate(tpl_pf_tecplot_header) };
dic_pf_pnl_header = { k: i for i, k in enumerate(tpl_pf_pnl_header) };

# List of deck files to run
DECK_FILES=[
  'case060100_0d_gas_injection', 
  'case060200_0d_gas_generation_midsat', 
  'case060210_0d_gas_generation_hisat', 
  'case060220_0d_gas_generation_lowsat', 
  'case060220_0d_gas_generation_superlowsat', 
  'case060230_0d_gas_generation_corbio_midsat', 
  'case060240_0d_gas_generation_corbio_hisat', 
  'case060250_0d_gas_generation_corbio_lowsat', 
  'case060300_0d_creep_static', 
  'case060310_0d_creep_gas_injection', 
  'case060320_0d_creep_gas_generation', 
  'case060400_0d_porecomp_gas_injection', 
  'case060500_0d_fracture_gas_injection', 
  'case060600_0d_klinkenberg_gas_injection', 
  'case060700_0d_rks_calc_gas_injection'
];

DECK_DIR=[
  'case060100',
  'case060200','case060200','case060200','case060200',
  'case060200','case060200','case060200',
  'case060300','case060300','case060300',
  'case060400',
  'case060500',
  'case060600',
  'case060700'
  ];

DECK_TITLES=[
  'Gas Injection', 
  'Gas Generation, Swi=0.50', 
  'Gas Generation, Swi=0.98', 
  'Gas Generation, Swi=0.20', 
  'Gas Generation, Swi=0.05', 
  'Gas Generation, No MgO Hydration, Swi=0.50', 
  'Gas Generation, No MgO Hydration, Swi=0.98', 
  'Gas Generation, No MgO Hydration, Swi=0.20', 
  'Creep Closure, Static Conditions', 
  'Creep Closure, Gas Injection', 
  'Creep Closure, Gas Generation', 
  'Pore Compressibility with Gas Injection', 
  'MB Fracturing with Gas Injection', 
  'Klinkenberg Gas Permeability with Gas Injection', 
  'RKS Gas EOS with Gas Injection'
];




for jj in range(len(DECK_FILES)):
  deckname = DECK_FILES[jj];
  decktitle = DECK_TITLES[jj];
  deck_dir = DECK_DIR[jj];
  
  print '\n============ ' + deck_dir + ' ============'
  os.chdir(deck_dir)
  
  #############################################################################
  # Read in the data
  #############################################################################
  tec1_filename =  'pf_' + deckname + '-obs-0.tec';
  pnl1_filename =  'pf_' + deckname + '-0.pnl';
  bf_h5_filename = 'bf_' + deckname + '.h5';
  
  # Read pflotran tecplot file
  try:
    pf_tecplot_data = np.loadtxt(tec1_filename, dtype='float64', skiprows=1);
    print tec1_filename
    skip_this_test = False
  except IOError:
    print tec1_filename + ' is missing!'
    skip_this_test = True
  
  # Read bragflo h5 file
  if not skip_this_test:
    try:
      fh_bf_h5 = h5py.File(bf_h5_filename, mode='r');
      print bf_h5_filename
      skip_this_test = False
    except IOError:
      print bf_h5_filename + ' is missing!'
      skip_this_test = True
    
  # Read pflotran pnl file (may not exist if no gas generation)
  try:
    pf_pnl_data = np.loadtxt(pnl1_filename, dtype='float64', skiprows=1);
    print pnl1_filename
    gas_generation = True
  except IOError:
    print pnl1_filename + ' is missing!'
    gas_generation = False
    
  if not skip_this_test:
    #############################################################################
    ## Plot the data
    #############################################################################
    print deckname + ' test:'
    
    # Set some defaults plot values
    plt.rcParams['figure.dpi'] = 300;
    plt.rcParams['xtick.labelsize'] = 9;
    plt.rcParams['ytick.labelsize'] = 9;
    plt.rcParams['axes.labelsize'] = 10;
    plt.rcParams['axes.titlesize'] = 10;
    plt.rcParams['legend.fontsize'] = 10;
    
    major_ticks_y = [0,1.0,10.0,100.0] 
    minor_ticks_y = [0.25,0.50,0.75,2.5,5.0,7.5,25.0,50.0,75.0]
    
    SECYR = 3.17097919837646E-08;
    YRSEC = 3.153600E+07;
    
    bf_data = [];  bf_data.append([]);
    pf_data = [];  pf_data.append([]);
    pf_data_at_bf_times = [];  pf_data_at_bf_times.append([]);
    error = [];  error.append([])
    title = []; title.append([])
    # get time
    bf_data[0] = fh_bf_h5['/output_tables/simulation_statistics_table']['TIMESEC'] / YRSEC
    pf_data[0] = pf_tecplot_data[:, dic_pf_tecplot_header['TIME']] / YRSEC
    bf_data.append([]); pf_data.append([]);          # hold space for pnl time array
    pf_data_at_bf_times.append([]); error.append([]) # hold space for pnl time array
    title.append([])                                 # hold space for pnl time array

    num_tecplot_plots = 0
    k = 2
    # load data from tecplot & hdf5
    print 'READING TECPLOT DATA. . . . '
    for var in tpl_pf_tecplot_header:
      if var == 'TIME' or var == 'RELPERMB' or var == 'RELPERMG':
        continue
      title.append([]); title[k] = var
      # get bragflo variable array 
      bf_data.append([])
      bf_data[k] = fh_bf_h5['/output_tables/history_variables_table'][dic_bf_hist_header[var]];
      # get pflotran variable array
      pf_data.append([])
      pf_data[k] = pf_tecplot_data[:, dic_pf_tecplot_header[var]]
      # interpolate pflotran data at bragflo times
      pf_data_at_bf_times.append([])
      pf_data_at_bf_times[k] = np.interp(bf_data[0],pf_data[0],pf_data[k])
      # calculate difference
      error.append([])
      error_bf_data = bf_data[k]
      error_pf_data = pf_data_at_bf_times[k]
      for g in range(len(error_bf_data)):
        if ((error_bf_data[g]<1.0e-20) and (error_bf_data[g]>=0.0)):
          error_bf_data[g] = 1.0e-20
        if ((error_bf_data[g]>-1.0e-20) and (error_bf_data[g]<0.0)):
          error_bf_data[g] = -1.0e-20
      for g in range(len(error_pf_data)):
        if ((error_pf_data[g]<1.0e-20) and (error_pf_data[g]>=0.0)):
          error_pf_data[g] = 1.0e-20
        if ((error_pf_data[g]>-1.0e-20) and (error_pf_data[g]<0.0)):
          error_pf_data[g] = -1.0e-20
      error[k] = 100.0*abs((error_bf_data - error_pf_data)/error_bf_data)
      k = k + 1
      num_tecplot_plots = num_tecplot_plots + 1
      
    # load data from pnl and hdf5 (don't reset k iterator)
    if gas_generation:
      print 'READING PNL (GAS GENERATION) DATA. . . . '
      # get time
      bf_data[1] = fh_bf_h5['/output_tables/simulation_statistics_table']['TIMESEC'] / YRSEC
      pf_data[1] = pf_pnl_data[1:, dic_pf_pnl_header['TIME']] / YRSEC
      num_pnl_plots = 0
      for var in tpl_pf_pnl_header:
        if var == 'TIME' or var == 'PNL':
          continue
        title.append([]); title[k] = var
        # get bragflo variable array 
        bf_data.append([])
        bf_data[k] = fh_bf_h5['/output_tables/history_variables_table'][dic_bf_hist_header[var]];
        # get pflotran variable array
        pf_data.append([])
        pf_data[k] = pf_pnl_data[1:, dic_pf_pnl_header[var]]
        # interpolate pflotran data at bragflo times
        pf_data_at_bf_times.append([])
        pf_data_at_bf_times[k] = np.interp(bf_data[1],pf_data[1],pf_data[k])
        # calculate difference
        error.append([])
        error_bf_data = bf_data[k]
        error_pf_data = pf_data_at_bf_times[k]
        for g in range(len(error_bf_data)):
          if ((error_bf_data[g]<1.0e-20) and (error_bf_data[g]>=0.0)):
            error_bf_data[g] = 1.0e-20
          if ((error_bf_data[g]>-1.0e-20) and (error_bf_data[g]<0.0)):
            error_bf_data[g] = -1.0e-20
        for g in range(len(error_pf_data)):
          if ((error_pf_data[g]<1.0e-20) and (error_pf_data[g]>=0.0)):
            error_pf_data[g] = 1.0e-20
          if ((error_pf_data[g]>-1.0e-20) and (error_pf_data[g]<0.0)):
            error_pf_data[g] = -1.0e-20
        error[k] = 100.0*abs((error_bf_data - error_pf_data)/error_bf_data)
        k = k + 1
        num_pnl_plots = num_pnl_plots + 1
        
    print 'PLOTTING TECPLOT DATA. . . . '
    for j in range(num_tecplot_plots):
      j = j + 2
      fig101,axes101 = plt.subplots(2,sharex=True,figsize=(3.5, 4.0));
      fig_subname = str(j-1) + '_' + title[j]
      major_ticks_y = [0,1.0,10.0,100.0] 
      minor_ticks_y = [0.25,0.50,0.75,2.5,5.0,7.5,25.0,50.0,75.0]
      axes101[0].semilogx(bf_data[0],bf_data[j],'b-',alpha=0.7,label='BRAGFLO');
      axes101[0].semilogx(pf_data[0],pf_data[j],'r--',alpha=0.7,label='PFLOTRAN');
      axes101[0].set_xlabel('Time [yr]');
      axes101[0].set_ylabel(title[j], color='k');
      axes101[0].set_xlim([1.e-1,1.e4]);
      axes101[0].legend(loc='best');
      axes101[0].set_title(title[j]);  
      axes101[1].plot(bf_data[0],error[j],'r',alpha=0.7);
      axes101[1].set_xscale('symlog');
      axes101[1].set_yscale('symlog',linthreshy=1.0);
      axes101[1].set_xlabel('Time [yr]');
      axes101[1].set_ylabel('Difference [%]', color='k');
      axes101[1].set_xlim([1.e-1,1.e4]);
      axes101[1].set_ylim([0,1.e2]);
      axes101[1].legend(loc='best');
      axes101[1].set_title('Difference in ' + title[j]);     
      axes101[1].set_yticks(major_ticks_y);
      axes101[1].set_yticks(minor_ticks_y, minor=True);
      axes101[1].grid(which='major',alpha=1.0);
      axes101[1].grid(which='minor',alpha=0.3);
      # adjust layout
      fig101.tight_layout();
      fig101.subplots_adjust(top=0.90);
      # save the figure
      fig101.savefig(deckname+'_'+fig_subname+'.png', dpi=300);
      # toss stuff
      plt.close(fig101);
      del(fig101, axes101);
      
    if gas_generation:
      print 'PLOTTING PNL (GAS GENERATION) DATA. . . . '
      for j in range(num_pnl_plots):
        j = j + 2 + num_tecplot_plots
        fig101,axes101 = plt.subplots(2,sharex=True,figsize=(3.5, 4.0));
        fig_subname = str(j-1) + '_' + title[j]
        axes101[0].semilogx(bf_data[1],bf_data[j],'b-',alpha=0.7,label='BRAGFLO');
        axes101[0].semilogx(pf_data[1],pf_data[j],'r--',alpha=0.7,label='PFLOTRAN');
        axes101[0].set_xlabel('Time [yr]');
        axes101[0].set_ylabel(title[j], color='k');
        axes101[0].set_xlim([1.e1,1.e4]);
        axes101[0].legend(loc='best');
        axes101[0].set_title(title[j]);
        axes101[1].plot(bf_data[1],error[j],'r',alpha=0.7);
        axes101[1].set_xscale('symlog');
        axes101[1].set_yscale('symlog',linthreshy=1.0);
        axes101[1].set_xlabel('Time [yr]');
        axes101[1].set_ylabel('Difference [%]', color='k');
        axes101[1].set_xlim([1.e-1,1.e4]);
        axes101[1].set_ylim([0,1.e2]);
        axes101[1].legend(loc='best');
        axes101[1].set_title('Difference in ' + title[j]);   
        axes101[1].set_yticks(major_ticks_y);
        axes101[1].set_yticks(minor_ticks_y, minor=True);
        axes101[1].grid(which='major',alpha=1.0);
        axes101[1].grid(which='minor',alpha=0.3);
        # adjust layout
        fig101.tight_layout();
        fig101.subplots_adjust(top=0.90);
        # save the figure
        fig101.savefig(deckname+'_'+fig_subname+'.png', dpi=300);
        # toss stuff
        plt.close(fig101);
        del(fig101, axes101);
  else:
    print 'Skipping test!'
    
  # go back
  os.chdir('..')
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
