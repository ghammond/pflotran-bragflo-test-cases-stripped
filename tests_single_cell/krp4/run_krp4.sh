$BRAGFLO_DIR/BRAGFLO/Source/bragflo -input bf_krp4.inp -binary bf_krp4.bin -output bf_krp4.out -summary bf_krp4.sum -rout bf_krp4.rout #> bf_krp4.stdout
python $BINREAD_DIR/binread.py --plane xy --postarray pcgw --binary bf_krp4.bin --hdf5 bf_krp4.h5 --unitgrid true --snapshot_timeunit y --round_time false --ascii bf_krp4.txt
$BRAGFLO_DIR/BRAGFLO/Source/bragflo -input bf_krp4_95.inp -binary bf_krp4_95.bin -output bf_krp4_95.out -summary bf_krp4_95.sum -rout bf_krp4_95.rout #> bf_krp4_95.stdout
python $BINREAD_DIR/binread.py --plane xy --postarray pcgw --binary bf_krp4_95.bin --hdf5 bf_krp4_95.h5 --unitgrid true --snapshot_timeunit y --round_time false --ascii bf_krp4_95.txt
$BRAGFLO_DIR/BRAGFLO/Source/bragflo -input bf_krp4_94.inp -binary bf_krp4_94.bin -output bf_krp4_94.out -summary bf_krp4_94.sum -rout bf_krp4_94.rout #> bf_krp4_94.stdout
python $BINREAD_DIR/binread.py --plane xy --postarray pcgw --binary bf_krp4_94.bin --hdf5 bf_krp4_94.h5 --unitgrid true --snapshot_timeunit y --round_time false --ascii bf_krp4_94.txt
$BRAGFLO_DIR/BRAGFLO/Source/bragflo -input bf_krp4_50.inp -binary bf_krp4_50.bin -output bf_krp4_50.out -summary bf_krp4_50.sum -rout bf_krp4_50.rout #> bf_krp4_50.stdout
python $BINREAD_DIR/binread.py --plane xy --postarray pcgw --binary bf_krp4_50.bin --hdf5 bf_krp4_50.h5 --unitgrid true --snapshot_timeunit y --round_time false --ascii bf_krp4_50.txt
$BRAGFLO_DIR/BRAGFLO/Source/bragflo -input bf_krp4_08.inp -binary bf_krp4_08.bin -output bf_krp4_08.out -summary bf_krp4_08.sum -rout bf_krp4_08.rout #> bf_krp4_08.stdout
python $BINREAD_DIR/binread.py --plane xy --postarray pcgw --binary bf_krp4_08.bin --hdf5 bf_krp4_08.h5 --unitgrid true --snapshot_timeunit y --round_time false --ascii bf_krp4_08.txt
$BRAGFLO_DIR/BRAGFLO/Source/bragflo -input bf_krp4_06.inp -binary bf_krp4_06.bin -output bf_krp4_06.out -summary bf_krp4_06.sum -rout bf_krp4_06.rout #> bf_krp4_06.stdout
python $BINREAD_DIR/binread.py --plane xy --postarray pcgw --binary bf_krp4_06.bin --hdf5 bf_krp4_06.h5 --unitgrid true --snapshot_timeunit y --round_time false --ascii bf_krp4_06.txt

#if necessary
#export PFLOTRAN_SRC=$PFLOTRAN_DIR/src/pflotran
$PFLOTRAN_SRC/pflotran -input_prefix pf_krp4 > pf_krp4.stdout
$PFLOTRAN_SRC/pflotran -input_prefix pf_krp4_95 > pf_krp4_95.stdout
$PFLOTRAN_SRC/pflotran -input_prefix pf_krp4_94 > pf_krp4_94.stdout
$PFLOTRAN_SRC/pflotran -input_prefix pf_krp4_50 > pf_krp4_50.stdout
$PFLOTRAN_SRC/pflotran -input_prefix pf_krp4_08 > pf_krp4_08.stdout
$PFLOTRAN_SRC/pflotran -input_prefix pf_krp4_06 > pf_krp4_06.stdout

#python bfpf_compare.py bf_krp4 pf_krp4 3 5

