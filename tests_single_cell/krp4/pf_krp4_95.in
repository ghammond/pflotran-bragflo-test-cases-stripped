# PFLOTRAN input deck for krp1 single cell test problem
#==================================simulation==================================#
SIMULATION
  INPUT_RECORD_FILE #special WIPP feature
  SIMULATION_TYPE SUBSURFACE
  PROCESS_MODELS
    SUBSURFACE_FLOW flow
      MODE BRAGFLO
      OPTIONS
        EXTERNAL_FILE ../../block_options.txt
      /
    /
  / #end PROCESS_MODELS block
END

SUBSURFACE
#===================================datasets===================================#
#=====================================grid=====================================#
GRID
  TYPE STRUCTURED
  BOUNDS
    0. 0. 0.
    2. 2. 2.
  /
  NXYZ 1 1 1
  GRAVITY -0.000000 0.000000 -9.806650
END
#============================reference temperature=============================#
REFERENCE_TEMPERATURE 2.7000000d+01
#=====================================eos======================================#
EOS WATER
  DENSITY BRAGFLO 1.2200000d+03 1.0132500d+05 3.1000000d-10
  VISCOSITY CONSTANT 2.1000000d-03
END

EOS GAS
  DENSITY RKS
    USE_CUBIC_ROOT_SOLUTION #hardwired
    USE_EFFECTIVE_PROPERTIES
    HYDROGEN
    TC 4.3600000d+01
    PC 2.0470000d+06
    AC 0.0000000d+00
    A  4.2747000d-01
    B  8.6640000d-02
  /
  VISCOSITY CONSTANT 8.9338900d-06
END
#=================================klinkenberg==================================#
KLINKENBERG_EFFECT
  A -3.4100000d-01
  B 2.7100000d-01
END
#================================timestepper================================#
EXTERNAL_FILE ../../block_timestepper.txt
#================================solver options================================#
EXTERNAL_FILE ../../block_solver_options.txt
#====================================times=====================================#
TIME
  FINAL_TIME 1. y #1.0000000d+04 y
  INITIAL_TIMESTEP_SIZE 8.6400000d+00 s
  MAXIMUM_TIMESTEP_SIZE 5.500000e+01 y at 0.000000000e+00 y
END
#====================================output====================================#
OUTPUT
  SNAPSHOT_FILE
    FORMAT HDF5
    TIMES y 0.0 1.0 #2.0 3.0 4.0 5.0 6.0 7.0 8.0 9.0 \
#           10.0 20.0 30.0 40.0 50.0 60.0 70.0 80.0 90.0 100.\
#           200. 300. 349. 3.50000000029023d2 351. 400. 500. 549. 551. 600. 700. \
#           800. 900. 1000. 1549. 1551. 2000. 3000. 4000. 5000. 6000.\
#           7000. 8000. 9000. 10000.
  /
  OBSERVATION_FILE
    PERIODIC TIMESTEP 1
  /
  VARIABLES
    LIQUID_PRESSURE
    LIQUID_SATURATION
    LIQUID_DENSITY
    GAS_PRESSURE
    GAS_SATURATION
    GAS_DENSITY
    CAPILLARY_PRESSURE
    EFFECTIVE_POROSITY
    PERMEABILITY
    MATERIAL_ID
    VOLUME
  /
END
#==============================observation points==============================#
OBSERVATION all
  REGION all
END
#===================================regions====================================#
REGION all
  COORDINATES
    -1.d20 -1.d20 -1.d20
     1.d20  1.d20  1.d20
  /
END

#================================creep closure=================================#
#======================material properties & char curves=======================#
MATERIAL_PROPERTY S_MB139
  ID 3
  POROSITY 1.100000d-02
  TORTUOSITY 1.000000d+00
  SOIL_COMPRESSIBILITY_FUNCTION POROSITY_EXPONENTIAL
  POROSITY_COMPRESSIBILITY 2.027273d-09
  SOIL_REFERENCE_PRESSURE 1.d7
  PERMEABILITY
    PERM_X_LOG10 -1.897965d+01
    PERM_Y_LOG10 -1.897965d+01
    PERM_Z_LOG10 -1.897965d+01
  /
  WIPP-FRACTURE
    INITIATING_PRESSURE 2.000000d+05
    ALTERED_PRESSURE 3.800000d+06
    MAXIMUM_FRACTURE_POROSITY 5.000000d-02
    FRACTURE_EXPONENT 1.518045d+01
    ALTER_PERM_X #hardwired
    ALTER_PERM_Y #hardwired
  /
  CHARACTERISTIC_CURVES S_MB139_KRP4
END

CHARACTERISTIC_CURVES S_MB139_KRP4
  SATURATION_FUNCTION BRAGFLO_KRP4
    KPC 2
    PCT_A 2.600000d-01
    PCT_EXP -3.480000d-01
    LAMBDA 6.879527d-01
    LIQUID_RESIDUAL_SATURATION 7.296712d-02
    GAS_RESIDUAL_SATURATION 5.495000d-02
    MAX_CAPILLARY_PRESSURE 1.000000d+08
  /
  PERMEABILITY_FUNCTION BRAGFLO_KRP4_LIQ
    LAMBDA 6.879527d-01
    LIQUID_RESIDUAL_SATURATION 7.296712d-02
    GAS_RESIDUAL_SATURATION 5.495000d-02
  /
  PERMEABILITY_FUNCTION BRAGFLO_KRP4_GAS
    LAMBDA 6.879527d-01
    LIQUID_RESIDUAL_SATURATION 7.296712d-02
    GAS_RESIDUAL_SATURATION 5.495000d-02
  /
END
#===============================flow conditions================================#
FLOW_CONDITION initial
  TYPE
    LIQUID_PRESSURE DIRICHLET
    GAS_SATURATION DIRICHLET
  /
  LIQUID_PRESSURE 1.d7
  GAS_SATURATION 0.05
END

#========================initial & boundary conditions=========================#
INITIAL_CONDITION initial
  FLOW_CONDITION initial
  REGION all
END

#====================================strata====================================#
STRATA
  REGION all
  MATERIAL S_MB139
END

END_SUBSURFACE
