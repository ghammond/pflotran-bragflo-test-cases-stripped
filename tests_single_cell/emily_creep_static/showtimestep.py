import matplotlib.pyplot as plt
import matplotlib.animation as animation
import time
import os
import numpy as np
import h5py
import sys

# Pass the hdf5 filenames on the command line
bfname_h5_input = sys.argv[1]
pfname = sys.argv[2]
bfh_h5 = h5py.File(bfname_h5_input + '.h5', mode='r')

# Read BRAGFLO step data
bftimeyr = bfh_h5['/output_tables/simulation_statistics_table']['TIMEYR']
bfdeltyr = bfh_h5['/output_tables/simulation_statistics_table']['DELTYR']

os.system("rm timestepdata.txt")

# Read PFLOTRAN step data
cmd = "cat " + str(pfname) + ".out | grep Step > timestepdata.txt"
os.system(cmd)
soltime, timestep  = np.loadtxt("timestepdata.txt", usecols=(3, 5), skiprows=1, unpack=True)
np.savetxt("timestepdata.txt", zip(soltime, timestep), delimiter=',', newline='\n', fmt='%1.4e %1.4e')
pullData = open("timestepdata.txt","r").read()
dataArray = pullData.split('\n')
xar = []
yar = []
for eachLine in dataArray:
    if len(eachLine)>1:
        x,y = eachLine.split(' ')
        xar.append(float(x))
        yar.append(float(y))
xar = np.asarray(xar)
yar = np.asarray(yar)

# Set up plots
fig = plt.figure(figsize=(9.75,6))
st = fig.suptitle('dt {}, {}'.format(bfname_h5_input,pfname), fontsize="x-large")

# Plot 1 from 0 to 5 yr
mask1bf = (bftimeyr <= 5.)
mask1pf = (xar <= 5.)
ax1 = fig.add_subplot(221)
ax1.plot(bftimeyr[mask1bf],bfdeltyr[mask1bf], 'b-', marker='o', label='BF')
ax1.plot(xar[mask1pf],yar[mask1pf], 'g-', marker='x', label='PF')
ax1.set_xlabel('Time [yr]')
ax1.set_ylabel('Time Step [yr]')
ax1.set_title('0 to 5 yr')
ax1.set_xlim(0.,5.)
ax1.legend(loc=0)
plt.setp(plt.gca().get_legend().get_texts(),fontsize='small')
plt.setp(plt.gca().get_legend().get_texts(),linespacing=0.)
plt.setp(plt.gca().get_legend().get_frame().set_fill(False))
plt.setp(plt.gca().get_legend().draw_frame(False))

# Plot 2 from 5 to 100 yr
mask2bf = (bftimeyr >= 0.) & (bftimeyr <= 100.)
mask2pf = (xar >= 0.) & (xar <= 100.)
ax2 = fig.add_subplot(222)
ax2.plot(bftimeyr[mask2bf],bfdeltyr[mask2bf], 'b-', marker='o', label='BF')
ax2.plot(xar[mask2pf],yar[mask2pf], 'g-', marker='x', label='PF')
ax2.set_xlabel('Time [yr]')
ax2.set_ylabel('Time Step [yr]')
ax2.set_title('0 to 100 yr')
ax2.set_xlim(0.,100.)
ax2.legend(loc=0)
plt.setp(plt.gca().get_legend().get_texts(),fontsize='small')
plt.setp(plt.gca().get_legend().get_texts(),linespacing=0.)
plt.setp(plt.gca().get_legend().get_frame().set_fill(False))
plt.setp(plt.gca().get_legend().draw_frame(False))

# Plot 3 from 0 to 10000 yr
mask3bf = (bftimeyr >= 0.) & (bftimeyr <= 10000.)
mask3pf = (xar >= 0.) & (xar <= 10000.)
ax3 = fig.add_subplot(212)
ax3.plot(bftimeyr[mask3bf],bfdeltyr[mask3bf], 'b-', marker='o', label='BF')
ax3.plot(xar[mask3pf],yar[mask3pf], 'g-', marker='x', label='PF')
ax3.set_xlabel('Time [yr]')
ax3.set_ylabel('Time Step [yr]')
ax3.set_title('0 to 10,000 yr')
ax3.set_xlim(0.,10000.)
ax3.legend(loc=0)
plt.setp(plt.gca().get_legend().get_texts(),fontsize='small')
plt.setp(plt.gca().get_legend().get_texts(),linespacing=0.)
plt.setp(plt.gca().get_legend().get_frame().set_fill(False))
plt.setp(plt.gca().get_legend().draw_frame(False))

# Finish Plots
plt.tight_layout()
st.set_y(0.95)
fig.subplots_adjust(top=0.85)
plt.savefig(bfname_h5_input + '-' + pfname + '_TIMESTEP', dpi=300)
plt.show()
