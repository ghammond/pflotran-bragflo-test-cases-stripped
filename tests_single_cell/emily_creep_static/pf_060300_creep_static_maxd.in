!==============================================================================
! Test Case 060300 - Creep closure in tank, partially saturated
! In this single-cell model, creep reduces porosity.
! The initial conditions are sw=0.5 and brine pressure 101325 Pa.
! The default no-flow boundary conditions apply.
! 
!
! 
! 
! 
! 
! 
!
!==============================================================================
! Emily add DT_FACTOR 10.3.17
! And MAX_PRESSURE_CHANGE, MAX_SATURATION_CHANGE
! Add hdf5 output
! Add EFFECTIVE_POROSITY (was saving POROSITY)
!=========================== flow mode ========================================
SIMULATION
  SIMULATION_TYPE SUBSURFACE
  PROCESS_MODELS
    SUBSURFACE_FLOW flow
      MODE WIPP_FLOW
      OPTIONS
        FIX_UPWIND_DIRECTION
        MAX_PRESSURE_CHANGE 5.0d5
        MAX_SATURATION_CHANGE 0.3d0
        GAS_COMPONENT_FORMULA_WEIGHT 2.01588D0 ! H2 kg/kmol
        NO_GAS_GENERATION
        
        ! if (dabs(liquid_residual(icell) / liquid_accumulation(icell)) > liquid_equation_tolerance), not converged [default = 1.d-2]
        LIQUID_EQUATION_TOLERANCE 1.d-6
        
        ! if (dabs(gas_residual(icell) / gas_accumulation(icell)) > gas_equation_tolerance), not converged [default = 1.d-2]
        GAS_EQUATION_TOLERANCE 1.d-6
        
        ! if (dabs(delta_liquid_pressure(icell) / liquid_pressure(icell)) > liquid_pressure_tolerance), not converged [default = 1.d-2]
        LIQUID_PRESSURE_TOLERANCE 1.d-5
        
        ! if (dabs(delta_gas_saturation(icell)) > gas_saturation_tolerance), not converged [default = 1.d-3]
        GAS_SATURATION_TOLERANCE 1.d-4
      /
    /
  /
END

!==============================================================================
SUBSURFACE
!=========================== TIMESTEPPER  =================================
TIMESTEPPER FLOW #Emily
  TS_ACCELERATION 10
  DT_FACTOR 1.25 1.25 1.25 1.10 1.00 1.00 0.8 0.6 0.4 0.33
END

!=========================== regression =======================================
REGRESSION
  CELLS
    1
  /
END

!=========================== discretization ===================================
GRID
  TYPE structured
  NXYZ 1 1 1
  DXYZ
    1000.d0
    1000.d0
    1.d0
  /
END

!=========================== fluid properties =================================

EOS WATER
  DENSITY EXPONENTIAL 1.2200E+03 101325.d0 3.1000E-10 ! ref_dens ref_pres compres
  VISCOSITY CONSTANT 2.10000E-03
END

EOS GAS
  ! DENSITY RKS
  !   HYDROGEN
  !   USE_EFFECTIVE_PROPERTIES
  !   TC 4.36000E+01
  !   PC 2.04700E+06
  !   AC 0.00000E+00
  !   A 4.27470E-01
  !   B 8.66400E-02
  ! /
  DENSITY IDEAL
  VISCOSITY CONSTANT 8.93389E-06
END

REFERENCE_PRESSURE 1.01325d5
REFERENCE_TEMPERATURE 27.d0 ! 300.15 K


!=========================== saturation functions =============================
CHARACTERISTIC_CURVES cc_WAS_AREA
  SATURATION_FUNCTION BRAGFLO_KRP12
    LIQUID_RESIDUAL_SATURATION 1.776051E-01
    ! GAS_RESIDUAL_SATURATION    1.654909E-02
    ! MAX_CAPILLARY_PRESSURE 1.00000d+08
    LAMBDA 2.890000E+00
    S_MIN 1.864854E-01
    S_EFFMIN 1.000000E-03
    ! IGNORE_PERMEABILITY
    ! ALPHA 1.000000E+00
    KPC 1
    PCT_A 0.d0
    PCT_EXP 0.d0
  /
  PERMEABILITY_FUNCTION BRAGFLO_KRP12_LIQ
    LIQUID_RESIDUAL_SATURATION 1.776051E-01
    GAS_RESIDUAL_SATURATION    1.654909E-02
    LAMBDA 2.890000E+00
  /
  PERMEABILITY_FUNCTION BRAGFLO_KRP12_GAS
    LIQUID_RESIDUAL_SATURATION 1.776051E-01
    GAS_RESIDUAL_SATURATION    1.654909E-02
    LAMBDA 2.890000E+00
  /
END

!=========================== material properties ==============================
MATERIAL_PROPERTY WAS_AREA
  ID 1
  CHARACTERISTIC_CURVES cc_WAS_AREA
  POROSITY 8.480000E-01
  SOIL_COMPRESSIBILITY_FUNCTION POROSITY_EXPONENTIAL
  POROSITY_COMPRESSIBILITY 0.0d0
  SOIL_REFERENCE_PRESSURE INITIAL_PRESSURE
  PERMEABILITY
    PERM_ISO 2.399938E-13
  /
  CREEP_CLOSURE_TABLE  creep001
END

! Creep Closure
CREEP_CLOSURE_TABLE  creep001
  FILENAME pflotran_closure.dat
  SHUTDOWN_PRESSURE  5.0d7 ! [Pa] stop creep if pressure exceeds this value
  TIME_CLOSEOFF  3.1557d12 ! [sec] stop creep if time exceeds this value
END

!=========================== regions ==========================================
REGION all
  COORDINATES
    0.d0 0.d0 0.d0
    1000.d0 1000.d0 1.d0
  /
END

REGION region1
  BLOCK 1 1 1 1 1 1
END

REGION observation_point
  BLOCK 1 1 1 1 1 1
END

REGION injection_point
  BLOCK 1 1 1 1 1 1
END

!=========================== stratigraphy couplers ============================
STRATA
  MATERIAL WAS_AREA
  REGION all
END

!=========================== flow conditions ==================================
! initial condition
FLOW_CONDITION 1
  TYPE
    LIQUID_PRESSURE DIRICHLET
    LIQUID_SATURATION DIRICHLET
  /
  LIQUID_PRESSURE 101325.d0
  LIQUID_SATURATION 0.5d0 ! 0.98345091E+00
END

! FLOW_CONDITION well  ! INJQ in BF
!   TYPE
!     RATE mass_rate
!   /
!   RATE 0.d0 2.302670E-06 kg/s kg/s ! water(kg/s) gas(kg/s) 
! END

!=========================== condition couplers ===============================
INITIAL_CONDITION
  FLOW_CONDITION 1
  REGION region1
END

! SOURCE_SINK well
!   FLOW_CONDITION well
!   REGION injection_point
! END

!=========================== solver options ===================================
NEWTON_SOLVER FLOW
!  RTOL 1.d-08
!  ATOL 1.d-08
!  STOL 1.d-30       ! Turn off STOL criteria
  MAXIT 20          ! Maximum newton iteration number
END

LINEAR_SOLVER FLOW
  SOLVER DIRECT
END

!=========================== times ============================================
TIME
  FINAL_TIME 1.d4 y #3.153600E+11 s ! 10k y
  INITIAL_TIMESTEP_SIZE 1.d0 s
  MAXIMUM_TIMESTEP_SIZE 1.576800E+07 s
END

!=========================== output options ===================================

OBSERVATION
  REGION observation_point
END

OUTPUT
  OBSERVATION_FILE
    #NO_PRINT_INITIAL
    #NO_PRINT_FINAL
    PERIODIC TIMESTEP 1
    VARIABLES
      LIQUID_PRESSURE
      GAS_PRESSURE
      EFFECTIVE_POROSITY
      LIQUID_MOBILITY
      GAS_MOBILITY
      LIQUID_DENSITY
      GAS_DENSITY
      PERMEABILITY_X
      GAS_PERMEABILITY_X
      LIQUID_SATURATION
      GAS_SATURATION
      CAPILLARY_PRESSURE
    /
  /
  SNAPSHOT_FILE
    FORMAT HDF5
    PERIODIC TIME 1 y between 0 y and 10 y
    PERIODIC TIME 10 y between 10 y and 100 y
    TIMES y 350. 1000. 3000. 5000. 7000. 9000. 10000.
    VARIABLES
      LIQUID_PRESSURE
      GAS_PRESSURE
      EFFECTIVE_POROSITY
      LIQUID_MOBILITY
      GAS_MOBILITY
      LIQUID_DENSITY
      GAS_DENSITY
      PERMEABILITY_X
      GAS_PERMEABILITY_X
      LIQUID_SATURATION
      GAS_SATURATION
      CAPILLARY_PRESSURE
    /
  /
    
END


!==============================================================================
END_SUBSURFACE
!==============================================================================


!=========================== WIPP Source Sink =================================
skip
WIPP_SOURCE_SINK
  BRUCITEC  5.40958610500549d-08  ![mol-MgOH2/kg-MgO/s] MgO inundated hydration rate in Salado brine
  BRUCITEH  1.90935050526199d-08  ![mol-MgOH2/kg-MgO/s] MgO humid hydration rate
  HYMAGCON  6.47595498826265d-10  ![mol-hydromag/kg-hydromag/s] hydromagnesite to magnesite conversion rate
  SAT_WICK  0.322252637147903d0   ![-] wicking saturation parameter
  SALT_PERCENT  3.2400d1   ![100*kg salt/kg water] weight percent salt in brine (rxns produce brine, not just water)
  GRATMICI  2.38570594086619d-10  ![mol-cell/kg-cell/s] inundated biodegradation rate for cellulose
  GRATMICH  3.38837738770187d-11  ![mol-cell/kg-cell/s] humid biodegradation rate for cellulose
  CORRMCO2  6.67748215472072d-15  ![m/s] inundated steel corrosion rate without microbial gas generation
  HUMCORR   0.d0    ![m/s] humid steel corrosion rate
  ASDRUM    6.d0    ![m2] surface area of corrodable metal per drum
  ALPHARXN -1.d3    ![-]
  SOCMIN    1.5d-2  ![-]
  BIOGENFC  0.725563609600067   ![-]
  PROBDEG   1       ![-]

  STOICHIOMETRIC_MATRIX
  # hydro  H2     H2O       Fe      Cell   FeOH2  FeS    MgO    MgOH2  MgCO3 
    0.0d0  1.0d0 -2.0d0     -1.0d0  0.0d0  1.0d0  0.0d0  0.0d0  0.0d0  0.0d0 # anoxic iron corrosion reaction
    0.0d0  0.0d0  0.91293d0  0.0d0 -1.0d0  0.0d0  0.0d0  0.0d0  0.0d0  0.0d0 # microbial gas generation reaction
    0.0d0 -1.0d0  2.0d0      0.0d0  0.0d0 -1.0d0  1.0d0  0.0d0  0.0d0  0.0d0 # iron hydroxide sulfidation
    0.0d0  0.0d0  0.0d0     -1.0d0  0.0d0  0.0d0  1.0d0  0.0d0  0.0d0  0.0d0 # metallic iron sulfidation
    0.0d0  0.0d0 -1.0d0      0.0d0  0.0d0  0.0d0  0.0d0 -1.0d0  1.0d0  0.0d0 # MgO hydration
    0.25d0 0.0d0  0.0d0      0.0d0  0.0d0  0.0d0  0.0d0  0.0d0 -1.25d0 0.0d0 # Mg(OH)2 (brucite) carbonation
    0.0d0  0.0d0  0.0d0      0.0d0  0.0d0  0.0d0  0.0d0 -1.0d0  0.0d0  1.0d0 # MgO carbonation
   -1.0d0  0.0d0  4.0d0      0.0d0  0.0d0  0.0d0  0.0d0  0.0d0  1.0d0  4.0d0 # hydromagnesite conversion
  END 
  # note: multiple inventories may be included, but here there is only one
  INVENTORY INV1 #each inventory is specific to region it is going in, so SCALE_BY_VOLUME when using whole repo inventory
    VREPOS     438406.08 m^3 ! optional - only needed if a WASTE_PANEL including this inventory needs to SCALE_BY_VOLUME
    SOLIDS #total kg in repository
      IRONCHW  1.09d7 kg   ! mass of Fe-based material in CH waste
      IRONRHW  1.35d6 kg   ! mass of Fe-based material in RH waste
      IRNCCHW  3.00d7 kg   ! mass of Fe containers for CH waste
      IRNCRHW  6.86d6 kg   ! mass of Fe containers for RH waste
      CELLCHW  3.55d6 kg   ! mass of cellulosics in CH waste
      CELLRHW  1.18d5 kg   ! mass of cellulosics in RH waste
      CELCCHW  7.23d5 kg   ! mass of cellulosics in container materials for CH waste
      CELCRHW  0.d0   kg   ! mass of cellulosics in container materials for RH waste
      CELECHW  2.60d5 kg   ! mass of cellulosics in emplacement materials for CH waste
      CELERHW  0.d0   kg   ! mass of cellulosics in emplacement materials for RH waste
      RUBBCHW  1.09d6 kg   ! mass of rubber in CH waste
      RUBBRHW  8.80d4 kg   ! mass of rubber in RH waste
      RUBCCHW  6.91d4 kg   ! mass of rubber in container materials for CH waste
      RUBCRHW  4.18d3 kg   ! mass of rubber in container materials for RH waste
      RUBECHW  0.d0   kg   ! mass of rubber in emplacement materials for CH waste
      RUBERHW  0.d0   kg   ! mass of rubber in emplacement materials for RH waste
      PLASCHW  5.20d6 kg   ! mass of plastics in CH waste
      PLASRHW  2.93d5 kg   ! mass of plastics in RH waste
      PLSCCHW  2.47d6 kg   ! mass of plastics in container materials for CH waste
      PLSCRHW  3.01d5 kg   ! mass of plastics in container materials for RH waste
      PLSECHW  1.25d6 kg   ! mass of plastics in emplacement materials for CH waste
      PLSERHW  0.d0   kg   ! mass of plastics in emplacement materials for RH waste
      PLASFAC  1.7d0       ! mass ratio of plastics to equivalent carbon
      MGO_EF   1.2d0       ! MgO excess factor: ratio mol-MgO/mol-Organic-C
      DRMCONC  1.8669852   ! [-/m3] number of metal drums per m3 in a panel in ideal packing (DRROOM/VROOM = 6804/3644.378))
    END
    AQUEOUS
      NITRATE 2.74d7   ! moles in panel  QINIT[B:32]
      SULFATE 4.91d6   ! moles in panel  QINIT[B:31]
    END
  END
  
  WASTE_PANEL WP1
    REGION injection_point
    INVENTORY INV1
    SCALE_BY_VOLUME YES
  END
  
  ! WASTE_PANEL WP2
  !   REGION WP2
  !   INVENTORY INV1
  !   SCALE_BY_VOLUME YES
  ! END
  
!==============================================================================
END_WIPP_SOURCE_SINK
!==============================================================================
noskip
