!==============================================================================
! Test Case 060210 - Gas generation in tank, saturated
! In this single-cell model, gas generation occurs and pressure builds.
! The initial conditions are sw=0.25 and brine pressure 101325 pa.
! The default no-flow boundary conditions apply.
! 
!
! 
! 
! 
! 
! 
!
!==============================================================================

!=========================== SIMULATION MOD ===================================
SIMULATION
  SIMULATION_TYPE SUBSURFACE
  PROCESS_MODELS
    SUBSURFACE_FLOW FLOW
      MODE WIPP_FLOW
      OPTIONS
        EXTERNAL_FILE ../../block_options.txt
      END
    END
  END
END

!==============================================================================
SUBSURFACE
!==============================================================================

!=========================== TIMESTEPPER ======================================
EXTERNAL_FILE ../../block_timestepper.txt

!=========================== SOLVER OPTIONS ===================================
EXTERNAL_FILE ../../block_solver_options.txt

!=========================== regression =======================================
REGRESSION
  CELLS
    1
  /
END

!=========================== discretization ===================================
GRID
  TYPE structured
  NXYZ 1 1 1
  DXYZ
    1000.d0
    1000.d0
    1.d0
  /
END

!=========================== fluid properties =================================

EOS WATER
  DENSITY EXPONENTIAL 1.2200E+03 101325.d0 3.1000E-10 ! ref_dens ref_pres compres
  VISCOSITY CONSTANT 2.10000E-03
END

EOS GAS
  ! DENSITY RKS
  !   HYDROGEN
  !   USE_EFFECTIVE_PROPERTIES
  !   USE_CUBIC_ROOT_SOLUTION
  !   TC 4.36000E+01
  !   PC 2.04700E+06
  !   AC 0.00000E+00
  !   A 4.27470E-01
  !   B 8.66400E-02
  ! /
  DENSITY IDEAL
  VISCOSITY CONSTANT 8.93389E-06
END

REFERENCE_PRESSURE 101325.d0
REFERENCE_TEMPERATURE 27.d0 ! 300.15 K


!=========================== saturation functions =============================
CHARACTERISTIC_CURVES cc_WAS_AREA
  SATURATION_FUNCTION BRAGFLO_KRP12
    LIQUID_RESIDUAL_SATURATION 1.776051E-01
    ! GAS_RESIDUAL_SATURATION    1.654909E-02
    ! MAX_CAPILLARY_PRESSURE 1.00000d+08
    LAMBDA 2.890000E+00
    S_MIN 1.864854E-01
    S_EFFMIN 1.000000E-03
    ! IGNORE_PERMEABILITY
    ! ALPHA 1.000000E+00
    KPC 1
    PCT_A 0.d0
    PCT_EXP 0.d0
  /
  PERMEABILITY_FUNCTION BRAGFLO_KRP12_LIQ
    LIQUID_RESIDUAL_SATURATION 1.776051E-01
    GAS_RESIDUAL_SATURATION    1.654909E-02
    LAMBDA 2.890000E+00
  /
  PERMEABILITY_FUNCTION BRAGFLO_KRP12_GAS
    LIQUID_RESIDUAL_SATURATION 1.776051E-01
    GAS_RESIDUAL_SATURATION    1.654909E-02
    LAMBDA 2.890000E+00
  /
END

!=========================== material properties ==============================
MATERIAL_PROPERTY WAS_AREA
  ID 1
  CHARACTERISTIC_CURVES cc_WAS_AREA
  POROSITY 8.480000E-01
  SOIL_COMPRESSIBILITY_FUNCTION POROSITY_EXPONENTIAL
  POROSITY_COMPRESSIBILITY 0.0d0
  SOIL_REFERENCE_PRESSURE INITIAL_PRESSURE
  PERMEABILITY
    PERM_ISO 2.399938E-13
  /
END

!=========================== regions ==========================================
REGION all
  COORDINATES
    0.d0 0.d0 0.d0
    1000.d0 1000.d0 1.d0
  /
END

REGION region1
  BLOCK 1 1 1 1 1 1
END

REGION observation_point
  BLOCK 1 1 1 1 1 1
END

REGION injection_point
  BLOCK 1 1 1 1 1 1
END

!=========================== stratigraphy couplers ============================
STRATA
  MATERIAL WAS_AREA
  REGION all
END

!=========================== flow conditions ==================================
! initial condition
FLOW_CONDITION 1
  TYPE
    LIQUID_PRESSURE DIRICHLET
    LIQUID_SATURATION DIRICHLET
  /
  LIQUID_PRESSURE 1.013250E+05
  LIQUID_SATURATION 0.200000E+00
END

! FLOW_CONDITION well  ! INJQ in BF
!   TYPE
!     RATE mass_rate
!   /
!   RATE 0.d0 2.302670E-06 kg/s kg/s ! water(kg/s) gas(kg/s) 
! END

!=========================== condition couplers ===============================
INITIAL_CONDITION
  FLOW_CONDITION 1
  REGION region1
END

! SOURCE_SINK well
!   FLOW_CONDITION well
!   REGION injection_point
! END

!=========================== times ============================================
EXTERNAL_FILE ../../block_time.txt

!=========================== output options ===================================

OBSERVATION
  REGION observation_point
END

OUTPUT
  OBSERVATION_FILE
      NO_PRINT_INITIAL
      NO_PRINT_FINAL
      PERIODIC TIMESTEP 1
      VARIABLES
        LIQUID_PRESSURE
        GAS_PRESSURE
        POROSITY
        LIQUID_MOBILITY
        GAS_MOBILITY
        LIQUID_DENSITY
        GAS_DENSITY
        PERMEABILITY_X
        GAS_PERMEABILITY_X
        LIQUID_SATURATION
        GAS_SATURATION
        ! CAPILLARY_PRESSURE
      /
    /
    
END


!==============================================================================
END_SUBSURFACE
!==============================================================================


!=========================== WIPP Source Sink =================================

WIPP_SOURCE_SINK

  EXTERNAL_FILE ../../block_rates.txt

  EXTERNAL_FILE ../../block_stoichiometry.txt

  EXTERNAL_FILE ../../block_inventory.txt
  
  WASTE_PANEL WP1
    REGION injection_point
    INVENTORY INV1
    SCALE_BY_VOLUME YES
  END
  
!==============================================================================
END_WIPP_SOURCE_SINK
!==============================================================================
