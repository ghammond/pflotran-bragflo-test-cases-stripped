#!/bin/python
import sys
import os
import argparse
import numpy as np
import h5py
import matplotlib.pyplot as plt
import matplotlib.ticker as tck
from matplotlib.ticker import ScalarFormatter

from matplotlib.backends.backend_pdf import PdfPages

""" Usage: python plot_pf_bf.py """
#""" Usage:  python plot_pf_bf_obs2.py  single_cell_figures.pdf  ./comparison_figures """

# Overload some stuff
# https://stackoverflow.com/questions/42142144/displaying-first-decimal-digit-in-scientific-notation-in-matplotlib
from matplotlib.ticker import ScalarFormatter
class ScalarFormatterForceFormat(ScalarFormatter):
    def _set_format(self,vmin,vmax):  # Override function that finds format to use.
        self.format = "%1.2f"  # Give format here

# Also
# https://stackoverflow.com/questions/6682784/how-to-reduce-number-of-ticks-with-matplotlib

# Get the output file names from the command line arguments
fname_fig_pdf = 'single_cell_figures.pdf';
fig_output_dir = './comparison_figures';

if len(sys.argv)>2:
  fname_fig_pdf = sys.argv[1]; # name of pdf file to create
  fig_output_dir = sys.argv[2]; # subdirectory in which to place the .png output figures
elif len(sys.argv)>1:
  fname_fig_pdf = sys.argv[1]; # name of pdf file to create



tpl_pf_tecplot_header = (
'TIME', 
'PRESBRIN', 
'PRESGAS', 
'POROS', 
'RELPERMB',
'RELPERMG',
'DENBRINE', 
'DENGAS', 
'PERMBRX',
'PERMGASX',
'SATBRINE', 
'SATGAS'
);

tpl_pf_pnl_header = (
'TIME', 
'PNL', 
'CORRATI', 
'CORRATH', 
'BIORATI', 
'BIORATH', 
'FEOH2_SR', 
'FE_SR', 
'MGO_HR', 
'MGOH2_CR', 
'MGO_CR', 
'HYMAG_CR', 
'H2RATE', 
'BRINRATE', 
'FERATE', 
'CELLRATE', 
'FEOH2R', 
'FESR', 
'MGOR', 
'MGOH2R', 
'HYMAGR', 
'MGCO3R', 
'FECONC', 
'CELLCONC', 
'FEOH2C', 
'FESC', 
'MGOC', 
'MGOH2C', 
'HYMAGC', 
'MGCO3C', 
'PORSOLID'
);

tpl_bf_hist_header = (
'PRESBRIN', 
'PRESGAS', 
'POROS', 
'RELPERMB',
'RELPERMG',
'DENBRINE', 
'DENGAS', 
'SATBRINE', 
'SATGAS', 
'CORRATI', 
'CORRATH', 
'BIORATI', 
'BIORATH', 
'FEOH2_SR', 
'FE_SR', 
'MGO_HR', 
'MGOH2_CR', 
'MGO_CR', 
'HYMAG_CR', 
'H2RATE', 
'BRINRATE', 
'FERATE', 
'CELLRATE', 
'FEOH2R', 
'FESR', 
'MGOR', 
'MGOH2R', 
'HYMAGR', 
'MGCO3R', 
'FECONC', 
'CELLCONC', 
'FEOH2C', 
'FESC', 
'MGOC', 
'MGOH2C', 
'HYMAGC', 
'MGCO3C', 
'PORSOLID'
);

dic_bf_hist_header = {
'PRESBRIN' : 'H0010001', 
'PRESGAS' : 'H0020001', 
'POROS' : 'H0060001', 
'RELPERMB' : 'H0070001', 
'RELPERMG' : 'H0080001', 
'DENBRINE' : 'H0090001', 
'DENGAS' : 'H0100001', 
'PERMBRX' : 'H0110001', 
'PERMGASX' : 'H0140001', 
'SATBRINE' : 'H0170001', 
'SATGAS' : 'H0180001', 
'CORRATI' : 'H0390001', 
'CORRATH' : 'H0400001', 
'BIORATI' : 'H0410001', 
'BIORATH' : 'H0420001', 
'FEOH2_SR' : 'H0430001', 
'FE_SR' : 'H0440001', 
'MGO_HR' : 'H0450001', 
'MGOH2_CR' : 'H0460001', 
'MGO_CR' : 'H0470001', 
'HYMAG_CR' : 'H0480001', 
'H2RATE' : 'H0490001', 
'BRINRATE' : 'H0500001', 
'FERATE' : 'H0510001', 
'CELLRATE' : 'H0520001', 
'FEOH2R' : 'H0530001', 
'FESR' : 'H0540001', 
'MGOR' : 'H0550001', 
'MGOH2R' : 'H0560001', 
'HYMAGR' : 'H0570001', 
'MGCO3R' : 'H0580001', 
'FECONC' : 'H0590001', 
'CELLCONC' : 'H0600001', 
'FEOH2C' : 'H0610001', 
'FESC' : 'H0620001', 
'MGOC' : 'H0630001', 
'MGOH2C' : 'H0640001', 
'HYMAGC' : 'H0650001', 
'MGCO3C' : 'H0660001', 
'PORSOLID' : 'H0680001'
};

# Convert tuple of column headings to an indexed dict
# dic_bf_hist_header = { tpl_bf_hist_header[i]: i for i in range(0, len(tpl_bf_hist_header) };
dic_pf_tecplot_header = { k: i for i, k in enumerate(tpl_pf_tecplot_header) };
dic_pf_pnl_header = { k: i for i, k in enumerate(tpl_pf_pnl_header) };

# dic_bf_hist_header = { k: i for i, k in enumerate(tpl_bf_hist_header) };


# List of deck files to run
DECK_FILES=[
  'case060100_0d_gas_injection', 
  'case060200_0d_gas_generation_midsat', 
  'case060210_0d_gas_generation_hisat', 
  'case060220_0d_gas_generation_lowsat', 
  'case060230_0d_gas_generation_corbio_midsat', 
  'case060240_0d_gas_generation_corbio_hisat', 
  'case060250_0d_gas_generation_corbio_lowsat', 
  'case060300_0d_creep_static', 
  'case060310_0d_creep_gas_injection', 
  'case060320_0d_creep_gas_generation', 
  'case060400_0d_porecomp_gas_injection', 
  'case060500_0d_fracture_gas_injection', 
  'case060600_0d_klinkenberg_gas_injection', 
  'case060700_0d_rks_calc_gas_injection'
];

DECK_DIR=[
  'case060100',
  'case060200','case060200','case060200',
  'case060200','case060200','case060200',
  'case060300','case060300','case060300',
  'case060400',
  'case060500',
  'case060600',
  'case060700'
  ];

DECK_TITLES=[
  'Gas Injection', 
  'Gas Generation, Swi=0.50', 
  'Gas Generation, Swi=0.98', 
  'Gas Generation, Swi=0.20', 
  'Gas Generation, No MgO Hydration, Swi=0.50', 
  'Gas Generation, No MgO Hydration, Swi=0.98', 
  'Gas Generation, No MgO Hydration, Swi=0.20', 
  'Creep Closure, Static Conditions', 
  'Creep Closure, Gas Injection', 
  'Creep Closure, Gas Generation', 
  'Pore Compressibility with Gas Injection', 
  'MB Fracturing with Gas Injection', 
  'Klinkenberg Gas Permeability with Gas Injection', 
  'RKS Gas EOS with Gas Injection'
];

# Fixed parameters
VISGAS=8.93389E-06;
VISBRINE=2.10000E-03;
PERM1=2.399938E-13;
PERM2=1.047981E-19;
SECYR = 3.17097919837646E-08;
YRSEC = 3.153600E+07;

# loop over files
for jj in range(len(DECK_FILES)):
#for deckname in DECK_FILES:
  deckname = DECK_FILES[jj];
  decktitle = DECK_TITLES[jj];
  deck_dir = DECK_DIR[jj];
  
  print deck_dir
  os.chdir(deck_dir)
  
  #############################################################################
  # Read in the data
  #############################################################################
  tec1_filename =  'pf_' + deckname + '-obs-0.tec';
  pnl1_filename =  'pf_' + deckname + '-0.pnl';
  bf_h5_filename = 'bf_' + deckname + '.h5';
  
  # Read pflotran tecplot file
  print tec1_filename
  pf_tecplot_data = np.loadtxt(tec1_filename, dtype='float64', skiprows=1);
  
  # Read bragflo h5 file
  print bf_h5_filename
  fh_bf_h5 = h5py.File(bf_h5_filename, mode='r');
  
  #############################################################################
  ## Plot the data
  #############################################################################
  
  print "PLOTTING " + deckname; 
  

  
  
  
  #############################################################################
  ## 2x2 Figures --> From *.tec 
  #############################################################################
  
  
  SECYR = 3.17097919837646E-08;
  YRSEC = 3.153600E+07;
  
  PRESBRIN_0 = 1.0E+5 * 1.0E-2;
  SATBRINE_0 = 1.0E-2 * 1.0E-2;
  POROS_0 = 1.0E-2 * 1.0E-2;
  LNPERM_0 = -1.0E+1 * 1.0E-2;
  PERM_0 = 1.0E-19 * 1.0E-3;
  DENBRINE_0 = 1.0E+3 * 1.0E-2;
  DENGAS_0 = 1.0E-1 * 1.0E-2;
  RXN_0 = 1.0E-12 * 1.0E-2;
  CONC_0 = 1.0E-12 * 1.0E-2;
  
  # Get Time arrays
  bf_time_yr = fh_bf_h5['/output_tables/simulation_statistics_table']['TIMESEC'] / YRSEC;
  pf_time_yr = pf_tecplot_data[:, dic_pf_tecplot_header['TIME']] / YRSEC;
  
  # Get Pressure arrays
  bf_presbrin = fh_bf_h5['/output_tables/history_variables_table'][dic_bf_hist_header['PRESBRIN']];
  print bf_presbrin
  pf_presbrin = pf_tecplot_data[:, dic_pf_tecplot_header['PRESBRIN']];
  # Sample pf values at bf time points
  pf_presbrin_at_bf_times = np.interp(bf_time_yr,  pf_time_yr, pf_presbrin);
  err_pressbrin = 100.0*(bf_presbrin - pf_presbrin_at_bf_times) / np.maximum(bf_presbrin, PRESBRIN_0);
  
  # Get Saturation arrays
  bf_satbrine = fh_bf_h5['/output_tables/history_variables_table'][dic_bf_hist_header['SATBRINE']];
  pf_satbrine = pf_tecplot_data[:, dic_pf_tecplot_header['SATBRINE']];
  # Sample pf values at bf time points
  pf_satbrine_at_bf_times = np.interp(bf_time_yr,  pf_time_yr, pf_satbrine);
  err_satbrine = 100.0*(bf_satbrine - pf_satbrine_at_bf_times) / np.maximum(bf_satbrine, SATBRINE_0);
  
  # Get porosity arrays
  bf_poros = fh_bf_h5['/output_tables/history_variables_table'][dic_bf_hist_header['POROS']];
  pf_poros = pf_tecplot_data[:, dic_pf_tecplot_header['POROS']];
  # Sample pf values at bf time points
  pf_poros_at_bf_times = np.interp(bf_time_yr,  pf_time_yr, pf_poros);
  err_poros = 100.0*(bf_poros - pf_poros_at_bf_times) / np.maximum(bf_poros, POROS_0);
  
  # Get brine density arrays
  bf_denbrine = fh_bf_h5['/output_tables/history_variables_table'][dic_bf_hist_header['DENBRINE']];
  pf_denbrine = pf_tecplot_data[:, dic_pf_tecplot_header['DENBRINE']];
  # Sample pf values at bf time points
  pf_denbrine_at_bf_times = np.interp(bf_time_yr,  pf_time_yr, pf_denbrine);
  err_denbrine = 100.0*(bf_denbrine - pf_denbrine_at_bf_times) / np.maximum(bf_denbrine, DENBRINE_0);
  
  # Get gas density arrays
  bf_dengas = fh_bf_h5['/output_tables/history_variables_table'][dic_bf_hist_header['DENGAS']];
  pf_dengas = pf_tecplot_data[:, dic_pf_tecplot_header['DENGAS']];
  # Sample pf values at bf time points
  pf_dengas_at_bf_times = np.interp(bf_time_yr,  pf_time_yr, pf_dengas);
  err_dengas = 100.0*(bf_dengas - pf_dengas_at_bf_times) / np.maximum(bf_dengas, SATBRINE_0);
  
  # Get permeability to brine arrays
  bf_permbrx = fh_bf_h5['/output_tables/history_variables_table'][dic_bf_hist_header['PERMBRX']];
  pf_permbrx = pf_tecplot_data[:, dic_pf_tecplot_header['PERMBRX']];
  # Sample pf values at bf time points
  pf_permbrx_at_bf_times = np.interp(bf_time_yr,  pf_time_yr, pf_permbrx);
  err_permbrx = 100.0*(bf_permbrx - pf_permbrx_at_bf_times) / np.maximum(bf_permbrx, PERM_0);
  
  # Get permeability to gas arrays
  bf_permgasx = fh_bf_h5['/output_tables/history_variables_table'][dic_bf_hist_header['PERMGASX']];
  pf_permgasx = pf_tecplot_data[:, dic_pf_tecplot_header['PERMGASX']];
  # Sample pf values at bf time points
  pf_permgasx_at_bf_times = np.interp(bf_time_yr,  pf_time_yr, pf_permgasx);
  err_permgasx = 100.0*(bf_permgasx - pf_permgasx_at_bf_times) / np.maximum(bf_permgasx, PERM_0);
  
  
  # Set some defaults plot values
  plt.rcParams['figure.dpi'] = 300;
  plt.rcParams['xtick.labelsize'] = 9;
  plt.rcParams['ytick.labelsize'] = 9;
  plt.rcParams['axes.labelsize'] = 10;
  plt.rcParams['axes.titlesize'] = 10;
  plt.rcParams['legend.fontsize'] = 10;
  
  #######################################
  # Pressure and Saturation
  #######################################
  fig101, axes101 = plt.subplots(nrows=2, ncols=2, sharex=False, sharey=False, squeeze=True, figsize=(6.5, 4.0));
  fig101.suptitle(decktitle, fontsize=11);
  
  fig_subname = '01_pressure_saturation';
  
  axes101[0,0].plot(bf_time_yr, bf_presbrin, 'b-', alpha=0.7, label='BRAGFLO');
  axes101[0,0].plot(pf_time_yr, pf_presbrin, 'r--', alpha=0.7, label='PFLOTRAN');
  axes101[0,0].xaxis.set_major_formatter(tck.FormatStrFormatter('%i'));
  #axes101[0,0].yaxis.set_major_formatter(tck.FormatStrFormatter('%.2f'));
  yfrmtr=ScalarFormatterForceFormat(); yfrmtr.set_powerlimits((-1,1));  yfrmtr.set_scientific(True);
  axes101[0,0].yaxis.set_major_formatter(yfrmtr);
  axes101[0,0].set_xlabel('Time [yr]');
  axes101[0,0].set_ylabel('Brine Pressure [Pa]', color='k');
  axes101[0,0].legend(loc='best');
  axes101[0,0].set_title('Brine Pressure');
  if('creep' in deckname): axes101[0,0].set_xlim(0, 100);
  
  axes101[0,1].plot(bf_time_yr, bf_satbrine, 'b-', alpha=0.7, label='BRAGFLO');
  axes101[0,1].plot(pf_time_yr, pf_satbrine, 'r--', alpha=0.7, label='PFLOTRAN');
  axes101[0,1].xaxis.set_major_formatter(tck.FormatStrFormatter('%i'));
  axes101[0,1].yaxis.set_major_formatter(tck.FormatStrFormatter('%.2f'));
  axes101[0,1].set_xlabel('Time [yr]');
  axes101[0,1].set_ylabel('Brine Saturation [-]', color='k');
  axes101[0,1].legend(loc='best');
  axes101[0,1].set_title('Brine Saturation');
  if('creep' in deckname): axes101[0,1].set_xlim(0, 100);
  
  axes101[1,0].plot(bf_time_yr, err_pressbrin, 'r--', alpha=0.7, label='PFLOTRAN');
  axes101[1,0].xaxis.set_major_formatter(tck.FormatStrFormatter('%i'));
  axes101[1,0].yaxis.set_major_formatter(tck.FormatStrFormatter('%.2f'));
  ymin, ymax = axes101[1,0].get_ylim();
  axes101[1,0].set_ylim( np.sign(ymin)*np.maximum(np.abs(ymin),0.01), np.sign(ymax)*np.maximum(np.abs(ymax),0.01));
  ymin, ymax = axes101[1,0].get_ylim();
  if(ymin==-0.01 and ymax==0.01): axes101[1,0].locator_params(axis='y', nbins=3);
  axes101[1,0].set_xlabel('Time [yr]');
  axes101[1,0].set_ylabel('Percent Error [%]', color='k');
  axes101[1,0].legend(loc='best');
  axes101[1,0].set_title('Error in Brine Pressure');
  if('creep' in deckname): axes101[1,0].set_xlim(0, 100);
  
  axes101[1,1].plot(bf_time_yr, err_satbrine, 'r--', alpha=0.7, label='PFLOTRAN');
  axes101[1,1].xaxis.set_major_formatter(tck.FormatStrFormatter('%i'));
  axes101[1,1].yaxis.set_major_formatter(tck.FormatStrFormatter('%.2f'));
  ymin, ymax = axes101[1,1].get_ylim();
  axes101[1,1].set_ylim( np.sign(ymin)*np.maximum(np.abs(ymin),0.01), np.sign(ymax)*np.maximum(np.abs(ymax),0.01));
  ymin, ymax = axes101[1,1].get_ylim();
  if(ymin==-0.01 and ymax==0.01): axes101[1,1].locator_params(axis='y', nbins=3);
  axes101[1,1].set_xlabel('Time [yr]');
  axes101[1,1].set_ylabel('Percent Error [%]', color='k');
  axes101[1,1].legend(loc='best');
  axes101[1,1].set_title('Error in Brine Saturation');
  if('creep' in deckname): axes101[1,1].set_xlim(0, 100);
  
  # adjust layout
  fig101.tight_layout();
  fig101.subplots_adjust(top=0.90);
  
  # save the figure
  fig101.savefig(deckname+'_'+fig_subname+'.png', dpi=300);
  
  # toss stuff
  plt.close(fig101);
  del(fig101, axes101);
  
  #######################################
  # Pressure and Porosity
  #######################################
  if('creep' in deckname or 'por' in deckname or 'fracture' in deckname):
    
    fig101, axes101 = plt.subplots(nrows=2, ncols=2, sharex=False, sharey=False, squeeze=True, figsize=(6.5, 4.0));
    fig101.suptitle(decktitle, fontsize=11);
    
    fig_subname = '02_pressure_porosity';
    
    axes101[0,0].plot(bf_time_yr, bf_presbrin, 'b-', alpha=0.7, label='BRAGFLO');
    axes101[0,0].plot(pf_time_yr, pf_presbrin, 'r--', alpha=0.7, label='PFLOTRAN');
    axes101[0,0].xaxis.set_major_formatter(tck.FormatStrFormatter('%i'));
    #axes101[0,0].yaxis.set_major_formatter(tck.FormatStrFormatter('%.2f'));
    yfrmtr=ScalarFormatterForceFormat(); yfrmtr.set_powerlimits((-1,1));  yfrmtr.set_scientific(True);
    axes101[0,0].yaxis.set_major_formatter(yfrmtr);
    axes101[0,0].set_xlabel('Time [yr]');
    axes101[0,0].set_ylabel('Brine Pressure [Pa]', color='k');
    axes101[0,0].legend(loc='best');
    axes101[0,0].set_title('Brine Pressure');
    if('creep' in deckname): axes101[0,0].set_xlim(0, 100);
    
    axes101[0,1].plot(bf_time_yr, bf_poros, 'b-', alpha=0.7, label='BRAGFLO');
    axes101[0,1].plot(pf_time_yr, pf_poros, 'r--', alpha=0.7, label='PFLOTRAN');
    axes101[0,1].xaxis.set_major_formatter(tck.FormatStrFormatter('%i'));
    axes101[0,1].yaxis.set_major_formatter(tck.FormatStrFormatter('%.2f'));
    axes101[0,1].set_xlabel('Time [yr]');
    axes101[0,1].set_ylabel('Porosity [-]', color='k');
    axes101[0,1].legend(loc='best');
    axes101[0,1].set_title('Porosity');
    if('creep' in deckname): axes101[0,1].set_xlim(0, 100);
    
    axes101[1,0].plot(bf_time_yr, err_pressbrin, 'r--', alpha=0.7, label='PFLOTRAN');
    axes101[1,0].xaxis.set_major_formatter(tck.FormatStrFormatter('%i'));
    axes101[1,0].yaxis.set_major_formatter(tck.FormatStrFormatter('%.2f'));
    ymin, ymax = axes101[1,0].get_ylim();
    axes101[1,0].set_ylim( np.sign(ymin)*np.maximum(np.abs(ymin),0.01), np.sign(ymax)*np.maximum(np.abs(ymax),0.01));
    ymin, ymax = axes101[1,0].get_ylim();
    if(ymin==-0.01 and ymax==0.01): axes101[1,0].locator_params(axis='y', nbins=3);
    axes101[1,0].set_xlabel('Time [yr]');
    axes101[1,0].set_ylabel('Percent Error [%]', color='k');
    axes101[1,0].legend(loc='best');
    axes101[1,0].set_title('Error in Brine Pressure');
    if('creep' in deckname): axes101[1,0].set_xlim(0, 100);
    
    axes101[1,1].plot(bf_time_yr, err_poros, 'r--', alpha=0.7, label='PFLOTRAN');
    axes101[1,1].xaxis.set_major_formatter(tck.FormatStrFormatter('%i'));
    axes101[1,1].yaxis.set_major_formatter(tck.FormatStrFormatter('%.2f'));
    ymin, ymax = axes101[1,1].get_ylim();
    axes101[1,1].set_ylim( np.sign(ymin)*np.maximum(np.abs(ymin),0.01), np.sign(ymax)*np.maximum(np.abs(ymax),0.01));
    ymin, ymax = axes101[1,1].get_ylim();
    if(ymin==-0.01 and ymax==0.01): axes101[1,1].locator_params(axis='y', nbins=3);
    axes101[1,1].set_xlabel('Time [yr]');
    axes101[1,1].set_ylabel('Percent Error [%]', color='k');
    axes101[1,1].legend(loc='best');
    axes101[1,1].set_title('Error in Porosity');
    if('creep' in deckname): axes101[1,1].set_xlim(0, 100);
    
    # adjust layout
    fig101.tight_layout();
    fig101.subplots_adjust(top=0.90);
    
    # save the figure
    fig101.savefig(deckname+'_'+fig_subname+'.png', dpi=300);
    
    # toss stuff
    plt.close(fig101);
    del(fig101, axes101);
  
  #######################################
  # Pressure and Permeability to gas
  #######################################
  if('fracture' in deckname or 'klinkenberg' in deckname):
    
    fig101, axes101 = plt.subplots(nrows=2, ncols=2, sharex=False, sharey=False, squeeze=True, figsize=(6.5, 4.0));
    fig101.suptitle(decktitle, fontsize=11);
    
    fig_subname = '03_pressure_permeability_gas';
    
    axes101[0,0].plot(bf_time_yr, bf_presbrin, 'b-', alpha=0.7, label='BRAGFLO');
    axes101[0,0].plot(pf_time_yr, pf_presbrin, 'r--', alpha=0.7, label='PFLOTRAN');
    axes101[0,0].xaxis.set_major_formatter(tck.FormatStrFormatter('%i'));
    #axes101[0,0].yaxis.set_major_formatter(tck.FormatStrFormatter('%.2f'));
    yfrmtr=ScalarFormatterForceFormat(); yfrmtr.set_powerlimits((-1,1));  yfrmtr.set_scientific(True);
    axes101[0,0].yaxis.set_major_formatter(yfrmtr);
    axes101[0,0].set_xlabel('Time [yr]');
    axes101[0,0].set_ylabel('Brine Pressure [Pa]', color='k');
    axes101[0,0].legend(loc='best');
    axes101[0,0].set_title('Brine Pressure');
    if('creep' in deckname): axes101[0,0].set_xlim(0, 100);
    
    axes101[0,1].plot(bf_time_yr, bf_permgasx, 'b-', alpha=0.7, label='BRAGFLO');
    axes101[0,1].plot(pf_time_yr, pf_permgasx, 'r--', alpha=0.7, label='PFLOTRAN');
    axes101[0,1].xaxis.set_major_formatter(tck.FormatStrFormatter('%i'));
    #axes101[0,1].yaxis.set_major_formatter(tck.FormatStrFormatter('%.2f'));
    yfrmtr=ScalarFormatterForceFormat(); yfrmtr.set_powerlimits((-1,1));  yfrmtr.set_scientific(True);
    axes101[0,1].yaxis.set_major_formatter(yfrmtr);
    axes101[0,1].set_xlabel('Time [yr]');
    axes101[0,1].set_ylabel('Permeability [m^2]', color='k');
    axes101[0,1].legend(loc='best');
    axes101[0,1].set_title('Permeability');
    if('creep' in deckname): axes101[0,1].set_xlim(0, 100);
     
    axes101[1,0].plot(bf_time_yr, err_pressbrin, 'r--', alpha=0.7, label='PFLOTRAN');
    axes101[1,0].xaxis.set_major_formatter(tck.FormatStrFormatter('%i'));
    axes101[1,0].yaxis.set_major_formatter(tck.FormatStrFormatter('%.2f'));
    ymin, ymax = axes101[1,0].get_ylim();
    axes101[1,0].set_ylim( np.sign(ymin)*np.maximum(np.abs(ymin),0.01), np.sign(ymax)*np.maximum(np.abs(ymax),0.01));
    ymin, ymax = axes101[1,0].get_ylim();
    if(ymin==-0.01 and ymax==0.01): axes101[1,0].locator_params(axis='y', nbins=3);
    axes101[1,0].set_xlabel('Time [yr]');
    axes101[1,0].set_ylabel('Percent Error [%]', color='k');
    axes101[1,0].legend(loc='best');
    axes101[1,0].set_title('Error in Brine Pressure');
    if('creep' in deckname): axes101[1,0].set_xlim(0, 100);
    
    axes101[1,1].plot(bf_time_yr, err_permgasx, 'r--', alpha=0.7, label='PFLOTRAN');
    axes101[1,1].xaxis.set_major_formatter(tck.FormatStrFormatter('%i'));
    axes101[1,1].yaxis.set_major_formatter(tck.FormatStrFormatter('%.2f'));
    ymin, ymax = axes101[1,1].get_ylim();
    axes101[1,1].set_ylim( np.sign(ymin)*np.maximum(np.abs(ymin),0.01), np.sign(ymax)*np.maximum(np.abs(ymax),0.01));
    ymin, ymax = axes101[1,1].get_ylim();
    if(ymin==-0.01 and ymax==0.01): axes101[1,1].locator_params(axis='y', nbins=3);
    axes101[1,1].set_xlabel('Time [yr]');
    axes101[1,1].set_ylabel('Percent Error [%]', color='k');
    axes101[1,1].legend(loc='best');
    axes101[1,1].set_title('Error in Permeability');
    if('creep' in deckname): axes101[1,1].set_xlim(0, 100);
    
    # adjust layout
    fig101.tight_layout();
    fig101.subplots_adjust(top=0.90);
    
    # save the figure
    fig101.savefig(deckname+'_'+fig_subname+'.png', dpi=300);
    
    # toss stuff
    plt.close(fig101);
    del(fig101, axes101);
  
  #######################################
  # Pressure and Gas Density
  #######################################
  if('rks' in deckname):
    
    fig101, axes101 = plt.subplots(nrows=2, ncols=2, sharex=False, sharey=False, squeeze=True, figsize=(6.5, 4.0));
    fig101.suptitle(decktitle, fontsize=11);
    
    fig_subname = '04_pressure_density_gas';
    
    axes101[0,0].plot(bf_time_yr, bf_presbrin, 'b-', alpha=0.7, label='BRAGFLO');
    axes101[0,0].plot(pf_time_yr, pf_presbrin, 'r--', alpha=0.7, label='PFLOTRAN');
    axes101[0,0].xaxis.set_major_formatter(tck.FormatStrFormatter('%i'));
    #axes101[0,0].yaxis.set_major_formatter(tck.FormatStrFormatter('%.2f'));
    yfrmtr=ScalarFormatterForceFormat(); yfrmtr.set_powerlimits((-1,1));  yfrmtr.set_scientific(True);
    axes101[0,0].yaxis.set_major_formatter(yfrmtr);
    axes101[0,0].set_xlabel('Time [yr]');
    axes101[0,0].set_ylabel('Brine Pressure [Pa]', color='k');
    axes101[0,0].legend(loc='best');
    axes101[0,0].set_title('Brine Pressure');
    if('creep' in deckname): axes101[0,0].set_xlim(0, 100);
    
    axes101[0,1].plot(bf_time_yr, bf_dengas, 'b-', alpha=0.7, label='BRAGFLO');
    axes101[0,1].plot(pf_time_yr, pf_dengas, 'r--', alpha=0.7, label='PFLOTRAN');
    axes101[0,1].xaxis.set_major_formatter(tck.FormatStrFormatter('%i'));
    axes101[0,1].yaxis.set_major_formatter(tck.FormatStrFormatter('%.2f'));
    axes101[0,1].set_xlabel('Time [yr]');
    axes101[0,1].set_ylabel('Gas Density [kg/m^3]', color='k');
    axes101[0,1].legend(loc='best');
    axes101[0,1].set_title('Gas Density');
    if('creep' in deckname): axes101[0,1].set_xlim(0, 100);
    
    axes101[1,0].plot(bf_time_yr, err_pressbrin, 'r--', alpha=0.7, label='PFLOTRAN');
    axes101[1,0].xaxis.set_major_formatter(tck.FormatStrFormatter('%i'));
    axes101[1,0].yaxis.set_major_formatter(tck.FormatStrFormatter('%.2f'));
    ymin, ymax = axes101[1,0].get_ylim();
    axes101[1,0].set_ylim( np.sign(ymin)*np.maximum(np.abs(ymin),0.01), np.sign(ymax)*np.maximum(np.abs(ymax),0.01));
    ymin, ymax = axes101[1,0].get_ylim();
    if(ymin==-0.01 and ymax==0.01): axes101[1,0].locator_params(axis='y', nbins=3);
    axes101[1,0].set_xlabel('Time [yr]');
    axes101[1,0].set_ylabel('Percent Error [%]', color='k');
    axes101[1,0].legend(loc='best');
    axes101[1,0].set_title('Error in Brine Pressure');
    if('creep' in deckname): axes101[1,0].set_xlim(0, 100);
    
    axes101[1,1].plot(bf_time_yr, err_dengas, 'r--', alpha=0.7, label='PFLOTRAN');
    axes101[1,1].xaxis.set_major_formatter(tck.FormatStrFormatter('%i'));
    axes101[1,1].yaxis.set_major_formatter(tck.FormatStrFormatter('%.2f'));
    ymin, ymax = axes101[1,1].get_ylim();
    axes101[1,1].set_ylim( np.sign(ymin)*np.maximum(np.abs(ymin),0.01), np.sign(ymax)*np.maximum(np.abs(ymax),0.01));
    ymin, ymax = axes101[1,1].get_ylim();
    if(ymin==-0.01 and ymax==0.01): axes101[1,1].locator_params(axis='y', nbins=3);
    axes101[1,1].set_xlabel('Time [yr]');
    axes101[1,1].set_ylabel('Percent Error [%]', color='k');
    axes101[1,1].legend(loc='best');
    axes101[1,1].set_title('Error in Gas Density');
    if('creep' in deckname): axes101[1,1].set_xlim(0, 100);
    
    # adjust layout
    fig101.tight_layout();
    fig101.subplots_adjust(top=0.90);
    
    # save the figure
    fig101.savefig(deckname+'_'+fig_subname+'.png', dpi=300);
    
    # toss stuff
    plt.close(fig101);
    del(fig101, axes101);
  
  
  # Plot gas generation data if available
  if( os.path.isfile(pnl1_filename) ):
    # Read pflotran pnl file
    pf_pnl_data = np.loadtxt(pnl1_filename, dtype='float64', skiprows=1);
      #RXN_0 = 1.0E-12 * 1.0E-2;
      #CONC_0 = 1.0E-12 * 1.0E-2;
    
    
    # Get Time arrays
    bf_time_yr = fh_bf_h5['/output_tables/simulation_statistics_table']['TIMESEC'] / YRSEC;
    pf_time_yr = pf_pnl_data[1:, dic_pf_pnl_header['TIME']] / YRSEC;
    
    # Get h2rate arrays
    bf_h2rate = fh_bf_h5['/output_tables/history_variables_table'][dic_bf_hist_header['H2RATE']];
    pf_h2rate = pf_pnl_data[1:, dic_pf_pnl_header['H2RATE']];
    # Sample pf values at bf time points
    pf_h2rate_at_bf_times = np.interp(bf_time_yr,  pf_time_yr, pf_h2rate);
    err_h2rate = 100.0*(bf_h2rate - pf_h2rate_at_bf_times) / np.maximum(bf_h2rate, 1.0e-1*np.amax(np.abs(bf_h2rate)) );
    
    # Get brinrate arrays
    bf_brinrate = fh_bf_h5['/output_tables/history_variables_table'][dic_bf_hist_header['BRINRATE']];
    pf_brinrate = pf_pnl_data[1:, dic_pf_pnl_header['BRINRATE']];
    # Sample pf values at bf time points
    pf_brinrate_at_bf_times = np.interp(bf_time_yr,  pf_time_yr, pf_brinrate);
    err_brinrate = 100.0*(bf_brinrate - pf_brinrate_at_bf_times) / np.maximum(bf_brinrate, 1.0e-1*np.amax(np.abs(bf_brinrate)) );
    print "err_brinrate: ",  np.amax(err_brinrate),  np.amin(err_brinrate);
    
    # Get feconc arrays
    bf_feconc = fh_bf_h5['/output_tables/history_variables_table'][dic_bf_hist_header['FECONC']];
    pf_feconc = pf_pnl_data[1:, dic_pf_pnl_header['FECONC']];
    # Sample pf values at bf time points
    pf_feconc_at_bf_times = np.interp(bf_time_yr,  pf_time_yr, pf_feconc);
    err_feconc = 100.0*(bf_feconc - pf_feconc_at_bf_times) / np.maximum(bf_feconc, 1.0e-1*np.amax(np.abs(bf_feconc)) );
    
    # Get cellconc arrays
    bf_cellconc = fh_bf_h5['/output_tables/history_variables_table'][dic_bf_hist_header['CELLCONC']];
    pf_cellconc = pf_pnl_data[1:, dic_pf_pnl_header['CELLCONC']];
    # Sample pf values at bf time points
    pf_cellconc_at_bf_times = np.interp(bf_time_yr,  pf_time_yr, pf_cellconc);
    err_cellconc = 100.0*(bf_cellconc - pf_cellconc_at_bf_times) / np.maximum(bf_cellconc, 1.0e-1*np.amax(np.abs(bf_cellconc)) );
    
    # Get mgo_hr arrays
    bf_mgo_hr = fh_bf_h5['/output_tables/history_variables_table'][dic_bf_hist_header['MGO_HR']];
    pf_mgo_hr = pf_pnl_data[1:, dic_pf_pnl_header['MGO_HR']];
    # Sample pf values at bf time points
    pf_mgo_hr_at_bf_times = np.interp(bf_time_yr,  pf_time_yr, pf_mgo_hr);
    mgo_hr0 = 1.0e-1*np.amax(np.abs(bf_mgo_hr));
    if(mgo_hr0==0.0): mgo_hr0 = 1.0;
    err_mgo_hr = 100.0*(bf_mgo_hr - pf_mgo_hr_at_bf_times) / np.maximum(bf_mgo_hr, mgo_hr0 );
    
    # Get mgoc arrays
    bf_mgoc = fh_bf_h5['/output_tables/history_variables_table'][dic_bf_hist_header['MGOC']];
    pf_mgoc = pf_pnl_data[1:, dic_pf_pnl_header['MGOC']];
    # Sample pf values at bf time points
    pf_mgoc_at_bf_times = np.interp(bf_time_yr,  pf_time_yr, pf_mgoc);
    err_mgoc = 100.0*(bf_mgoc - pf_mgoc_at_bf_times) / np.maximum(bf_mgoc, 1.0e-1*np.amax(np.abs(bf_mgoc)) );
    
    
    # Plot stuff
    #  matplotlib.pyplot.subplots(nrows=1, ncols=1, sharex=False, sharey=False, squeeze=True, subplot_kw=None, gridspec_kw=None, **fig_kw)
    fig201, axes201 = plt.subplots(nrows=2, ncols=2, sharex=False, sharey=False, squeeze=True, figsize=(6.5, 4.0));
    fig201.suptitle(decktitle, fontsize=11);
    
    fig_subname = '05_h2rate_brinrate';
    
    axes201[0,0].plot(bf_time_yr, bf_h2rate, 'b-', alpha=0.7, label='BRAGFLO');
    axes201[0,0].plot(pf_time_yr, pf_h2rate, 'r--', alpha=0.7, label='PFLOTRAN');
    axes201[0,0].xaxis.set_major_formatter(tck.FormatStrFormatter('%i'));
    #axes201[0,0].yaxis.set_major_formatter(tck.FormatStrFormatter('%.1e'));
    yfrmtr=ScalarFormatterForceFormat(); yfrmtr.set_powerlimits((-1,1));  yfrmtr.set_scientific(True);
    axes201[0,0].yaxis.set_major_formatter(yfrmtr);
    axes201[0,0].yaxis.get_offset_text().set_size(8); 
    axes201[0,0].set_xlabel('Time [yr]');
    axes201[0,0].set_ylabel('H2 Rate [kg/m3/s]', color='k');
    axes201[0,0].legend(loc='best');
    axes201[0,0].set_title('H2 Generation Rate');
    if('creep' in deckname): axes201[0,0].set_xlim(0, 100);
    
    
    axes201[0,1].plot(bf_time_yr, bf_brinrate, 'b-', alpha=0.7, label='BRAGFLO');
    axes201[0,1].plot(pf_time_yr, pf_brinrate, 'r--', alpha=0.7, label='PFLOTRAN');
    axes201[0,1].xaxis.set_major_formatter(tck.FormatStrFormatter('%i'));
    #axes201[0,1].yaxis.set_major_formatter(tck.FormatStrFormatter('%.2f'));
    yfrmtr=ScalarFormatterForceFormat(); yfrmtr.set_powerlimits((-1,1));  yfrmtr.set_scientific(True);
    axes201[0,1].yaxis.set_major_formatter(yfrmtr);
    axes201[0,1].yaxis.get_offset_text().set_size(8); 
    axes201[0,1].set_xlabel('Time [yr]');
    axes201[0,1].set_ylabel('Brine Rate [kg/m3/s]', color='k');
    axes201[0,1].legend(loc='best');
    axes201[0,1].set_title('Brine Generation Rate');
    if('creep' in deckname): axes201[0,1].set_xlim(0, 100);
    
    
    axes201[1,0].plot(bf_time_yr, err_h2rate, 'r--', alpha=0.7, label='PFLOTRAN');
    axes201[1,0].xaxis.set_major_formatter(tck.FormatStrFormatter('%i'));
    axes201[1,0].yaxis.set_major_formatter(tck.FormatStrFormatter('%.2f'));
    ymin, ymax = axes201[1,0].get_ylim();
    axes201[1,0].set_ylim( np.sign(ymin)*np.maximum(np.abs(ymin),0.01), np.sign(ymax)*np.maximum(np.abs(ymax),0.01));
    ymin, ymax = axes201[1,0].get_ylim();
    if(ymin==-0.01 and ymax==0.01): axes201[1,0].locator_params(axis='y', nbins=3);
    axes201[1,0].set_ylim( np.sign(ymin)*np.minimum(np.abs(ymin),25.0), np.sign(ymax)*np.minimum(np.abs(ymax),25.0));
    axes201[1,0].set_xlabel('Time [yr]');
    axes201[1,0].set_ylabel('Percent Error [%]', color='k');
    axes201[1,0].legend(loc='best');
    axes201[1,0].set_title('Error in H2 Rate');
    if('creep' in deckname): axes201[1,0].set_xlim(0, 100);
    
    axes201[1,1].plot(bf_time_yr, err_brinrate, 'r--', alpha=0.7, label='PFLOTRAN');
    axes201[1,1].xaxis.set_major_formatter(tck.FormatStrFormatter('%i'));
    axes201[1,1].yaxis.set_major_formatter(tck.FormatStrFormatter('%.2f'));
    ymin, ymax = axes201[1,1].get_ylim();
    axes201[1,1].set_ylim( np.sign(ymin)*np.maximum(np.abs(ymin),0.01), np.sign(ymax)*np.maximum(np.abs(ymax),0.01));
    ymin, ymax = axes201[1,1].get_ylim();
    if(ymin==-0.01 and ymax==0.01): axes201[1,1].locator_params(axis='y', nbins=3);
    axes201[1,1].set_ylim( np.sign(ymin)*np.minimum(np.abs(ymin),25.0), np.sign(ymax)*np.minimum(np.abs(ymax),25.0));
    axes201[1,1].set_xlabel('Time [yr]');
    axes201[1,1].set_ylabel('Percent Error [%]', color='k');
    axes201[1,1].legend(loc='best');
    axes201[1,1].set_title('Error in Brine Rate');
    if('creep' in deckname): axes201[1,1].set_xlim(0, 100);
    
    # adjust layout
    fig201.tight_layout();
    fig201.subplots_adjust(top=0.90);
    
    # append to pdf
    #pdf_pages6.savefig(fig201);
    fig201.savefig(deckname+'_'+fig_subname+'.png', dpi=300);
    
    # toss stuff
    plt.close(fig201);
    del(fig201, axes201);
  
  
    # Plot stuff
    #  matplotlib.pyplot.subplots(nrows=1, ncols=1, sharex=False, sharey=False, squeeze=True, subplot_kw=None, gridspec_kw=None, **fig_kw)
    fig201, axes201 = plt.subplots(nrows=2, ncols=2, sharex=False, sharey=False, squeeze=True, figsize=(6.5, 4.0));
    fig201.suptitle(decktitle, fontsize=11);
    
    fig_subname = '06_feconc_cellconc';
    
    axes201[0,0].plot(bf_time_yr, bf_feconc, 'b-', alpha=0.7, label='BRAGFLO');
    axes201[0,0].plot(pf_time_yr, pf_feconc, 'r--', alpha=0.7, label='PFLOTRAN');
    axes201[0,0].xaxis.set_major_formatter(tck.FormatStrFormatter('%i'));
    #axes201[0,0].yaxis.set_major_formatter(tck.FormatStrFormatter('%.2f'));
    yfrmtr=ScalarFormatterForceFormat(); yfrmtr.set_powerlimits((-1,1));  yfrmtr.set_scientific(True);
    axes201[0,0].yaxis.set_major_formatter(yfrmtr);
    axes201[0,0].set_xlabel('Time [yr]');
    axes201[0,0].set_ylabel('Fe [kg/m3]', color='k');
    axes201[0,0].legend(loc='best');
    axes201[0,0].set_title('Fe Concentration');
    if('creep' in deckname): axes201[0,0].set_xlim(0, 100);
    
    
    axes201[0,1].plot(bf_time_yr, bf_cellconc, 'b-', alpha=0.7, label='BRAGFLO');
    axes201[0,1].plot(pf_time_yr, pf_cellconc, 'r--', alpha=0.7, label='PFLOTRAN');
    axes201[0,1].xaxis.set_major_formatter(tck.FormatStrFormatter('%i'));
    #axes201[0,1].yaxis.set_major_formatter(tck.FormatStrFormatter('%.2f'));
    yfrmtr=ScalarFormatterForceFormat(); yfrmtr.set_powerlimits((-1,1));  yfrmtr.set_scientific(True);
    axes201[0,1].yaxis.set_major_formatter(yfrmtr);
    axes201[0,1].set_xlabel('Time [yr]');
    axes201[0,1].set_ylabel('Cellulose [kg/m3]', color='k');
    axes201[0,1].legend(loc='best');
    axes201[0,1].set_title('Cellulose Concentration');
    if('creep' in deckname): axes201[0,1].set_xlim(0, 100);
    
    
    axes201[1,0].plot(bf_time_yr, err_feconc, 'r--', alpha=0.7, label='PFLOTRAN');
    axes201[1,0].xaxis.set_major_formatter(tck.FormatStrFormatter('%i'));
    axes201[1,0].yaxis.set_major_formatter(tck.FormatStrFormatter('%.2f'));
    ymin, ymax = axes201[1,0].get_ylim();
    axes201[1,0].set_ylim( np.sign(ymin)*np.maximum(np.abs(ymin),0.01), np.sign(ymax)*np.maximum(np.abs(ymax),0.01));
    ymin, ymax = axes201[1,0].get_ylim();
    if(ymin==-0.01 and ymax==0.01): axes201[1,0].locator_params(axis='y', nbins=3);
    axes201[1,0].set_ylim( np.sign(ymin)*np.minimum(np.abs(ymin),25.0), np.sign(ymax)*np.minimum(np.abs(ymax),25.0));
    axes201[1,0].set_xlabel('Time [yr]');
    axes201[1,0].set_ylabel('Percent Error [%]', color='k');
    axes201[1,0].legend(loc='best');
    axes201[1,0].set_title('Error in Fe Concentration');
    if('creep' in deckname): axes201[1,0].set_xlim(0, 100);
    
    axes201[1,1].plot(bf_time_yr, err_cellconc, 'r--', alpha=0.7, label='PFLOTRAN');
    axes201[1,1].xaxis.set_major_formatter(tck.FormatStrFormatter('%i'));
    axes201[1,1].yaxis.set_major_formatter(tck.FormatStrFormatter('%.2f'));
    ymin, ymax = axes201[1,1].get_ylim();
    axes201[1,1].set_ylim( np.sign(ymin)*np.maximum(np.abs(ymin),0.01), np.sign(ymax)*np.maximum(np.abs(ymax),0.01));
    ymin, ymax = axes201[1,1].get_ylim();
    if(ymin==-0.01 and ymax==0.01): axes201[1,1].locator_params(axis='y', nbins=3);
    axes201[1,1].set_ylim( np.sign(ymin)*np.minimum(np.abs(ymin),25.0), np.sign(ymax)*np.minimum(np.abs(ymax),25.0));
    axes201[1,1].set_xlabel('Time [yr]');
    axes201[1,1].set_ylabel('Percent Error [%]', color='k');
    axes201[1,1].legend(loc='best');
    axes201[1,1].set_title('Error Cellulose Concentration');
    if('creep' in deckname): axes201[1,1].set_xlim(0, 100);
    
    # adjust layout
    fig201.tight_layout();
    fig201.subplots_adjust(top=0.90);
    
    # append to pdf
    #pdf_pages6.savefig(fig201);
    fig201.savefig(deckname+'_'+fig_subname+'.png', dpi=300);
    
    # toss stuff
    plt.close(fig201);
    del(fig201, axes201);
    
    
    # Plot stuff
    #  matplotlib.pyplot.subplots(nrows=1, ncols=1, sharex=False, sharey=False, squeeze=True, subplot_kw=None, gridspec_kw=None, **fig_kw)
    fig201, axes201 = plt.subplots(nrows=2, ncols=2, sharex=False, sharey=False, squeeze=True, figsize=(6.5, 4.0));
    fig201.suptitle(decktitle, fontsize=11);
    
    fig_subname = '07_mgo_hr_mgoc';
    
    axes201[0,0].plot(bf_time_yr, bf_mgo_hr, 'b-', alpha=0.7, label='BRAGFLO');
    axes201[0,0].plot(pf_time_yr, pf_mgo_hr, 'r--', alpha=0.7, label='PFLOTRAN');
    axes201[0,0].xaxis.set_major_formatter(tck.FormatStrFormatter('%i'));
    #axes201[0,0].yaxis.set_major_formatter(tck.FormatStrFormatter('%.2f'));
    yfrmtr=ScalarFormatterForceFormat(); yfrmtr.set_powerlimits((-1,1));  yfrmtr.set_scientific(True);
    axes201[0,0].yaxis.set_major_formatter(yfrmtr);
    axes201[0,0].set_xlabel('Time [yr]');
    axes201[0,0].set_ylabel('MgO Hyd. Rate [kg/m3/s]', color='k');
    axes201[0,0].legend(loc='best');
    axes201[0,0].set_title('MgO Hydration Rate');
    #if('creep' in deckname): axes201[0,0].set_xlim(0, 100);
    axes201[0,0].set_xlim(0, 25);
    
    
    axes201[0,1].plot(bf_time_yr, bf_mgoc, 'b-', alpha=0.7, label='BRAGFLO');
    axes201[0,1].plot(pf_time_yr, pf_mgoc, 'r--', alpha=0.7, label='PFLOTRAN');
    axes201[0,1].xaxis.set_major_formatter(tck.FormatStrFormatter('%i'));
    #axes201[0,1].yaxis.set_major_formatter(tck.FormatStrFormatter('%.2f'));
    yfrmtr=ScalarFormatterForceFormat(); yfrmtr.set_powerlimits((-1,1));  yfrmtr.set_scientific(True);
    axes201[0,1].yaxis.set_major_formatter(yfrmtr);
    axes201[0,1].set_xlabel('Time [yr]');
    axes201[0,1].set_ylabel('MgO [kg/m3]', color='k');
    axes201[0,1].legend(loc='best');
    axes201[0,1].set_title('MgO Concentration');
    #if('creep' in deckname): axes201[0,1].set_xlim(0, 100);
    axes201[0,1].set_xlim(0, 25);
    
    axes201[1,0].plot(bf_time_yr, err_mgo_hr, 'r--', alpha=0.7, label='PFLOTRAN');
    axes201[1,0].xaxis.set_major_formatter(tck.FormatStrFormatter('%i'));
    axes201[1,0].yaxis.set_major_formatter(tck.FormatStrFormatter('%.2f'));
    ymin, ymax = axes201[1,0].get_ylim();
    axes201[1,0].set_ylim( np.sign(ymin)*np.maximum(np.abs(ymin),0.01), np.sign(ymax)*np.maximum(np.abs(ymax),0.01));
    ymin, ymax = axes201[1,0].get_ylim();
    if(ymin==-0.01 and ymax==0.01): axes201[1,0].locator_params(axis='y', nbins=3);
    axes201[1,0].set_ylim( np.sign(ymin)*np.minimum(np.abs(ymin),25.0), np.sign(ymax)*np.minimum(np.abs(ymax),25.0));
    axes201[1,0].set_xlabel('Time [yr]');
    axes201[1,0].set_ylabel('Percent Error [%]', color='k');
    axes201[1,0].legend(loc='best');
    axes201[1,0].set_title('Error in MgO Rate');
    #if('creep' in deckname): axes201[1,0].set_xlim(0, 100);
    axes201[1,0].set_xlim(0, 25);
    
    axes201[1,1].plot(bf_time_yr, err_mgoc, 'r--', alpha=0.7, label='PFLOTRAN');
    axes201[1,1].xaxis.set_major_formatter(tck.FormatStrFormatter('%i'));
    axes201[1,1].yaxis.set_major_formatter(tck.FormatStrFormatter('%.2f'));
    ymin, ymax = axes201[1,1].get_ylim();
    axes201[1,1].set_ylim( np.sign(ymin)*np.maximum(np.abs(ymin),0.01), np.sign(ymax)*np.maximum(np.abs(ymax),0.01));
    ymin, ymax = axes201[1,1].get_ylim();
    if(ymin==-0.01 and ymax==0.01): axes201[1,1].locator_params(axis='y', nbins=3);
    axes201[1,0].set_ylim( np.sign(ymin)*np.minimum(np.abs(ymin),25.0), np.sign(ymax)*np.minimum(np.abs(ymax),25.0));
    axes201[1,1].set_xlabel('Time [yr]');
    axes201[1,1].set_ylabel('Percent Error [%]', color='k');
    axes201[1,1].legend(loc='best');
    axes201[1,1].set_title('Error in MgO Concentration');
    #if('creep' in deckname): axes201[1,1].set_xlim(0, 100);
    axes201[1,1].set_xlim(0, 25);
    
    # adjust layout
    fig201.tight_layout();
    fig201.subplots_adjust(top=0.90);
    
    # append to pdf
    #pdf_pages6.savefig(fig201);
    fig201.savefig(deckname+'_'+fig_subname+'.png', dpi=300);
    
    # toss stuff
    plt.close(fig201);
    del(fig201, axes201);
  
  
  
  
  
  
  
  
  
  # toss stuff
  del(pf_tecplot_data);
  fh_bf_h5.close();
  
  # go back
  os.chdir('..')

# end for

# fig1.savefig('fig_case060100.png', dpi=150);
#plt.show();

#pdf_pages6.close();
