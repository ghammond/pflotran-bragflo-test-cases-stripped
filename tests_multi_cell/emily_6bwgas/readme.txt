Four simulations in this directory:
bf_6b_wgas - identical to the original
bf_6b_wgas_nomaxd - above plus PRESNORM and SATNORM set to 1.d20
pf_6b_wgas - NOT identical to the original - has DT_FACTOR with max of 1.25
pf_6b_wgas_maxd - above plus MAX_PRESSURE_CHANGE 5.5d5 and MAX_SATURATION_CHANGE 0.3d0


