$BRAGFLO_DIR/BRAGFLO/Source/bragflo -input bf_markerbeds.inp -binary bf_markerbeds.bin -output bf_markerbeds.out -summary bf_markerbeds.sum -rout bf_markerbeds.rout #> bf_markerbeds.stdout
python $BINREAD_DIR/binread.py --plane xy --postarray pcgw --binary bf_markerbeds.bin --hdf5 bf_markerbeds.h5 --unitgrid true --snapshot_timeunit y --round_time false --ascii bf_markerbeds.txt

#if necessary
#export PFLOTRAN_SRC=$PFLOTRAN_DIR/src/pflotran
$PFLOTRAN_SRC/pflotran -input_prefix pf_markerbeds > pf_markerbeds.stdout

#python bfpf_compare.py bf_markerbeds pf_markerbeds 3 5

