'''Make pflotran explicit unstructured grid and .ex files from dimensions in
   bf_flared.inp. Copy and paste!
   Use pflotran convention: dy is into the page, dz is vertical
   Largely copied from wipp_uge_slim.py in prepflotran
   pm_bragflo version using alpha and elevation
   includes dip = 0 !!! ie no dip
   11 cell tall version for time-5y case
   Emily 12.15.2017
'''
import numpy as np
from h5py import *

def make_grid():
  ugename = 'pf_time-5y_5x11.uge'
  domainname = 'pf_time-5y_5x11.h5'
  domainequal = 'pf_time-5y_5x11_equal.h5'
  topname = 'top.ex'

  xwidth = [2.9,2.0,43.8,10.0,2.0]
  ywidth = [998.53,988.73,126.20,28.80,4.8]
  zwidth = [0.85,0.69,0.69,1.32,1.32,1.32,2.62,0.27,4.53,4.53,0.18]
  dip = 0. #degree

  #for init cond calculation, close enough
  gravacc = 9.80665
  dnsfluid = 1220.

  nrow = len(zwidth)
  ncol = len(xwidth)
  ncell = nrow*ncol

  ycenter = 0.
  xcenter = []
  zcenter = []
  xarea = [] #xcoord of x connection
  zarea = [] #zcoord of z connection
  vol = []
  dx = []
  dy = []
  dz = []
  top_area = []
  vertices = []
  elevation = []
  initpres = []

  x0 = sum(xwidth)/len(xwidth)
  z0 = sum(zwidth)/len(zwidth)

  z1 = 0.
  for j in range(nrow):
    x1 = 0.
    z2 = z1 + zwidth[j]
    for i in range(ncol):
      #cell and face centroids, cell volume
      x2 = x1 + xwidth[i]
      xcenter.append((x1+x2)/2.) #xcoord cell centroid
      vol.append(xwidth[i]*ywidth[i]*zwidth[j])
      xarea.append(x2)           #xcoord face centroid
      zcenter.append((z1+z2)/2.) #zcoord cell centroid
      zarea.append(z2)           #zcoord face centroid
      #add dip
      elevation.append(np.cos(np.radians(dip))*(zcenter[-1] - z0)
                        + np.sin(np.radians(dip))*(xcenter[-1]-x0) + z0)
      #connection areas
      dx.append(xwidth[i]) #will be written as z connection area
      dy.append(ywidth[i]) #will be written as alpha
      dz.append(zwidth[j]) #will be written as x connection area
      #need boundary areas for top of grid only
      if j == nrow-1: #top
        top_area.append(xwidth[i]*ywidth[i])
      #vertices - for visualization
      y2 = ywidth[i]/2.
      y1 = -y2
      vertices.extend([[x1,y1,z1],[x2,y1,z1],[x2,y2,z1],[x1,y2,z1]]) #these are listed in order!
      x1 += xwidth[i] #update for next iter
    z1 += zwidth[j]  #update for next iter
  #save last set of vertices
  x1 = 0. #z1 is correct after last iter of previous loop
  for i in range(ncol):
    x2 = x1+xwidth[i] #x and y need to be recalc'd
    y2 = ywidth[i]/2.
    y1 = -y2
    vertices.extend([[x1,y1,z1],[x2,y1,z1],[x2,y2,z1],[x1,y2,z1]]) #these are listed in order!
    x1 += xwidth[i]
  print('top of domain is at z = %f m' % z2)
  print('end of domain is at z = %f m' % x2)
  #open grid (.uge) and boundary connection (.ex) files
  f2 = open(ugename,'w')
  f3 = open(topname,'w')
  #write cells to .uge
  f2.write('CELLS %i\n' % ncell)
  for j in range(nrow):
    for i in range(ncol):
      index = i+(j*ncol)
      cellid = index+1
      f2.write('%i %f %f %f %f\n' % (cellid,
                                     xcenter[index],
                                     ycenter,
                                     zcenter[index],
                                     vol[index]))
  #write connections to .uge and .ex files
  ncon = (ncol-1)*nrow + ncol*(nrow-1)
  f2.write('CONNECTIONS %i\n' % (ncon)) #cons within grid
  f3.write('CONNECTIONS %i\n' % (ncol)) #cons for top bc
  for j in range(nrow):
    for i in range(ncol):
      index = i+(j*ncol)
      cellid = index+1
      if i < ncol-1: #x connections within grid
        right = cellid + 1
        f2.write('%i %i %f %f %f %f\n' % (cellid,right,
                                          xarea[index],  #xcoord
                                          ycenter,       #ycoord
                                          zcenter[index],#zcoord
                                          dz[index]))
      if j < nrow-1: #z connections within grid
        up = cellid + ncol
        f2.write('%i %i %f %f %f %f\n' % (cellid,
                                          up,
                                          xcenter[index],
                                          ycenter,
                                          zarea[index],
                                          dx[index]))
      elif j == nrow -1: #top
        f3.write('%i %f %1.6e %1.6e %f\n' % (cellid,
                                       xcenter[index],
                                       ycenter,
                                       z2,
                                       top_area[i]))

          
  f2.close()
  print('.uge file written')

  #write .h5 files for use with Paraview
  #save centers XC,YC,ZC for h5 file
  xarray = np.array(xcenter,dtype=np.float64)
  yarray = np.zeros(len(xcenter),dtype=np.float64)
  zarray = np.array(zcenter,dtype=np.float64)
  elarray = np.array(elevation,'=f8')
  volarray = np.array(vol,dtype=np.float64)
  #store vertices and elements for h5 file
  varray = np.array(vertices,dtype=np.float64)
  nel = ncell
  earray = np.zeros(nel*9,'=i4')
  for j in range(nrow):  #0-indexed vertices to define elements/cells
    for i in range(ncol):
      iel = i*9+j*ncol*9
      index = i*4+(j*ncol*4)
      index2 = i*4+(j+1)*ncol*4 #one row up
      this_el = np.array([9,index,index+1,index+2,index+3,
                          index2,index2+1,index2+2,index2+3],dtype=np.float64)
      earray[iel:iel+9] = this_el
  #write elements/cells and vertices to h5 file for paraview
  iarray = np.zeros((ncell),'i4')
  for i in range(ncell):
    iarray[i] = i+1
  h5f = File(domainname,'w')
  h5f.create_dataset('Domain/Cells', data=earray)
  h5f.create_dataset('Domain/Vertices',data=varray)
  h5f.create_dataset('Domain/XC',data=xarray)
  h5f.create_dataset('Domain/YC',data=yarray)
  h5f.create_dataset('Domain/ZC',data=zarray)
  h5f.create_dataset('Domain/Elevation',data=elarray)
  h5f.create_dataset('Domain/Volumes',data=volarray)
  h5f.create_dataset('Domain/Cell_Ids',data=iarray)
  h5f.close()
  #second domain with equally spaced vertices,just for viz
  #observe pflotran_cell_numbering_schemes.pdf
  varray = np.zeros((2*(ncol+1)*(nrow+1),3),'f8')
  for k in range(2):
    for j in range(nrow+1):
      for i in range(ncol+1):
        index = i + j*(ncol+1) + k*(ncol+1)*(nrow+1)
        varray[index] = [float(i),float(k)-0.5,float(j)]
  earray = np.zeros(ncol*nrow*9,'i4')
  for j in range(nrow):
    for i in range(ncol):
      iel = i*9+j*ncol*9
      n1 = i+j*(ncol+1)
      n4 = (ncol+1)*(nrow+1) + n1
      n5 = (i+(j+1)*(ncol+1))
      n8 = (ncol+1)*(nrow+1) + n5
      this_el = np.array([9,n1,n1+1,n4+1,n4,n5,n5+1,n8+1,n8])
      earray[iel:iel+9] = this_el
  h5f = File(domainequal,'w')
  h5f.create_dataset('Domain/Cells', data=earray)
  h5f.create_dataset('Domain/Vertices',data=varray)
  h5f.create_dataset('Domain/XC',data=xarray)
  h5f.create_dataset('Domain/YC',data=yarray)
  h5f.create_dataset('Domain/ZC',data=zarray)
  h5f.create_dataset('Domain/Elevations',data=elarray)
  h5f.create_dataset('Domain/Volumes',data=volarray)
  h5f.create_dataset('Domain/Cell_Ids',data=iarray)
  h5f.close()

  #write alpha and elev and soil reference pressure and initial P and S
  aarray = np.array(dy,'=f8')
  iarray = np.arange(1,len(dy)+1)
  h5f = File('alpha.h5','w')
  h5f.create_dataset('Cell Ids',data=iarray)
  h5f.create_dataset('Alpha',data=aarray)
  h5f.close()

  h5f = File('elevation.h5','w')
  h5f.create_dataset('Cell Ids',data=iarray)
  h5f.create_dataset('Elevation',data=elarray)
  h5f.close()
  with open('bf_elevations.txt','w') as f:
    for elev in elevation:
      f.write('1*{:21.15e}\n'.format(elev))
  
  for elev in elevation:
    initpres.append((z2-elev)*dnsfluid*gravacc + 1.2e7)
  iparray = np.array(initpres,'=f8')
  #iparray = np.full(ncell,4.e6,'=f8')
  iparray[17:20] = 101325.
  iparray[22:25] = 101325.
  iparray[27:30] = 101325.
  isarray = np.zeros(ncell,'=f8')
  isarray[17:20] = 1. #gas saturation
  isarray[22:25] = 1. #gas saturation
  isarray[27:30] = 1. #gas saturation
  h5f = File('initcond.h5','w')
  h5f.create_dataset('Cell Ids',data=iarray)
  h5f.create_dataset('Liquid_Pressure',data=iparray)
  h5f.create_dataset('Gas_Saturation',data=isarray)
  h5f.create_dataset('soil_reference_pressure',data=iparray)
  h5f.close()
  with open('bf_initpres.txt','w') as f:
    for pres in iparray:
      f.write('1*{:21.15e}\n'.format(pres))
  
  with open('bf_initsat.txt','w') as f:
    for sat in isarray:
      f.write('1*{:21.15e}\n'.format(1.-sat))
  

  return

if __name__ == '__main__':
  make_grid()
  print('done')

