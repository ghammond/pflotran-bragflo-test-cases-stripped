!==============================================================================
! Test Case 1 - Pressure drawdown with 1D radial grid, single-phase flow
! In this 1d radial model, 
! water is produced at constant rate 12.33kg/s from the left (center) cell for
! 5000s, then stopped (i.e. the well is shut-in).
! Pressure is held constant at the right boundary, mimicking an infinite reservoir.
! This simulates a pressure transient before the pressure disturbance reaches 
! the boundary, and thus the analytical solution for the pressure diffusivity 
! equation in a radial, semi-inifinte reservoir applies for early times.
!
! Note: for this case, BRAGFLO uses g=9.79, STP 300.  1.01325E+05
!
! 8/21/2017 JB Modified optional tolerances
! 8/21/2017 JB Modified with parameters from BD's test case 6a
!==============================================================================

!=========================== SIMULATION MOD ===================================
SIMULATION
  SIMULATION_TYPE SUBSURFACE
  PROCESS_MODELS
    SUBSURFACE_FLOW FLOW
      MODE WIPP_FLOW
      OPTIONS
        EXTERNAL_FILE ../../block_options_multicell.txt
        NO_GAS_GENERATION
      END
    END
  END
END

!==============================================================================
SUBSURFACE
!==============================================================================

!=========================== TIMESTEPPER ======================================
EXTERNAL_FILE ../../block_timestepper.txt

!=========================== SOLVER OPTIONS ===================================
EXTERNAL_FILE ../../block_solver_options.txt

!=========================== regression =======================================
REGRESSION
  CELLS_PER_PROCESS 10
  CELLS
    1
    50
  /
END

!=========================== discretization ===================================
GRID
  TYPE structured cylindrical
  NXYZ 50 1 1    ! 1D radial problem
  DXYZ
  0.1000000E-01  0.1300000E-01  0.1690000E-01  0.2197000E-01  0.2856100E-01 \
  0.3712929E-01  0.4826808E-01  0.6274850E-01  0.8157305E-01  0.1060450E+00 \
  0.1378584E+00  0.1792160E+00  0.2329807E+00  0.3028749E+00  0.3937374E+00 \
  0.5118586E+00  0.6654161E+00  0.8650409E+00  0.1124553E+01  0.1461919E+01 \
  0.1900495E+01  0.2470643E+01  0.3211836E+01  0.4175386E+01  0.5428001E+01 \
  0.7056402E+01  0.9173322E+01  0.1192532E+02  0.1550291E+02  0.2015379E+02 \
  0.2619992E+02  0.3405989E+02  0.4427786E+02  0.5756122E+02  0.7482958E+02 \
  0.9727845E+02  0.1264620E+03  0.1644006E+03  0.2137207E+03  0.2778369E+03 \
  0.3611880E+03  0.4695444E+03  0.6104077E+03  0.7935300E+03  0.1031589E+04 \
  0.1341066E+04  0.1743385E+04  0.2266401E+04  0.2946321E+04  0.3830217E+04
  1.0
  1.0
  /
END


!=========================== fluid properties =================================
FLUID_PROPERTY
  PHASE LIQUID
  DIFFUSION_COEFFICIENT 0.d0
END

FLUID_PROPERTY
  PHASE GAS
  DIFFUSION_COEFFICIENT 0.d0
END

EOS WATER
  DENSITY EXPONENTIAL 1.2200d+03  101325.d0  3.1000E-10 ! ref_dens ref_pres compres
  VISCOSITY CONSTANT 2.10000E-03  ! VISC_BR in BF
  ENTHALPY CONSTANT 1.8890d6    ! default water enthalpy constant
END

EOS GAS
  VISCOSITY CONSTANT 8.93389000E-06
  HENRYS_CONSTANT CONSTANT 1.d10
  DENSITY IDEAL
END


!=========================== material properties ==============================
MATERIAL_PROPERTY soil1
  ID 1
  POROSITY 0.45d0
  TORTUOSITY 1.                  ! for diffusion calculations.
  ROCK_DENSITY 2650.d0           ! thermal parameter. isothermal ignores this.
  THERMAL_CONDUCTIVITY_DRY 0.5   ! thermal parameter. isothermal ignores this.
  THERMAL_CONDUCTIVITY_WET 2.    ! thermal parameter. isothermal ignores this.
  HEAT_CAPACITY 830.             ! thermal parameter. isothermal ignores this.
!  SOIL_COMPRESSIBILITY_FUNCTION BRAGFLO
!  BULK_COMPRESSIBILITY 2.0250d-9
  SOIL_REFERENCE_PRESSURE INITIAL_PRESSURE
  PERMEABILITY
    PERM_X_LOG10 -17.0
    PERM_Y_LOG10 -17.0
    PERM_Z_LOG10 -17.0
  END
  CHARACTERISTIC_CURVES sf1
END

CHARACTERISTIC_CURVES sf1
  SATURATION_FUNCTION BRAGFLO_KRP4
    KPC 2
    LAMBDA 7.0000000d-01
    PCT_A 8.300000d+04
    PCT_EXP 0.000000d+00
    LIQUID_RESIDUAL_SATURATION 2.0000000d-01
    GAS_RESIDUAL_SATURATION 1.d-05
    MAX_CAPILLARY_PRESSURE 1.0000000d+08
  END
  PERMEABILITY_FUNCTION BRAGFLO_KRP4_LIQ
    PHASE LIQUID
    LAMBDA 7.0000000d-01
    LIQUID_RESIDUAL_SATURATION 2.0000000d-01
    GAS_RESIDUAL_SATURATION 1.d-05
  END
  PERMEABILITY_FUNCTION BRAGFLO_KRP4_GAS
    PHASE GAS
    LAMBDA 0.7d0
    LIQUID_RESIDUAL_SATURATION 2.0000000d-01
    GAS_RESIDUAL_SATURATION 1.d-05
  END
END

!=========================== regions ==========================================
REGION all
  COORDINATES
    0.d0 0.d0 0.d0
    16597.57597492 1.d0 1.d0
  /
END

REGION center
  COORDINATES
    0.d0 0.d0 0.d0
    0.0d0 1.d0 1.d0
  /
END

REGION outside
  FACE east
  COORDINATES
    16597.57597492 0.d0 0.d0
    16597.57597492 1.d0 1.d0
  /
END

REGION obspoint
  COORDINATES
  100.0  0.0 0.5
/
END


!=========================== stratigraphy couplers ============================
STRATA
  REGION all
  MATERIAL soil1
END


!=========================== flow conditions ==================================
FLOW_CONDITION initial
  TYPE
    LIQUID_PRESSURE DIRICHLET
    LIQUID_SATURATION DIRICHLET
  /
  LIQUID_PRESSURE 1.D7
  LIQUID_SATURATION 1.d0
END

FLOW_CONDITION well
  TYPE
    RATE mass_rate
  /
  RATE LIST
    TIME_UNITS y
    DATA_UNITS kg/y kg/y MW
    0.d0 -616.5d0  0.d0  ! time(y) water(kg/y) gas(kg/y) 
    5.d3   0.d0  0.d0  ! time(y) water(kg/y) gas(kg/y) 
  /
END


!=========================== condition couplers ===============================
INITIAL_CONDITION
  FLOW_CONDITION initial
  REGION all
END

BOUNDARY_CONDITION outlet
  FLOW_CONDITION initial
  REGION outside
END

SOURCE_SINK well
  FLOW_CONDITION well
  REGION center
END

!=========================== observation points ===============================
OBSERVATION
  REGION obspoint
END

!========================== solver ============================================
NEWTON_SOLVER FLOW
!  RTOL 1.d-08
!  ATOL 1.d-08
!  STOL 1.d-30       ! Turn off STOL criteria
  MAXIT 20          ! Maximum newton iteration number
END

LINEAR_SOLVER FLOW
!  SOLVER DIRECT
END

!=========================== output options ===================================
OUTPUT
  TIMES y 0.0 1.0 2.0 3.0 4.0 5.0 6.0 7.0 8.0 9.0 10.0 20.0 30.0 40.0 50.0 60.0 70.0 80.0 90.0 100.0 350.0 1000.0 3000.0 5000.0 7000.0 9000.0 10000.0
  FORMAT HDF5
  VARIABLES
    LIQUID_PRESSURE
    GAS_PRESSURE
    EFFECTIVE_POROSITY
    LIQUID_SATURATION
    GAS_SATURATION
    CAPILLARY_PRESSURE
  END
END


!=========================== times ============================================
TIME
  FINAL_TIME 1.d4 y
  INITIAL_TIMESTEP_SIZE 1 s
  MAXIMUM_TIMESTEP_SIZE 55.d0 y 
END


!==============================================================================
END_SUBSURFACE
!==============================================================================
