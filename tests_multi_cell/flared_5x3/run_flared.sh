$BRAGFLO_DIR/BRAGFLO/Source/bragflo -input bf_flared_allpm.inp -csd bf2_closure.dat -binary bf_flared_allpm.bin -output bf_flared_allpm.out -summary bf_flared_allpm.sum -rout bf_flared_allpm.rout
python $BINREAD_DIR/binread.py --plane xy --postarray pcgw --binary bf_flared_allpm.bin --hdf5 bf_flared_allpm.h5 --unitgrid true --snapshot_timeunit y --round_time true --ascii bf_flared_allpm.txt

#if necessary
#export PFLOTRAN_SRC=$PFLOTRAN_DIR/src/pflotran
$PFLOTRAN_SRC/pflotran -input_prefix pf_flared_allpm

#python bfpf_compare.py bf_flared_allpm pf_flared_allpm 2 2

