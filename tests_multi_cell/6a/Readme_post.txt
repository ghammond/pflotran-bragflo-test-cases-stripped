#on jt
python /utilities/bragflo/BRAGFLO/Source/binreadyr.py --plane xy --postarray pcgw --binary bf_6a.bin --ascii bf_6a.txt --hdf5 bf_6a.h5
python /utilities/bragflo/BRAGFLO/Source/binreadyr.py --plane xy --postarray pcgw --binary bf_6a_allgen.bin --ascii bf_6a_allgen.txt --hdf5 bf_6a_allgen.h5
python /utilities/bragflo/BRAGFLO/Source/binreadyr.py --plane xy --postarray pcgw --binary bf_6a_midgen.bin --ascii bf_6a_midgen.txt --hdf5 bf_6a_midgen.h5
python /utilities/bragflo/BRAGFLO/Source/binreadyr.py --plane xy --postarray pcgw --binary bf_6a_midgen_nomgo.bin --ascii bf_6a_midgen_nomgo.txt --hdf5 bf_6a_midgen_nomgo.h5
python /utilities/bragflo/BRAGFLO/Source/binreadyr.py --plane xy --postarray pcgw --binary bf_6a_allgen_nomgo.bin --ascii bf_6a_allgen_nomgo.txt --hdf5 bf_6a_allgen_nomgo.h5
python /utilities/bragflo/BRAGFLO/Source/binreadyr.py --plane xy --postarray pcgw --binary bf_6a_allgen_ttol.bin --ascii bf_6a_allgen_ttol.txt --hdf5 bf_6a_allgen_ttol.h5


python bfpf_compres.py bf_6a pf_6a 1 11
python bfpf_compres.py bf_6a_allgen pf_6a_allgen 1 11
python bfpf_compres.py bf_6a_midgen pf_6a_midgen 1 11
python bfpf_compres.py bf_6a_midgen_nomgo pf_6a_midgen_nomgo 1 11
python bfpf_compres.py bf_6a_allgen_nomgo pf_6a_allgen_nomgo 1 11
python bfpf_compres.py bf_6a_allgen_ttol pf_6a_allgen_ttol 1 11

