#!/usr/bin/python
import sys
import os
import numpy as np
import h5py
import matplotlib.pyplot as plt


###############################################################################
## Open the PFLOTRAN h5 files
###############################################################################

# Pass the hdf5 filenames on the command line along with ij index of cell
bfname_h5_input = sys.argv[1]
bfh_h5 = h5py.File(bfname_h5_input + '.h5', mode='r')
pfname_h5_input = sys.argv[2]
pfh_h5 = h5py.File(pfname_h5_input  + '.h5', mode='r')
cell_i_index = int(sys.argv[3])
cell_j_index = int(sys.argv[4])


# Read dataset names from PFLOTRAN and get times
datasetNames = [n for n in pfh_h5.keys()]
tmptimestr=[]
tmptime=[]
for n in datasetNames:
    if 'Time' in str(n): 
        tmptime.append(str(n).split())
        tmptimestr.append(str(n)) 
pftimeyr = [round(float(i[1])) for i in tmptime]
sortedziptimes = sorted(zip(pftimeyr, tmptimestr))
pftimeyr = sorted(pftimeyr)
datasets = [i[1] for i in sortedziptimes]


###############################################################################
## Plot cell data, domain averages, and percent differences
###############################################################################

# Time
pf_timeyr = np.array(pftimeyr)
bf_timeyr = pf_timeyr
bftimeyr = bf_timeyr

# Brine Pressure

fig = plt.figure(figsize=(9.75,6))
st = fig.suptitle('Brine Pressure Results for Cell ' + str(cell_i_index) + ', ' + str(cell_j_index) + ' and Domain Average', fontsize="x-large")

bf_brine_pressure = np.array([ bfh_h5[datasets[itime]]['Liquid_Pressure [Pa]'][:,0,:] for itime in range(0,len(pftimeyr)) ])[:,cell_i_index-1,cell_j_index-1]
bf_brine_pressure_domain_avg = np.mean(np.array([ bfh_h5[datasets[itime]]['Liquid_Pressure [Pa]'][:,0,:] for itime in range(0,len(pftimeyr)) ]), axis=(1,2))
pf_brine_pressure = np.array([ pfh_h5[datasets[itime]]['Liquid_Pressure [Pa]'][:,0,:] for itime in range(0,len(pftimeyr)) ])[:,cell_i_index-1,cell_j_index-1]
pf_brine_pressure_domain_avg = np.mean(np.array([ pfh_h5[datasets[itime]]['Liquid_Pressure [Pa]'][:,0,:] for itime in range(0,len(pftimeyr)) ]), axis=(1,2))
error_brine_pressure = ((bf_brine_pressure - pf_brine_pressure)/(bf_brine_pressure))*100.
error_brine_pressure_domain_avg = ((bf_brine_pressure_domain_avg - pf_brine_pressure_domain_avg)/(bf_brine_pressure_domain_avg))*100.

ax1 = fig.add_subplot(221)
ax1.plot(bf_timeyr, bf_brine_pressure/1.e6, label="PF1")
ax1.plot(pf_timeyr, pf_brine_pressure/1.e6, 'o', label="PF2")
ax1.set_xlim(round(bftimeyr[0]),round(bftimeyr[-1]))
ax1.set_xlabel('Time [yr]')
ax1.set_ylabel('Brine Pressure [MPa]')
ax1.set_title('Cell')
ax1.legend()

ax2 = fig.add_subplot(222)
ax2.plot(bf_timeyr, bf_brine_pressure_domain_avg/1.e6, label="PF1")
ax2.plot(pf_timeyr, pf_brine_pressure_domain_avg/1.e6, 'o', label="PF2")
ax2.set_xlim(round(bftimeyr[0]),round(bftimeyr[-1]))
ax2.set_xlabel('Time [yr]')
ax2.set_ylabel('Brine Pressure [MPa]')
ax2.set_title('Domain')
ax2.legend()

ax3 = fig.add_subplot(223)
ax3.plot(bf_timeyr, error_brine_pressure, 'rx')
ax3.set_xlim(round(bftimeyr[0]),round(bftimeyr[-1]))
ax3.set_xlabel('Time [yr]')
ax3.set_ylabel('Brine Pressure Error [%]')
ax3.set_title("Cell")

ax4 = fig.add_subplot(224)
ax4.plot(bf_timeyr, error_brine_pressure_domain_avg, 'rx')
ax4.set_xlim(round(bftimeyr[0]),round(bftimeyr[-1]))
ax4.set_xlabel('Time [yr]')
ax4.set_ylabel('Brine Pressure Error [%]')
ax4.set_title('Domain')

plt.tight_layout()
st.set_y(0.95)
fig.subplots_adjust(top=0.85)
plt.savefig(bfname_h5_input + '-' + pfname_h5_input + '_brine_pressure.png', dpi=300)
plt.show()

# Brine Saturation

fig = plt.figure(figsize=(9.75,6))
st = fig.suptitle('Brine Saturation Results for Cell ' + str(cell_i_index) + ', ' + str(cell_j_index) + ' and Domain Average', fontsize="x-large")

bf_brine_saturation = np.array([ bfh_h5[datasets[itime]]['Liquid_Saturation'][:,0,:] for itime in range(0,len(pftimeyr)) ])[:,cell_i_index-1,cell_j_index-1]
bf_brine_saturation_domain_avg = np.mean(np.array([ bfh_h5[datasets[itime]]['Liquid_Saturation'][:,0,:] for itime in range(0,len(pftimeyr)) ]), axis=(1,2))
pf_brine_saturation = np.array([ pfh_h5[datasets[itime]]['Liquid_Saturation'][:,0,:] for itime in range(0,len(pftimeyr)) ])[:,cell_i_index-1,cell_j_index-1]
pf_brine_saturation_domain_avg = np.mean(np.array([ pfh_h5[datasets[itime]]['Liquid_Saturation'][:,0,:] for itime in range(0,len(pftimeyr)) ]), axis=(1,2))
error_brine_saturation = ((bf_brine_saturation - pf_brine_saturation)/(bf_brine_saturation))*100.
error_brine_saturation_domain_avg = ((bf_brine_saturation_domain_avg - pf_brine_saturation_domain_avg)/(bf_brine_saturation_domain_avg))*100.

ax1 = fig.add_subplot(221)
ax1.plot(bf_timeyr, bf_brine_saturation, label="PF1")
ax1.plot(pf_timeyr, pf_brine_saturation, 'o', label="PF2")
ax1.set_xlim(round(bftimeyr[0]),round(bftimeyr[-1]))
ax1.set_xlabel('Time [yr]')
ax1.set_ylabel('Brine Saturation [nondim]')
ax1.set_title('Cell')
ax1.legend()

ax2 = fig.add_subplot(222)
ax2.plot(bf_timeyr, bf_brine_saturation_domain_avg, label="PF1")
ax2.plot(pf_timeyr, pf_brine_saturation_domain_avg, 'o', label="PF2")
ax2.set_xlim(round(bftimeyr[0]),round(bftimeyr[-1]))
ax2.set_xlabel('Time [yr]')
ax2.set_ylabel('Brine saturation [nondim]')
ax2.set_title('Domain')
ax2.legend()

ax3 = fig.add_subplot(223)
ax3.plot(bf_timeyr, error_brine_saturation, 'rx')
ax3.set_xlim(round(bftimeyr[0]),round(bftimeyr[-1]))
ax3.set_xlabel('Time [yr]')
ax3.set_ylabel('Brine Saturation Error [%]')
ax3.set_title("Cell")

ax4 = fig.add_subplot(224)
ax4.plot(bf_timeyr, error_brine_saturation_domain_avg, 'rx')
ax4.set_xlim(round(bftimeyr[0]),round(bftimeyr[-1]))
ax4.set_xlabel('Time [yr]')
ax4.set_ylabel('Brine Saturation Error [%]')
ax4.set_title('Domain')

plt.tight_layout()
st.set_y(0.95)
fig.subplots_adjust(top=0.85)
plt.savefig(bfname_h5_input + '-' + pfname_h5_input + '_brine_saturation.png', dpi=300)
plt.show()

# Gas Pressure

fig = plt.figure(figsize=(9.75,6))
st = fig.suptitle('Gas Pressure Results for Cell ' + str(cell_i_index) + ', ' + str(cell_j_index) + ' and Domain Average', fontsize="x-large")

bf_gas_pressure = np.array([ bfh_h5[datasets[itime]]['Gas_Pressure [Pa]'][:,0,:] for itime in range(0,len(pftimeyr)) ])[:,cell_i_index-1,cell_j_index-1]
bf_gas_pressure_domain_avg = np.mean(np.array([ bfh_h5[datasets[itime]]['Gas_Pressure [Pa]'][:,0,:] for itime in range(0,len(pftimeyr)) ]), axis=(1,2))
pf_gas_pressure = np.array([ pfh_h5[datasets[itime]]['Gas_Pressure [Pa]'][:,0,:] for itime in range(0,len(pftimeyr)) ])[:,cell_i_index-1,cell_j_index-1]
pf_gas_pressure_domain_avg = np.mean(np.array([ pfh_h5[datasets[itime]]['Gas_Pressure [Pa]'][:,0,:] for itime in range(0,len(pftimeyr)) ]), axis=(1,2))
error_gas_pressure = ((bf_gas_pressure - pf_gas_pressure)/(bf_gas_pressure))*100.
error_gas_pressure_domain_avg = ((bf_gas_pressure_domain_avg - pf_gas_pressure_domain_avg)/(bf_gas_pressure_domain_avg))*100.

ax1 = fig.add_subplot(221)
ax1.plot(bf_timeyr, bf_gas_pressure/1.e6, label="PF1")
ax1.plot(pf_timeyr, pf_gas_pressure/1.e6, 'o', label="PF2")
ax1.set_xlim(round(bftimeyr[0]),round(bftimeyr[-1]))
ax1.set_xlabel('Time [yr]')
ax1.set_ylabel('Gas Pressure [MPa]')
ax1.set_title('Cell')
ax1.legend()

ax2 = fig.add_subplot(222)
ax2.plot(bf_timeyr, bf_gas_pressure_domain_avg/1.e6, label="PF1")
ax2.plot(pf_timeyr, pf_gas_pressure_domain_avg/1.e6, 'o', label="PF2")
ax2.set_xlim(round(bftimeyr[0]),round(bftimeyr[-1]))
ax2.set_xlabel('Time [yr]')
ax2.set_ylabel('Gas Pressure [MPa]')
ax2.set_title('Domain')
ax2.legend()

ax3 = fig.add_subplot(223)
ax3.plot(bf_timeyr, error_gas_pressure, 'rx')
ax3.set_xlim(round(bftimeyr[0]),round(bftimeyr[-1]))
ax3.set_xlabel('Time [yr]')
ax3.set_ylabel('Gas Pressure Error [%]')
ax3.set_title("Cell")

ax4 = fig.add_subplot(224)
ax4.plot(bf_timeyr, error_gas_pressure_domain_avg, 'rx')
ax4.set_xlim(round(bftimeyr[0]),round(bftimeyr[-1]))
ax4.set_xlabel('Time [yr]')
ax4.set_ylabel('Gas Pressure Error [%]')
ax4.set_title('Domain')

plt.tight_layout()
st.set_y(0.95)
fig.subplots_adjust(top=0.85)
plt.savefig(bfname_h5_input + '-' + pfname_h5_input + '_gas_pressure.png', dpi=300)
plt.show()

# Capillary Pressure

fig = plt.figure(figsize=(9.75,6))
st = fig.suptitle('Capillary Pressure Results for Cell ' + str(cell_i_index) + ', ' + str(cell_j_index) + ' and Domain Average', fontsize="x-large")

bf_capillary_pressure = np.array([ bfh_h5[datasets[itime]]['Capillary_Pressure [Pa]'][:,0,:] for itime in range(0,len(pftimeyr)) ])[:,cell_i_index-1,cell_j_index-1]
bf_capillary_pressure_domain_avg = np.mean(np.array([ bfh_h5[datasets[itime]]['Capillary_Pressure [Pa]'][:,0,:] for itime in range(0,len(pftimeyr)) ]), axis=(1,2))
pf_capillary_pressure = np.array([ pfh_h5[datasets[itime]]['Capillary_Pressure [Pa]'][:,0,:] for itime in range(0,len(pftimeyr)) ])[:,cell_i_index-1,cell_j_index-1]
pf_capillary_pressure_domain_avg = np.mean(np.array([ pfh_h5[datasets[itime]]['Capillary_Pressure [Pa]'][:,0,:] for itime in range(0,len(pftimeyr)) ]), axis=(1,2))
error_capillary_pressure = (np.divide((bf_capillary_pressure - pf_capillary_pressure),bf_capillary_pressure, out=np.zeros_like(bf_capillary_pressure - pf_capillary_pressure), where=bf_capillary_pressure!=0.))*100.
error_capillary_pressure_domain_avg = (np.divide((bf_capillary_pressure_domain_avg - pf_capillary_pressure_domain_avg),bf_capillary_pressure_domain_avg, out=np.zeros_like(bf_capillary_pressure_domain_avg - pf_capillary_pressure_domain_avg), where=bf_capillary_pressure_domain_avg!=0.))*100.

ax1 = fig.add_subplot(221)
ax1.plot(bf_timeyr, bf_capillary_pressure/1.e6, label="PF1")
ax1.plot(pf_timeyr, pf_capillary_pressure/1.e6, 'o', label="PF2")
ax1.set_xlim(round(bftimeyr[0]),round(bftimeyr[-1]))
ax1.set_xlabel('Time [yr]')
ax1.set_ylabel('Capillary Pressure [MPa]')
ax1.set_title('Cell')
ax1.legend()

ax2 = fig.add_subplot(222)
ax2.plot(bf_timeyr, bf_capillary_pressure_domain_avg/1.e6, label="PF1")
ax2.plot(pf_timeyr, pf_capillary_pressure_domain_avg/1.e6, 'o', label="PF2")
ax2.set_xlim(round(bftimeyr[0]),round(bftimeyr[-1]))
ax2.set_xlabel('Time [yr]')
ax2.set_ylabel('Capillary Pressure [MPa]')
ax2.set_title('Domain')
ax2.legend()

ax3 = fig.add_subplot(223)
ax3.plot(bf_timeyr, error_capillary_pressure, 'rx')
ax3.set_xlim(round(bftimeyr[0]),round(bftimeyr[-1]))
ax3.set_xlabel('Time [yr]')
ax3.set_ylabel('Capillary Pressure Error [%]')
ax3.set_title("Cell")

ax4 = fig.add_subplot(224)
ax4.plot(bf_timeyr, error_capillary_pressure_domain_avg, 'rx')
ax4.set_xlim(round(bftimeyr[0]),round(bftimeyr[-1]))
ax4.set_xlabel('Time [yr]')
ax4.set_ylabel('Capillary Pressure Error [%]')
ax4.set_title('Domain')

plt.tight_layout()
st.set_y(0.95)
fig.subplots_adjust(top=0.85)
plt.savefig(bfname_h5_input + '-' + pfname_h5_input + '_capillary_pressure.png', dpi=300)
plt.show()

# Porosity

fig = plt.figure(figsize=(9.75,6))
st = fig.suptitle('Porosity Results for Cell ' + str(cell_i_index) + ', ' + str(cell_j_index) + ' and Domain Average', fontsize="x-large")

bf_porosity = np.array([ bfh_h5[datasets[itime]]['Effective_Porosity'][:,0,:] for itime in range(0,len(pftimeyr)) ])[:,cell_i_index-1,cell_j_index-1]
bf_porosity_domain_avg = np.mean(np.array([ bfh_h5[datasets[itime]]['Effective_Porosity'][:,0,:] for itime in range(0,len(pftimeyr)) ]), axis=(1,2))
pf_porosity = np.array([ pfh_h5[datasets[itime]]['Effective_Porosity'][:,0,:] for itime in range(0,len(pftimeyr)) ])[:,cell_i_index-1,cell_j_index-1]
pf_porosity_domain_avg = np.mean(np.array([ pfh_h5[datasets[itime]]['Effective_Porosity'][:,0,:] for itime in range(0,len(pftimeyr)) ]), axis=(1,2))
error_porosity = ((bf_porosity - pf_porosity)/(bf_porosity))*100.
error_porosity_domain_avg = ((bf_porosity_domain_avg - pf_porosity_domain_avg)/(bf_porosity_domain_avg))*100.

ax1 = fig.add_subplot(221)
ax1.plot(bf_timeyr, bf_porosity, label="PF1")
ax1.plot(pf_timeyr, pf_porosity, 'o', label="PF2")
ax1.set_xlim(round(bftimeyr[0]),round(bftimeyr[-1]))
ax1.set_xlabel('Time [yr]')
ax1.set_ylabel('Porosity [nondim]')
ax1.set_title('Cell')
ax1.legend()

ax2 = fig.add_subplot(222)
ax2.plot(bf_timeyr, bf_porosity_domain_avg, label="PF1")
ax2.plot(pf_timeyr, pf_porosity_domain_avg, 'o', label="PF2")
ax2.set_xlim(round(bftimeyr[0]),round(bftimeyr[-1]))
ax2.set_xlabel('Time [yr]')
ax2.set_ylabel('Porosity [nondim]')
ax2.set_title('Domain')
ax2.legend()

ax3 = fig.add_subplot(223)
ax3.plot(bf_timeyr, error_porosity, 'rx')
ax3.set_xlim(round(bftimeyr[0]),round(bftimeyr[-1]))
ax3.set_xlabel('Time [yr]')
ax3.set_ylabel('Porosity Error [%]')
ax3.set_title("Cell")

ax4 = fig.add_subplot(224)
ax4.plot(bf_timeyr, error_porosity_domain_avg, 'rx')
ax4.set_xlim(round(bftimeyr[0]),round(bftimeyr[-1]))
ax4.set_xlabel('Time [yr]')
ax4.set_ylabel('Porosity Error [%]')
ax4.set_title('Domain')

plt.tight_layout()
st.set_y(0.95)
fig.subplots_adjust(top=0.85)
plt.savefig(bfname_h5_input + '-' + pfname_h5_input + '_porosity.png', dpi=300)
plt.show()

# Brine density

fig = plt.figure(figsize=(9.75,6))
st = fig.suptitle('Brine Density Results for Cell ' + str(cell_i_index) + ', ' + str(cell_j_index) + ' and Domain Average', fontsize="x-large")

bf_brine_density = np.array([ bfh_h5[datasets[itime]]['Liquid_Density [kg_m^3]'][:,0,:] for itime in range(0,len(pftimeyr)) ])[:,cell_i_index-1,cell_j_index-1]
bf_brine_density_domain_avg = np.mean(np.array([ bfh_h5[datasets[itime]]['Liquid_Density [kg_m^3]'][:,0,:] for itime in range(0,len(pftimeyr)) ]), axis=(1,2))
pf_brine_density = np.array([ pfh_h5[datasets[itime]]['Liquid_Density [kg_m^3]'][:,0,:] for itime in range(0,len(pftimeyr)) ])[:,cell_i_index-1,cell_j_index-1]
pf_brine_density_domain_avg = np.mean(np.array([ pfh_h5[datasets[itime]]['Liquid_Density [kg_m^3]'][:,0,:] for itime in range(0,len(pftimeyr)) ]), axis=(1,2))
error_brine_density = ((bf_brine_density - pf_brine_density)/(bf_brine_density))*100.
error_brine_density_domain_avg = ((bf_brine_density_domain_avg - pf_brine_density_domain_avg)/(bf_brine_density_domain_avg))*100.

ax1 = fig.add_subplot(221)
ax1.plot(bf_timeyr, bf_brine_density, label="PF1")
ax1.plot(pf_timeyr, pf_brine_density, 'o', label="PF2")
ax1.set_xlim(round(bftimeyr[0]),round(bftimeyr[-1]))
ax1.set_xlabel('Time [yr]')
ax1.set_ylabel('Brine Density [kg/m^3]')
ax1.set_title('Cell')
ax1.legend()

ax2 = fig.add_subplot(222)
ax2.plot(bf_timeyr, bf_brine_density_domain_avg, label="PF1")
ax2.plot(pf_timeyr, pf_brine_density_domain_avg, 'o', label="PF2")
ax2.set_xlim(round(bftimeyr[0]),round(bftimeyr[-1]))
ax2.set_xlabel('Time [yr]')
ax2.set_ylabel('Brine Density [kg/m^3]')
ax2.set_title('Domain')
ax2.legend()

ax3 = fig.add_subplot(223)
ax3.plot(bf_timeyr, error_brine_density, 'rx')
ax3.set_xlim(round(bftimeyr[0]),round(bftimeyr[-1]))
ax3.set_xlabel('Time [yr]')
ax3.set_ylabel('Brine Density Error [%]')
ax3.set_title("Cell")

ax4 = fig.add_subplot(224)
ax4.plot(bf_timeyr, error_brine_density_domain_avg, 'rx')
ax4.set_xlim(round(bftimeyr[0]),round(bftimeyr[-1]))
ax4.set_xlabel('Time [yr]')
ax4.set_ylabel('Brine Density Error [%]')
ax4.set_title('Domain')

plt.tight_layout()
st.set_y(0.95)
fig.subplots_adjust(top=0.85)
plt.savefig(bfname_h5_input + '-' + pfname_h5_input + '_brine_density.png', dpi=300)
plt.show()

# Gas density

fig = plt.figure(figsize=(9.75,6))
st = fig.suptitle('Gas Density Results for Cell ' + str(cell_i_index) + ', ' + str(cell_j_index) + ' and Domain Average', fontsize="x-large")

bf_gas_density = np.array([ bfh_h5[datasets[itime]]['Gas_Density [kg_m^3]'][:,0,:] for itime in range(0,len(pftimeyr)) ])[:,cell_i_index-1,cell_j_index-1]
bf_gas_density_domain_avg = np.mean(np.array([ bfh_h5[datasets[itime]]['Gas_Density [kg_m^3]'][:,0,:] for itime in range(0,len(pftimeyr)) ]), axis=(1,2))
pf_gas_density = np.array([ pfh_h5[datasets[itime]]['Gas_Density [kg_m^3]'][:,0,:] for itime in range(0,len(pftimeyr)) ])[:,cell_i_index-1,cell_j_index-1]
pf_gas_density_domain_avg = np.mean(np.array([ pfh_h5[datasets[itime]]['Gas_Density [kg_m^3]'][:,0,:] for itime in range(0,len(pftimeyr)) ]), axis=(1,2))
error_gas_density = ((bf_gas_density - pf_gas_density)/(bf_gas_density))*100.
error_gas_density_domain_avg = ((bf_gas_density_domain_avg - pf_gas_density_domain_avg)/(bf_gas_density_domain_avg))*100.

ax1 = fig.add_subplot(221)
ax1.plot(bf_timeyr, bf_gas_density, label="PF1")
ax1.plot(pf_timeyr, pf_gas_density, 'o', label="PF2")
ax1.set_xlim(round(bftimeyr[0]),round(bftimeyr[-1]))
ax1.set_xlabel('Time [yr]')
ax1.set_ylabel('Gas Density [kg/m^3]')
ax1.set_title('Cell')
ax1.legend()

ax2 = fig.add_subplot(222)
ax2.plot(bf_timeyr, bf_gas_density_domain_avg, label="PF1")
ax2.plot(pf_timeyr, pf_gas_density_domain_avg, 'o', label="PF2")
ax2.set_xlim(round(bftimeyr[0]),round(bftimeyr[-1]))
ax2.set_xlabel('Time [yr]')
ax2.set_ylabel('Gas Density [kg/m^3]')
ax2.set_title('Domain')
ax2.legend()

ax3 = fig.add_subplot(223)
ax3.plot(bf_timeyr, error_gas_density, 'rx')
ax3.set_xlim(round(bftimeyr[0]),round(bftimeyr[-1]))
ax3.set_xlabel('Time [yr]')
ax3.set_ylabel('Gas Density Error [%]')
ax3.set_title("Cell")

ax4 = fig.add_subplot(224)
ax4.plot(bf_timeyr, error_gas_density_domain_avg, 'rx')
ax4.set_xlim(round(bftimeyr[0]),round(bftimeyr[-1]))
ax4.set_xlabel('Time [yr]')
ax4.set_ylabel('Gas Density Error [%]')
ax4.set_title('Domain')

plt.tight_layout()
st.set_y(0.95)
fig.subplots_adjust(top=0.85)
plt.savefig(bfname_h5_input + '-' + pfname_h5_input + '_gas_density.png', dpi=300)
plt.show()

# Close the hdf5 files
bfh_h5.close()
pfh_h5.close()
