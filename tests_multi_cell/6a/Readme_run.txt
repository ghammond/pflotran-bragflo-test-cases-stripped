$BRAGFLO_DIR/BRAGFLO/Source/bragflo -input bf_6a.inp -binary bf_6a.bin -output bf_6a.out -summary bf_6a.sum -rout bf_6a.rout
$BRAGFLO_DIR/BRAGFLO/Source/bragflo -input bf_6a_allgen.inp -binary bf_6a_allgen.bin -output bf_6a_allgen.out -summary bf_6a_allgen.sum -rout bf_6a_allgen.rout
$BRAGFLO_DIR/BRAGFLO/Source/bragflo -input bf_6a_midgen.inp -binary bf_6a_midgen.bin -output bf_6a_midgen.out -summary bf_6a_midgen.sum -rout bf_6a_midgen.rout
$BRAGFLO_DIR/BRAGFLO/Source/bragflo -input bf_6a_midgen_nomgo.inp -binary bf_6a_midgen_nomgo.bin -output bf_6a_midgen_nomgo.out -summary bf_6a_midgen_nomgo.sum -rout bf_6a_midgen_nomgo.rout
$BRAGFLO_DIR/BRAGFLO/Source/bragflo -input bf_6a_allgen_nomgo.inp -binary bf_6a_allgen_nomgo.bin -output bf_6a_allgen_nomgo.out -summary bf_6a_allgen_nomgo.sum -rout bf_6a_allgen_nomgo.rout
$BRAGFLO_DIR/BRAGFLO/Source/bragflo -input bf_6a_allgen_ttol.inp -binary bf_6a_allgen_ttol.bin -output bf_6a_allgen_ttol.out -summary bf_6a_allgen_ttol.sum -rout bf_6a_allgen_ttol.rout

$PFLOTRAN_DIR/src/pflotran/pflotran -input_prefix pf_6a -output_prefix pf_6a
$PFLOTRAN_DIR/src/pflotran/pflotran -input_prefix pf_6a_allgen -output_prefix pf_6a_allgen
$PFLOTRAN_DIR/src/pflotran/pflotran -input_prefix pf_6a_midgen -output_prefix pf_6a_midgen
$PFLOTRAN_DIR/src/pflotran/pflotran -input_prefix pf_6a_midgen_nomgo -output_prefix pf_6a_midgen_nomgo
$PFLOTRAN_DIR/src/pflotran/pflotran -input_prefix pf_6a_allgen_nomgo -output_prefix pf_6a_allgen_nomgo
$PFLOTRAN_DIR/src/pflotran/pflotran -input_prefix pf_6a_allgen_ttol -output_prefix pf_6a_allgen_ttol

