$BRAGFLO_DIR/BRAGFLO/Source/bragflo -input bf_markerbeds_krp1.inp -binary bf_markerbeds_krp1.bin -output bf_markerbeds_krp1.out -summary bf_markerbeds_krp1.sum -rout bf_markerbeds_krp1.rout #> bf_markerbeds_krp1.stdout
python $BINREAD_DIR/binread.py --plane xy --postarray pcgw --binary bf_markerbeds_krp1.bin --hdf5 bf_markerbeds_krp1.h5 --unitgrid true --snapshot_timeunit y --round_time false --ascii bf_markerbeds_krp1.txt

#if necessary
#export PFLOTRAN_SRC=$PFLOTRAN_DIR/src/pflotran
$PFLOTRAN_SRC/pflotran -input_prefix pf_markerbeds_krp1 > pf_markerbeds_krp1.stdout

#python bfpf_compare.py bf_markerbeds_krp1 pf_markerbeds_krp1 3 5

