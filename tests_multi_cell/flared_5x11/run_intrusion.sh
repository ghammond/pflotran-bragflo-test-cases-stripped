$BRAGFLO_DIR/BRAGFLO/Source/bragflo -input bf_intrusion.inp -csd bf2_closure.dat -binary bf_intrusion.bin -output bf_intrusion.out -summary bf_intrusion.sum -rout bf_intrusion.rout > bf_intrusion.stdout
python $BINREAD_DIR/binread.py --plane xy --postarray pcgw --binary bf_intrusion.bin --hdf5 bf_intrusion.h5 --unitgrid true --snapshot_timeunit y --round_time false --ascii bf_intrusion.txt

#if necessary
#export PFLOTRAN_SRC=$PFLOTRAN_DIR/src/pflotran
$PFLOTRAN_SRC/pflotran -input_prefix pf_intrusion > pf_intrusion.stdout

#python bfpf_compare.py bf_intrusion pf_intrusion 3 5

