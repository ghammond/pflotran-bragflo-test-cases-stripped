#!/bin/python
import matplotlib
matplotlib.use('Agg') # needed for jt if no display
import matplotlib.pyplot as plt
import sys
import os
import argparse
import numpy as np
import h5py

#####################################################
tpl_pf_pnl_header = (
'TIME', 'PNL', 'CORRATI', 'CORRATH', 
'BIORATI', 'BIORATH', 'FEOH2_SR', 'FE_SR', 
'MGO_HR', 'MGOH2_CR', 'MGO_CR', 'HYMAG_CR', 
'H2RATE', 'BRINRATE', 'FERATE', 'CELLRATE', 
'FEOH2R', 'FESR', 'MGOR', 'MGOH2R', 
'HYMAGR', 'MGCO3R', 'FECONC', 'CELLCONC', 
'FEOH2C', 'FESC', 'MGOC', 'MGOH2C', 
'HYMAGC', 'MGCO3C', 'PORSOLID'
);
#####################################################
tpl_bf_hist_header = (
'PRESBRIN', 'PRESGAS', 'SATGAS',  'SATBRINE', 'PCGW',
'DENBRINE', 'DENGAS', 'POROS'
);
#####################################################
tpl_h5_labels = (
'Brine Pressure', 'Gas Pressure', 'Gas Saturation',  
'Brine Saturation', 'Capillary Pressure',
'Brine Density', 'Gas Density', 'Eff. Porosity'
);
#####################################################
dic_bf_hist_header = { #unstructured grid output
'PRESBRIN' : 'Liquid Pressure [Pa]', 
'PRESGAS' : 'Gas Pressure [Pa]', 
'SATGAS' : 'Gas Saturation',  
'SATBRINE' : 'Liquid Saturation',
'PCGW' : 'Capillary Pressure [Pa]',
'DENBRINE' : 'Liquid Density [kg_m^3]', 
'DENGAS' : 'Gas Density [kg_m^3]', 
'POROS' : 'Effective Porosity'
};
#####################################################

# Convert tuple of column headings to an indexed dict
dic_pf_pnl_header = { k: i for i, k in enumerate(tpl_pf_pnl_header) };
dic_bf_h5_header = { k: i for i, k in enumerate(tpl_bf_hist_header) };

# List of deck files to run
DECK_FILES=[
  'flared_allpm', 
  'intrusion', 
  'markerbeds',
  ];

DECK_DIR=[
  'flared_5x3', 'flared_5x11', 'flared_markerbeds',
  ];

# Cell indexes of each test for the plots 
CELL_I=[
  2,3,1,
  ];
CELL_J=[
  2,8,6,
  ];

# Number of columns in grid (necessary for unstructured grid output)
NCOL=[
  5,5,5,
  ];

for jj in range(len(DECK_FILES)):
  deckname = DECK_FILES[jj]
  deck_dir = DECK_DIR[jj]
  cell_i_index = CELL_I[jj]
  cell_j_index = CELL_J[jj]
  grid_ncol = NCOL[jj]
  
  print '\n============ ' + deck_dir + ' ============'
  os.chdir(deck_dir)
  
  #############################################################################
  # Read in the data
  #############################################################################
  pf_h5_filename = 'pf_' + deckname + '.h5'
  pnl1_filename =  'pf_' + deckname + '-0.pnl'
  bf_h5_filename = 'bf_' + deckname + '.h5'
  
  # Read bragflo h5 file
  try:
    fh_bf_h5 = h5py.File(bf_h5_filename, mode='r');
    print bf_h5_filename + ' found.'
    skip_this_test = False
  except IOError:
    print bf_h5_filename + ' is missing!'
    skip_this_test = True
    
  # Read pflotran h5 file
  if not skip_this_test:
    try:
      fh_pf_h5 = h5py.File(pf_h5_filename, mode='r');
      print pf_h5_filename + ' found.'
      skip_this_test = False
    except IOError:
      print pf_h5_filename + ' is missing!'
      skip_this_test = True
      
  # Read pflotran pnl file (may not exist if no gas generation)
  try:
    pf_pnl_data = np.loadtxt(pnl1_filename, dtype='float64', skiprows=1);
    print pnl1_filename + ' found.'
    gas_generation = True
  except IOError:
    print pnl1_filename + ' is missing!'
    gas_generation = False
 
 
 
  if not skip_this_test:
    #############################################################################
    ## Plot the data
    #############################################################################
    print deckname + ' test:'
    
    # Set some defaults plot values
    plt.rcParams['figure.dpi'] = 300;
    plt.rcParams['xtick.labelsize'] = 9;
    plt.rcParams['ytick.labelsize'] = 9;
    plt.rcParams['axes.labelsize'] = 9;
    plt.rcParams['axes.titlesize'] = 9;
    plt.rcParams['legend.fontsize'] = 9;
    SECYR = 3.17097919837646E-08;
    YRSEC = 3.153600E+07;
  
    bf_data = []; pf_data = []; 
    pf_data_at_bf_times = []; error = [];
    title = []; label = [];
    # get bragflo time [yr]
    bf_timeyr = fh_bf_h5['/output_times/time_y']
    snapshot_refs = fh_bf_h5['/output_times/snapshot_refs']
    bf_timeyr = np.array([fh_bf_h5[snapshot_refs[itime]].attrs['time_y'] \
                for itime in range(snapshot_refs.shape[0]) ]) 
    # get pflotran time [yr]
    pf_timestamps = [n for n in fh_pf_h5.keys()]
    tmptimestr=[]; tmptime=[]
    for n in pf_timestamps:
      if 'Time' in str(n): 
        tmptime.append(str(n).split())
        tmptimestr.append(str(n)) 
        pf_timeyr = [round(float(i[2])) for i in tmptime] #unstructured grid output
        sortedziptimes = sorted(zip(pf_timeyr, tmptimestr))
        pf_timeyr = sorted(pf_timeyr)  
        datasets = [i[1] for i in sortedziptimes]
    pf_timeyr = np.array(pf_timeyr)
    
    num_h5_plots = 0
    k = 0
    j = 0
    # load data from hdf5 files
    print 'READING HDF5 DATA. . . . '
    for var in tpl_bf_hist_header:
      try:
        # set titles
        title.append([]); title[k] = dic_bf_hist_header[var] + ' (' + \
                          str(cell_i_index) + ',' + str(cell_j_index) + ')'
        title.append([]); title[k+1] = dic_bf_hist_header[var] + ' domain avg.'
        label.append([]); label[k] = tpl_h5_labels[j]
        label.append([]); label[k+1] = tpl_h5_labels[j]
        pf_data_at_bf_times.append([]); pf_data_at_bf_times.append([]); # hold space
        # get bragflo variable array 
        bf_data.append([])
        bf_data[k] = np.array([ fh_bf_h5[snapshot_refs[itime]][var][:,:,0] \
                     for itime in range(snapshot_refs.shape[0]) ])[:,cell_i_index-1,cell_j_index-1]
        bf_data.append([])
        bf_data[k+1] = np.mean(np.array([ fh_bf_h5[snapshot_refs[itime]][var][:,:,0] \
                       for itime in range(snapshot_refs.shape[0]) ]), axis=(1,2)) 
        # get pflotran variable array
        pf_data.append([])
        pf_data[k] = np.array([ fh_pf_h5[datasets[itime]][dic_bf_hist_header[var]][(cell_i_index-1)+(cell_j_index-1)*grid_ncol] \
                     for itime in range(0,len(pf_timeyr)) ])
        pf_data.append([])
        pf_data[k+1] = np.mean(np.array([ fh_pf_h5[datasets[itime]][dic_bf_hist_header[var]][:] \
                       for itime in range(0,len(pf_timeyr)) ]), axis=(1)) 
        # calculate difference for cell index
        error.append([])
        error_bf_data = bf_data[k]
        error_pf_data = pf_data[k]
        for g in range(len(error_bf_data)):
          if ((error_bf_data[g]<1.0e-20) and (error_bf_data[g]>=0.0)):
            error_bf_data[g] = 1.0e-20
          if ((error_bf_data[g]>-1.0e-20) and (error_bf_data[g]<0.0)):
            error_bf_data[g] = -1.0e-20
        for g in range(len(error_pf_data)):
          if ((error_pf_data[g]<1.0e-20) and (error_pf_data[g]>=0.0)):
            error_pf_data[g] = 1.0e-20
          if ((error_pf_data[g]>-1.0e-20) and (error_pf_data[g]<0.0)):
            error_pf_data[g] = -1.0e-20
        m = min(len(error_bf_data),len(error_pf_data))
        error[k] = 100.0*abs((error_bf_data[0:m-1] - error_pf_data[0:m-1])/error_bf_data[0:m-1])
        # calculate difference for domain average
        error.append([])
        error_bf_data = bf_data[k+1]
        error_pf_data = pf_data[k+1]
        for g in range(len(error_bf_data)):
          if ((error_bf_data[g]<1.0e-20) and (error_bf_data[g]>=0.0)):
            error_bf_data[g] = 1.0e-20
          if ((error_bf_data[g]>-1.0e-20) and (error_bf_data[g]<0.0)):
            error_bf_data[g] = -1.0e-20
        for g in range(len(error_pf_data)):
          if ((error_pf_data[g]<1.0e-20) and (error_pf_data[g]>=0.0)):
            error_pf_data[g] = 1.0e-20
          if ((error_pf_data[g]>-1.0e-20) and (error_pf_data[g]<0.0)):
            error_pf_data[g] = -1.0e-20
        m = min(len(error_bf_data),len(error_pf_data))
        error[k+1] = 100.0*abs((error_bf_data[0:m-1] - error_pf_data[0:m-1])/error_bf_data[0:m-1])
        k = k + 2
        num_h5_plots = num_h5_plots + 2
        j = j + 1
      except KeyError:
        continue
  
  
    # load data from pnl and hdf5 (don't reset k iterator)
    if gas_generation:
      print 'READING PNL (GAS GENERATION) DATA. . . . '
      # get time
      bf_pnl_timeyr = fh_bf_h5['/output_tables/simulation_statistics_table']['TIMESEC'] / YRSEC
      pf_pnl_timeyr = pf_pnl_data[1:, dic_pf_pnl_header['TIME']] / YRSEC
      num_pnl_plots = 0
      for var in tpl_pf_pnl_header:
        # each test only has one waste panel
        if var == 'TIME' or var == 'PNL':
          continue
        title.append([]); title[k] = var
        # get pflotran variable array
        pf_data.append([])
        pf_data[k] = pf_pnl_data[1:, dic_pf_pnl_header[var]]
        # interpolate pflotran data at bragflo times
        pf_data_at_bf_times.append([])
        pf_data_at_bf_times[k] = np.interp(bf_pnl_timeyr,pf_pnl_timeyr,pf_data[k])
        k = k + 1
        num_pnl_plots = num_pnl_plots + 1
  
  
    print 'PLOTTING TECPLOT DATA. . . . '
    j = 0
    k = 1
    while j < num_h5_plots:
      fig101,axes101 = plt.subplots(2,2,sharex=True,figsize=(8.0, 4.0));
      fig_subname = str(k) + '_' + title[j]
      major_ticks_y = [0,1.0,10.0,100.0] 
      minor_ticks_y = [0.25,0.50,0.75,2.5,5.0,7.5,25.0,50.0,75.0]
      m = min(len(bf_data[j]),len(pf_data[j]))
      axes101[0,0].semilogx(bf_timeyr[0:m-1],bf_data[j][0:m-1],'b-',alpha=0.7,label='BRAGFLO');
      axes101[0,0].semilogx(pf_timeyr[0:m-1],pf_data[j][0:m-1],'r--',alpha=0.7,label='PFLOTRAN');
      axes101[0,0].set_xlabel('Time [yr]');
      axes101[0,0].set_ylabel(label[j], color='k');
      axes101[0,0].set_xlim([1.e0,1.e4]);
      axes101[0,0].legend(loc='best');
      axes101[0,0].set_title(title[j]);
      axes101[1,0].plot(bf_timeyr[0:m-1],error[j][0:m-1],'ro',alpha=0.7);
      axes101[1,0].set_xscale('symlog');
      axes101[1,0].set_yscale('symlog',linthreshy=1.0);
      axes101[1,0].set_xlabel('Time [yr]');
      axes101[1,0].set_ylabel('Difference [%]', color='k');
      axes101[1,0].set_xlim([1.e0,1.e4]);
      axes101[1,0].set_ylim([0,1.e2]);
      axes101[1,0].legend(loc='best');
      axes101[1,0].set_title('Difference in ' + title[j]);
      axes101[1,0].set_yticks(major_ticks_y);
      axes101[1,0].set_yticks(minor_ticks_y, minor=True);
      axes101[1,0].grid(which='major',alpha=1.0);
      axes101[1,0].grid(which='minor',alpha=0.3);
      axes101[0,1].semilogx(bf_timeyr[0:m-1],bf_data[j+1][0:m-1],'b-',alpha=0.7,label='BRAGFLO');
      axes101[0,1].semilogx(pf_timeyr[0:m-1],pf_data[j+1][0:m-1],'r--',alpha=0.7,label='PFLOTRAN');
      axes101[0,1].set_xlabel('Time [yr]');
      axes101[0,1].set_ylabel(label[j], color='k');
      axes101[0,1].set_xlim([1.e0,1.e4]);
      axes101[0,1].legend(loc='best');
      axes101[0,1].set_title(title[j+1]);
      axes101[1,1].plot(bf_timeyr[0:m-1],error[j+1][0:m-1],'ro',alpha=0.7);
      axes101[1,1].set_xscale('symlog');
      axes101[1,1].set_yscale('symlog',linthreshy=1.0);
      axes101[1,1].set_xlabel('Time [yr]');
      axes101[1,1].set_ylabel('Difference [%]', color='k');
      axes101[1,1].set_xlim([1.e0,1.e4]);
      axes101[1,1].set_ylim([0,1.e2]);
      axes101[1,1].legend(loc='best');
      axes101[1,1].set_title('Difference in ' + title[j+1]);      
      axes101[1,1].set_yticks(major_ticks_y);
      axes101[1,1].set_yticks(minor_ticks_y, minor=True);
      axes101[1,1].grid(which='major',alpha=1.0);
      axes101[1,1].grid(which='minor',alpha=0.3);
      # adjust layout
      fig101.tight_layout();
      fig101.subplots_adjust(top=0.90);
      # save the figure
      fig101.savefig(deckname+'_'+fig_subname+'.png', dpi=300);
      # toss stuff
      plt.close(fig101);
      del(fig101, axes101);
      j = j + 2
      k = k + 1
  
  
  
  # go back
  os.chdir('..')
  
  


