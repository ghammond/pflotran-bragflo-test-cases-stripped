$BRAGFLO_DIR/BRAGFLO/Source/bragflo -input bf_6b.inp -csd bf2_closure.dat -binary bf_6b.bin -output bf_6b.out -summary bf_6b.sum -rout bf_6b.rout
$BRAGFLO_DIR/BRAGFLO/Source/bragflo -input bf_6b_wgas.inp -csd bf2_closure.dat -binary bf_6b_wgas.bin -output bf_6b_wgas.out -summary bf_6b_wgas.sum -rout bf_6b_wgas.rout
$BRAGFLO_DIR/BRAGFLO/Source/bragflo -input bf_6b_wgasnocreep.inp -binary bf_6b_wgasnocreep.bin -output bf_6b_wgasnocreep.out -summary bf_6b_wgasnocreep.sum -rout bf_6b_wgasnocreep.rout
$BRAGFLO_DIR/BRAGFLO/Source/bragflo -input bf_6b_winj.inp -csd bf2_closure.dat -binary bf_6b_winj.bin -output bf_6b_winj.out -summary bf_6b_winj.sum -rout bf_6b_winj.rout
$BRAGFLO_DIR/BRAGFLO/Source/bragflo -input bf_6b_wgas_ttol.inp -csd bf2_closure.dat -binary bf_6b_wgas_ttol.bin -output bf_6b_wgas_ttol.out -summary bf_6b_wgas_ttol.sum -rout bf_6b_wgas_ttol.rout

$PFLOTRAN_DIR/src/pflotran/pflotran -input_prefix pf_6b -output_prefix pf_6b
$PFLOTRAN_DIR/src/pflotran/pflotran -input_prefix pf_6b_wgas -output_prefix pf_6b_wgas
$PFLOTRAN_DIR/src/pflotran/pflotran -input_prefix pf_6b_wgasnocreep -output_prefix pf_6b_wgasnocreep
$PFLOTRAN_DIR/src/pflotran/pflotran -input_prefix pf_6b_winj -output_prefix pf_6b_winj
$PFLOTRAN_DIR/src/pflotran/pflotran -input_prefix pf_6b_wgas_ttol -output_prefix pf_6b_wgas_ttol

