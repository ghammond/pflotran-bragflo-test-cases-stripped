#!/usr/bin/python
import sys
import os
import numpy as np
import h5py
import matplotlib.pyplot as plt


###############################################################################
## Open the BRAGFLO 1 and BRAGFLO 2 h5 files
###############################################################################

# Pass the hdf5 filenames on the command line along with ij index of cell
bfname_h5_input = sys.argv[1]
bfh_h5 = h5py.File(bfname_h5_input + '.h5', mode='r')
pfname_h5_input = sys.argv[2]
pfh_h5 = h5py.File(pfname_h5_input  + '.h5', mode='r')
cell_i_index = int(sys.argv[3])
cell_j_index = int(sys.argv[4])

# Read BRAGFLO 1 time data
bftimeyr = bfh_h5['/output_times/time_y']
snapshot_refs1 = bfh_h5['/output_times/snapshot_refs']

# Read BRAGFLO 2 time data
pftimeyr = pfh_h5['/output_times/time_y']
snapshot_refs2 = pfh_h5['/output_times/snapshot_refs']

# Specify desired output
bf_resname = ('PRESBRIN', 'SATBRINE', 'PRESGAS', 'PCGW', 'POROS', 'DENBRINE', 'DENGAS')
pf_resname = ('PRESBRIN', 'SATBRINE', 'PRESGAS', 'PCGW', 'POROS', 'DENBRINE', 'DENGAS')
mp_restxt = ('Brine Pressure', 'Brine Saturation', 'Gas Pressure', 'Capillary Pressure', 'Porosity', 'Brine Density', 'Gas Density')
mp_units = ('(Pa)', '()', '(Pa)', '(Pa)', '()', '(kg/m^3)', '(kg/m^3)')

# Dictionary of region names and i,j cell index numbers (starting at lower-left of grid (1,1)
res_dict = { str('CELL '+str(cell_i_index)+' '+str(cell_j_index)) : (cell_i_index, cell_i_index, cell_j_index, cell_j_index), \
           }

###############################################################################
## Plot region averages, domain averages, and percent differences
###############################################################################

# Time
bf_timeyr = np.array([ bfh_h5[snapshot_refs1[itime]].attrs['time_y'] for itime in range(snapshot_refs1.shape[0]) ]) 
pf_timeyr = np.array([ pfh_h5[snapshot_refs2[itime]].attrs['time_y'] for itime in range(snapshot_refs2.shape[0]) ])

# Determine rows and columns of data sizes
cols = np.array([ bfh_h5[snapshot_refs1[itime]]['PRESBRIN'][:,:,0] for itime in range(snapshot_refs1.shape[0]) ]).shape[1]
rows = np.array([ bfh_h5[snapshot_refs1[itime]]['PRESBRIN'][:,:,0] for itime in range(snapshot_refs1.shape[0]) ]).shape[2]

# Plot Results

for i in range(0,len(mp_restxt)):
    print str(mp_restxt[i])
    for j in res_dict:
        print ' - ' + j
        fig = plt.figure(figsize=(9.75,6))
        st = fig.suptitle(str(mp_restxt[i]) + ' for ' + str(j) + ' and Total Domain', fontsize="x-large")
        if ((res_dict[j][0] - res_dict[j][1] != 0) and (res_dict[j][2] - res_dict[j][3] != 0)):
            bf_region_avg = np.mean(np.array([ bfh_h5[snapshot_refs1[itime]][bf_resname[i]][:,:,0] \
                            for itime in range(snapshot_refs1.shape[0]) ])[:,res_dict[j][0]-1:res_dict[j][1]-1,res_dict[j][2]-1:res_dict[j][3]-1], axis=(1,2))
            pf_region_avg = np.mean(np.array([ pfh_h5[snapshot_refs2[itime]][pf_resname[i]][:,:,0] \
                            for itime in range(snapshot_refs2.shape[0]) ])[:,res_dict[j][0]-1:res_dict[j][1]-1,res_dict[j][2]-1:res_dict[j][3]-1], axis=(1,2))
        elif ((res_dict[j][0] - res_dict[j][1] == 0) and (res_dict[j][2] - res_dict[j][3] != 0)):
            bf_region_avg = np.mean(np.array([ bfh_h5[snapshot_refs1[itime]][bf_resname[i]][:,:,0] \
                            for itime in range(snapshot_refs1.shape[0]) ])[:,res_dict[j][0]-1,res_dict[j][2]-1:res_dict[j][3]-1], axis=(1))
            pf_region_avg = np.mean(np.array([ pfh_h5[snapshot_refs2[itime]][pf_resname[i]][:,:,0] \
                            for itime in range(snapshot_refs2.shape[0]) ])[:,res_dict[j][0]-1,res_dict[j][2]-1:res_dict[j][3]-1], axis=(1))
        elif ((res_dict[j][0] - res_dict[j][1] != 0) and (res_dict[j][2] - res_dict[j][3] == 0)):
            bf_region_avg = np.mean(np.array([ bfh_h5[snapshot_refs1[itime]][bf_resname[i]][:,:,0] \
                            for itime in range(snapshot_refs1.shape[0]) ])[:,res_dict[j][0]-1:res_dict[j][1]-1,res_dict[j][2]-1], axis=(1))
            pf_region_avg = np.mean(np.array([ pfh_h5[snapshot_refs2[itime]][pf_resname[i]][:,:,0] \
                            for itime in range(snapshot_refs2.shape[0]) ])[:,res_dict[j][0]-1:res_dict[j][1]-1,res_dict[j][2]-1], axis=(1))
        else:
            bf_region_avg = np.array([ bfh_h5[snapshot_refs1[itime]][bf_resname[i]][:,:,0] \
                            for itime in range(snapshot_refs1.shape[0]) ])[:,res_dict[j][0]-1,res_dict[j][2]-1]
            pf_region_avg = np.array([ pfh_h5[snapshot_refs2[itime]][pf_resname[i]][:,:,0] \
                            for itime in range(snapshot_refs2.shape[0]) ])[:,res_dict[j][0]-1,res_dict[j][2]-1]
        bf_domain_avg = np.mean(np.array([ bfh_h5[snapshot_refs1[itime]][bf_resname[i]][:,:,0] \
                        for itime in range(snapshot_refs1.shape[0]) ]), axis=(1,2))
        pf_domain_avg = np.mean(np.array([ pfh_h5[snapshot_refs2[itime]][pf_resname[i]][:,:,0] \
                        for itime in range(snapshot_refs2.shape[0]) ]), axis=(1,2))
        error_region = (np.divide((bf_region_avg - pf_region_avg),bf_region_avg,out=np.zeros_like(bf_region_avg - pf_region_avg),where=bf_region_avg!=0.))*100.
        error_domain = (np.divide((bf_domain_avg - pf_domain_avg),bf_domain_avg,out=np.zeros_like(bf_domain_avg - pf_domain_avg),where=bf_domain_avg!=0.))*100.

        ax1 = fig.add_subplot(221)
        ax1.plot(bf_timeyr, bf_region_avg, linestyle='-', marker='o', label="BF")
        ax1.plot(pf_timeyr, pf_region_avg, linestyle='--', marker='x', label="BF ttol")
        ax1.set_xlim(round(bftimeyr[0]),round(bftimeyr[-1]))
        ax1.ticklabel_format(style='sci', axis='y', useOffset=False, scilimits=(-3,4))
        ax1.set_xlabel('Time [yr]')
        ax1.set_ylabel(str(mp_restxt[i]) + ' ' + str(mp_units[i]))
        ax1.set_title(str(j))
        ax1.legend()

        ax2 = fig.add_subplot(222)
        ax2.plot(bf_timeyr, bf_domain_avg, linestyle='-', marker='o', label="BF")
        ax2.plot(pf_timeyr, pf_domain_avg, linestyle='--', marker='x', label="BF ttol")
        ax2.set_xlim(round(bftimeyr[0]),round(bftimeyr[-1]))
        ax2.ticklabel_format(style='sci', axis='y', useOffset=False, scilimits=(-3,4))
        ax2.set_xlabel('Time [yr]')
        ax2.set_ylabel(str(mp_restxt[i]) + ' ' + str(mp_units[i]))
        ax2.set_title('Domain')
        ax2.legend()

        ax3 = fig.add_subplot(223)
        ax3.plot(bf_timeyr, error_region, 'r^')
        ax3.set_xlim(round(bftimeyr[0]),round(bftimeyr[-1]))
        ax3.ticklabel_format(style='sci', axis='y', useOffset=False, scilimits=(-3,4))
        ax3.set_xlabel('Time [yr]')
        ax3.set_ylabel(str(mp_restxt[i]) + ' Error (%)')
        ax3.set_title(str(j))

        ax4 = fig.add_subplot(224)
        ax4.plot(bf_timeyr, error_domain, 'r^')
        ax4.set_xlim(round(bftimeyr[0]),round(bftimeyr[-1]))
        ax4.ticklabel_format(style='sci', axis='y', useOffset=False, scilimits=(-3,4))
        ax4.set_xlabel('Time [yr]')
        ax4.set_ylabel(str(mp_restxt[i]) + ' Error (%)')
        ax4.set_title('Domain')

        plt.tight_layout()
        st.set_y(0.95)
        fig.subplots_adjust(top=0.85)
        plt.savefig(bfname_h5_input + '-' + pfname_h5_input + '_' + str(bf_resname[i]) + '_' + str(j), dpi=300)
#        plt.show()

# Close the hdf5 files
bfh_h5.close()
pfh_h5.close()
