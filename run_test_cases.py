import numpy as np
import os
import sys

###############################################################################
#### COMMAND LINE INPUTS ######################################################
###############################################################################

single = False
multi = False
arg_help = False

for k in range(len(sys.argv)-1):
  k = k + 1
  if sys.argv[k] == '-single':
    single = True
  elif sys.argv[k] == '-multi':
    multi = True
  elif sys.argv[k] == '-all':
    multi = True
    single = True
  elif sys.argv[k] == '-help':
    arg_help = True
  else:
    print '\nERROR: Unrecognized argument ' + sys.argv[k]
    print '       If you need help, use argument -help'
    exit()

if arg_help or (len(sys.argv) == 1):
  print ' \nUsage: \n'
  print ' python run_test_cases.py -flag \n'
  print ' Only one of the following flags must be given: \n'
  print '   -single     :: runs all single cell tests'
  print '   -multi      :: runs all multi cell tests'
  print '   -all        :: runs all tests'
  print '   -help       :: prints help message'
  print ' '
  print ' Have a Carlsgood day! \n'
  exit()

###############################################################################
#### MULTI CELL TESTS #########################################################
###############################################################################

if multi:
  multi_cell_tests = []
  #### list all sub-directories in the 0th element
  multi_cell_tests.append([])
  multi_cell_tests[0].append("1")
  multi_cell_tests[0].append("2")
  multi_cell_tests[0].append("4")
  multi_cell_tests[0].append("6a")
  multi_cell_tests[0].append("6b")
  multi_cell_tests[0].append("6c")
  multi_cell_tests[0].append("flared_5x3")
  #### tuple input values for each test:
  #### ("name_of_test","-csd bf_closure_filename","plot_script_name",
  ####   "plot_points")
  #### sub-directory 1/ tests:
  multi_cell_tests.append([])
  multi_cell_tests[1].append(("1","","bfpf_compare.py","10 1"))
  #### sub-directory 2/ tests:
  multi_cell_tests.append([])
  multi_cell_tests[2].append(("2","","bfpf_compare.py","30 1"))
  #### sub-directory 4/ tests:
  multi_cell_tests.append([])
  multi_cell_tests[3].append(("4","","bfpf_compare.py","15 10"))
  #### sub-directory 6a/ tests:
  multi_cell_tests.append([])
  multi_cell_tests[4].append(("6a","","bfpf_compres.py","1 11"))
  multi_cell_tests[4].append(("6a_allgen","","bfpf_compres.py","1 11"))
  multi_cell_tests[4].append(("6a_allgen_nomgo","","bfpf_compres.py","1 11"))
  multi_cell_tests[4].append(("6a_allgen_ttol","","bfpf_compres.py","1 11"))
  multi_cell_tests[4].append(("6a_midgen","","bfpf_compres.py","1 11"))
  multi_cell_tests[4].append(("6a_midgen_nomgo","","bfpf_compres.py","1 11"))
  #### sub-directory 6b/ tests:
  multi_cell_tests.append([])
  multi_cell_tests[5].append(("6b","-csd bf2_closure.dat","bfpf_compres.py", \
                              "3 2"))
  multi_cell_tests[5].append(("6b_wgas","-csd bf2_closure.dat", \
                              "bfpf_compres.py","3 2"))
  multi_cell_tests[5].append(("6b_wgasnocreep","","bfpf_compres.py","3 2"))
  multi_cell_tests[5].append(("6b_wgas_ttol","-csd bf2_closure.dat", \
                              "bfpf_compres.py","3 2"))
  multi_cell_tests[5].append(("6b_winj","-csd bf2_closure.dat", \
                              "bfpf_compres.py","3 2"))
  #### sub-directory 6c/ tests:
  multi_cell_tests.append([])
  multi_cell_tests[6].append(("6c","","bfpf_compare.py","3 1"))

  #### sub-directory flared_5x3/ tests:
  multi_cell_tests.append([])
  multi_cell_tests[7].append(("flared_allpm","-csd bf2_closure.dat","bfpf_compare.py","2 2"))

  os.chdir("tests_multi_cell")
  screen = "" #"-screen_output off"

  k = 0
  for directory in multi_cell_tests[0]:
    os.chdir(directory)
    #### Clean the directory of old results figures:
    command_line = "rm *.png"
    os.system(command_line)
    k = k + 1
    for test in multi_cell_tests[k]:
      #### Run BRAGFLO test:
      bftest = "bf_" + test[0]
      print "\nBRAGFLO test " + bftest + ".inp"
      command_line = "$BRAGFLO_DIR/BRAGFLO/Source/bragflo -input " + bftest + \
                   ".inp " + test[1] + " -binary " + bftest + ".bin -output " \
                   + bftest + ".out -summary " + bftest + ".sum -rout " + \
                   bftest + ".rout | tee " + bftest + ".stdout"
      print command_line
      os.system(command_line)
      #### Convert BRAGFLO output to h5:
      command_line = "python $BINREAD_DIR/binread.py --plane xy " + \
                   "--postarray pcgw --binary " + bftest + ".bin " + \
                   "--ascii " + bftest + ".txt --hdf5 " + bftest + ".h5"
      print command_line
      os.system(command_line)
      #### Run PFLOTRAN test:
      pftest = "pf_" + test[0]
      print "\nPFLOTRAN test " + pftest + ".in"
      command_line = "$PFLOTRAN_SRC/pflotran -pflotranin " + pftest + ".in " + \
                     "| tee " + pftest + ".stdout"
      print command_line
      os.system(command_line)
      #### Clean the directory of temporary files:
      command_line = "rm *.sum *.regression *.bin *.rout bf*.txt"
      os.system(command_line)
      #### Plot the comparison results:
      command_line = "python " + test[2] + " " + bftest + " " + pftest + " " + \
                   test[3]
      print "\n" + command_line
      os.system(command_line)
    os.chdir("..")

  # Go back
  os.chdir("..")

###############################################################################
#### SINGLE CELL TESTS ########################################################
###############################################################################

if single:

  single_cell_tests = []
  #### list all sub-directories in the 0th element
  single_cell_tests.append([])
  single_cell_tests[0].append("case060100")
  single_cell_tests[0].append("case060200")
  single_cell_tests[0].append("case060300")
  single_cell_tests[0].append("case060400")
  single_cell_tests[0].append("case060500")
  single_cell_tests[0].append("case060600")
  single_cell_tests[0].append("case060700")
  single_cell_tests[0].append("case072100")
  #### tuple input values for each test:
  #### ("name_of_test","-csd bf_closure_filename")
  #### sub-directory case060100/ tests:
  single_cell_tests.append([])
  single_cell_tests[1].append(("case060100_0d_gas_injection",""))
  #### sub-directory case060200/ tests:
  single_cell_tests.append([])
  single_cell_tests[2].append(("case060200_0d_gas_generation_midsat",""))
  single_cell_tests[2].append(("case060210_0d_gas_generation_hisat",""))
  single_cell_tests[2].append(("case060220_0d_gas_generation_lowsat",""))
  single_cell_tests[2].append(("case060220_0d_gas_generation_superlowsat",""))
  single_cell_tests[2].append(("case060230_0d_gas_generation_corbio_midsat",""))
  single_cell_tests[2].append(("case060240_0d_gas_generation_corbio_hisat",""))
  single_cell_tests[2].append(("case060250_0d_gas_generation_corbio_lowsat",""))
  #### sub-directory case060300/ tests:
  single_cell_tests.append([])
  single_cell_tests[3].append(("case060300_0d_creep_static", \
                               "-csd bf_closure.dat"))
  single_cell_tests[3].append(("case060310_0d_creep_gas_injection", \
                               "-csd bf_closure.dat"))
  single_cell_tests[3].append(("case060320_0d_creep_gas_generation", \
                               "-csd bf_closure.dat"))
  #### sub-directory case060400/ tests:
  single_cell_tests.append([])
  single_cell_tests[4].append(("case060400_0d_porecomp_gas_injection",""))
  #### sub-directory case060500/ tests:
  single_cell_tests.append([])
  single_cell_tests[5].append(("case060500_0d_fracture_gas_injection",""))
  #### sub-directory case060600/ tests:
  single_cell_tests.append([])
  single_cell_tests[6].append(("case060600_0d_klinkenberg_gas_injection",""))
  #### sub-directory case060700/ tests:
  single_cell_tests.append([])
  single_cell_tests[7].append(("case060700_0d_rks_calc_gas_injection",""))
  #### sub-directory case072100/ tests:
  single_cell_tests.append([])
  single_cell_tests[8].append(("case072100_reduced_repo", \
                               "-csd bf_closure.dat"))

  os.chdir("tests_single_cell")
  screen = "" #"-screen_output off"

  k = 0
  for directory in single_cell_tests[0]:
    os.chdir(directory)
    #### Clean the directory of old results figures:
    command_line = "rm *.png"
    os.system(command_line)
    k = k + 1
    for test in single_cell_tests[k]:
      #### Run BRAGFLO test:
      bftest = "bf_" + test[0]
      print "\nBRAGFLO test " + bftest + ".inp"
      command_line = "$BRAGFLO_DIR/BRAGFLO/Source/bragflo -input " + bftest + \
                   ".inp " + test[1] + " -binary " + bftest + ".bin -output " \
                   + bftest + ".out -summary " + bftest + ".sum -rout " + \
                   bftest + ".rout | tee " + bftest + ".stdout"
      print command_line
      os.system(command_line)
      #### Convert BRAGFLO output to h5:
      command_line = "python $BINREAD_DIR/binread.py --plane xz " + \
                   "--binary " + bftest + ".bin --hdf5 " + bftest + ".h5"
      print command_line
      os.system(command_line)
      #### Run PFLOTRAN test:
      pftest = "pf_" + test[0]
      print "\nPFLOTRAN test " + pftest + ".in"
      command_line = "$PFLOTRAN_SRC/pflotran -pflotranin " + pftest + ".in " + \
                     "| tee " + pftest + ".stdout"
      print command_line
      os.system(command_line)
      #### Clean the directory of temporary files:
      command_line = "rm *.out *.sum *.regression *.bin *.rout bf*.txt"
      os.system(command_line)
    os.chdir("..")
    
  #### Plot the results
  command_line = "python plot_comparison.py"
  os.system(command_line)

  # Go back
  os.chdir("..")
